[TOC]

# mbuf

```c
#include<rte_mbuf.h>
```

> 源码位置：src/lib/librte_mbuf/rte_mbuf.h

```c
/**
 * @file
 * RTE Mbuf
 *
 * The mbuf library provides the ability to create and destroy buffers
 * that may be used by the RTE application to store message
 * buffers. The message buffers are stored in a mempool, using the
 * RTE mempool library.
 * 
 * 这个mbuf库提供了创建和销毁buffer的能力，buffer能被RTE应用来存储消息缓存。
 * 这个消息缓存存储在内存池中，使用RTE内存池库
 * 
 *
 * The preferred way to create a mbuf pool is to use
 * rte_pktmbuf_pool_create(). However, in some situations, an
 * application may want to have more control (ex: populate the pool with
 * specific memory), in this case it is possible to use functions from
 * rte_mempool. See how rte_pktmbuf_pool_create() is implemented for
 * details.
 * 
 * 创建mbuf池的首选方法是使用 rte_pktmbuf_pool_create().
 * 但是，在某些情况下，应用程序可能希望拥有更多控制权（例如：使用特定内存来填充内存池）
 * 在这种情况下，可以使用 rte_mempool 中的函数，有关详细信息，请参阅如何实现 rte_pktmbuf_pool_create()。
 *
 * This library provides an API to allocate/free packet mbufs, which are
 * used to carry network packets.
 * 
 * 该库提供了一个API来分配/释放数据包mbuf，用于承载网络数据包。
 *
 * To understand the concepts of packet buffers or mbufs, you
 * should read "TCP/IP Illustrated, Volume 2: The Implementation,
 * Addison-Wesley, 1995, ISBN 0-201-63354-X from Richard Stevens"
 * http://www.kohala.com/start/tcpipiv2.html
 * 
 * 要了解数据包缓冲区或mbufs的概念，您应该阅读
 * "TCP/IP Illustrated, Volume 2: The Implementation,
 * Addison-Wesley, 1995, ISBN 0-201-63354-X from Richard Stevens"
 * http://www.kohala.com/start/tcpipiv2.html
 * 
 */
```

## 函数

### rte_get_rx_ol_flag_name

```c
/**
 * Get the name of a RX offload flag
 * 
 * 获取RX卸载标志的名称
 *
 * @param mask
 *   The mask describing the flag.
 *   描述标志的掩码
 *   
 * @return
 *   The name of this flag, or NULL if it's not a valid RX flag.
 *   返回指定标记的名称，如果不是有效的 RX 标志，则为 NULL
 *   
 */
const char *rte_get_rx_ol_flag_name(uint64_t mask);
```

### rte_get_rx_ol_flag_list

```c
/**
 * Dump the list of RX offload flags in a buffer
 * 
 * 将 RX 卸载标志列表转储到缓冲区中
 *
 * @param mask
 *   The mask describing the RX flags.
 *   描述 RX 标志的掩码。
 * @param buf
 *   The output buffer.
 *   输出的缓冲区
 * @param buflen
 *   The length of the buffer.
 *   缓冲区的长度。
 * @return
 *   0 on success, (-1) on error.
 *   0表示成功，-1表示失败
 */
int rte_get_rx_ol_flag_list(uint64_t mask, char *buf, size_t buflen);
```

### rte_get_tx_ol_flag_name

```c
/**
 * Get the name of a TX offload flag
 * 
 * 获取TX卸载标志的名称
 *
 * @param mask
 *   The mask describing the flag. Usually only one bit must be set.
 *   Several bits can be given if they belong to the same mask.
 *   Ex: PKT_TX_L4_MASK.
 *   描述标志的掩码
 *   通常只需设置一位。如果几个位属于同一个掩码，则可以传入多个位。
 * @return
 *   The name of this flag, or NULL if it's not a valid TX flag.
 *   返回指定标记的名称，如果不是有效的 TX 标志，则为 NULL
 */
const char *rte_get_tx_ol_flag_name(uint64_t mask);
```

### rte_get_tx_ol_flag_list

```c
/**
 * Dump the list of TX offload flags in a buffer
 * 
 * 将 TX 卸载标志列表转储到缓冲区中
 *
 * @param mask
 *   The mask describing the TX flags.
 *   描述 TX 标志的掩码。
 * @param buf
 *   The output buffer.
 *   输出的缓冲区
 * @param buflen
 *   The length of the buffer.
 *   缓冲区的长度
 * @return
 *   0 on success, (-1) on error.
 *   0表示成功，-1表示失败
 */
int rte_get_tx_ol_flag_list(uint64_t mask, char *buf, size_t buflen);
```

### rte_mbuf_prefetch_part1

```c
/**
 * Prefetch the first part of the mbuf
 * 
 * 预取 mbuf 的第一部分
 *
 * The first 64 bytes of the mbuf corresponds to fields that are used early
 * in the receive path. If the cache line of the architecture is higher than
 * 64B, the second part will also be prefetched.
 * 
 * mbuf 的前 64 个字节对应于在接收路径早期使用的字段。
 * 如果架构的cache line高于64B，第二部分也会被预取。
 *
 * @param m
 *   The pointer to the mbuf.
 *   指向 mbuf 的指针
 */
static inline void
rte_mbuf_prefetch_part1(struct rte_mbuf *m)
{
	rte_prefetch0(&m->cacheline0);
}
```

### rte_mbuf_prefetch_part2

```c
/**
 * Prefetch the second part of the mbuf
 * 
 * 预取 mbuf 的第二部分
 *
 * The next 64 bytes of the mbuf corresponds to fields that are used in the
 * transmit path. If the cache line of the architecture is higher than 64B,
 * this function does nothing as it is expected that the full mbuf is
 * already in cache.
 * 
 * mbuf 的下一个 64 字节对应于传输路径中使用的字段。
 * 如果架构的缓存线高于 64B，则此函数不执行任何操作，因为预计完整的 mbuf 已在缓存中。
 *
 * @param m
 *   The pointer to the mbuf.
 *   指向 mbuf 的指针。
 */
static inline void
rte_mbuf_prefetch_part2(struct rte_mbuf *m)
{
#if RTE_CACHE_LINE_SIZE == 64
	rte_prefetch0(&m->cacheline1);
#else
	RTE_SET_USED(m);
#endif
}
```


### rte_pktmbuf_priv_size

```c
//获取内存池的私有空间大小
static inline uint16_t rte_pktmbuf_priv_size(struct rte_mempool *mp);
```


### rte_mbuf_data_iova

```c
/**
 * Return the IO address of the beginning of the mbuf data
 * 
 * 返回mbuf数据开头的IO地址
 *
 * @param mb
 *   The pointer to the mbuf.
 *   指向 mbuf 的指针。
 * @return
 *   The IO address of the beginning of the mbuf data
 *   mbuf数据开头的IO地址
 */
static inline rte_iova_t
rte_mbuf_data_iova(const struct rte_mbuf *mb)
{
	return mb->buf_iova + mb->data_off;
}
```

### rte_mbuf_data_iova_default

```c
/**
 * Return the default IO address of the beginning of the mbuf data
 * 
 * 返回mbuf数据开头的默认IO地址
 *
 * This function is used by drivers in their receive function, as it
 * returns the location where data should be written by the NIC, taking
 * the default headroom in account.
 * 
 * 驱动程序在其接收函数中使用此函数，因为它返回 NIC 应写入数据的位置.
 *
 * @param mb
 *   The pointer to the mbuf.
 *   指向 mbuf 的指针。
 * @return
 *   The IO address of the beginning of the mbuf data
 *   mbuf数据开头的IO地址
 */
static inline rte_iova_t
rte_mbuf_data_iova_default(const struct rte_mbuf *mb)
{
	return mb->buf_iova + RTE_PKTMBUF_HEADROOM;
}
```


### rte_mbuf_from_indirect

```c
/**
 * Return the mbuf owning the data buffer address of an indirect mbuf.
 * 
 * 返回拥有间接mbuf的数据缓冲区地址的mbuf。
 *
 * @param mi
 *   The pointer to the indirect mbuf.
 *   指向间接 mbuf 的指针。
 * @return
 *   The address of the direct mbuf corresponding to buffer_addr.
 *   buffer_addr对应的直接mbuf的地址。
 */
static inline struct rte_mbuf *
rte_mbuf_from_indirect(struct rte_mbuf *mi)
{
	return (struct rte_mbuf *)RTE_PTR_SUB(mi->buf_addr, sizeof(*mi) + mi->priv_size);
}
```

### rte_mbuf_buf_addr

```c
/**
 * Return address of buffer embedded in the given mbuf.
 * 
 * 嵌入在给定mbuf中的缓冲区的地址。
 *
 * The return value shall be same as mb->buf_addr if the mbuf is already
 * initialized and direct. However, this API is useful if mempool of the
 * mbuf is already known because it doesn't need to access mbuf contents in
 * order to get the mempool pointer.
 * 
 * 如果 mbuf是直接buf并且已经初始化，则返回值应与mb->buf_addr相同。
 * 但是，如果 mbuf 的内存池已知，则此 API 很有用，
 * 因为它不需要访问 mbuf 内容来获取内存池指针。
 *
 * @warning
 * @b EXPERIMENTAL: This API may change without prior notice.
 * This will be used by rte_mbuf_to_baddr() which has redundant code once
 * experimental tag is removed.
 * 
 * @warning 
 * @b 实验：此 API 可能会更改，恕不另行通知。
 * 这将由 rte_mbuf_to_baddr() 使用，一旦删除实验性标签，它就会有冗余代码。
 *
 * @param mb
 *   The pointer to the mbuf.
 *   指向 mbuf 的指针。
 * @param mp
 *   The pointer to the mempool of the mbuf.
 *   指向 mbuf内存池的指针。
 * @return
 *   The pointer of the mbuf buffer.
 *   mbuf 缓冲区的指针。
 */
__rte_experimental
static inline char *
rte_mbuf_buf_addr(struct rte_mbuf *mb, struct rte_mempool *mp)
{
	return (char *)mb + sizeof(*mb) + rte_pktmbuf_priv_size(mp);
}
```


### rte_mbuf_data_addr_default

```c
/**
 * Return the default address of the beginning of the mbuf data.
 * 
 * 返回mbuf数据开始的默认地址。
 *
 * @warning
 * @b EXPERIMENTAL: This API may change without prior notice.
 * 
 * @warning 
 * @b 实验：此 API 可能会更改，恕不另行通知。
 *
 * @param mb
 *   The pointer to the mbuf.
 *   指向 mbuf 的指针。
 * @return
 *   The pointer of the beginning of the mbuf data.
 *   mbuf 数据开头的指针。
 */
__rte_experimental
static inline char *
rte_mbuf_data_addr_default(__rte_unused struct rte_mbuf *mb)
{
	/* gcc complains about calling this experimental function even
	 * when not using it. Hide it with ALLOW_EXPERIMENTAL_API.
	 */
#ifdef ALLOW_EXPERIMENTAL_API
	return rte_mbuf_buf_addr(mb, mb->pool) + RTE_PKTMBUF_HEADROOM;
#else
	return NULL;
#endif
}
```

### rte_mbuf_to_baddr

```c
/**
 * Return address of buffer embedded in the given mbuf.
 * 
 * 嵌入在给定mbuf中缓冲区的返回地址。
 *
 * @note: Accessing mempool pointer of a mbuf is expensive because the
 * pointer is stored in the 2nd cache line of mbuf. If mempool is known, it
 * is better not to reference the mempool pointer in mbuf but calling
 * rte_mbuf_buf_addr() would be more efficient.
 * 
 * @note：访问 mbuf 的内存池指针很昂贵，因为指针存储在 mbuf 的第二个缓存行中。
 * 如果内存池已知，最好不要在 mbuf 中引用内存池指针，但调用 rte_mbuf_buf_addr() 会更有效。
 *
 * @param md
 *   The pointer to the mbuf.
 *   指向 mbuf 的指针。
 * @return
 *   The address of the data buffer owned by the mbuf.
 *   mbuf 拥有的数据缓冲区的地址。
 */
static inline char *
rte_mbuf_to_baddr(struct rte_mbuf *md)
{
#ifdef ALLOW_EXPERIMENTAL_API
	return rte_mbuf_buf_addr(md, md->pool);
#else
	char *buffer_addr;
	buffer_addr = (char *)md + sizeof(*md) + rte_pktmbuf_priv_size(md->pool);
	return buffer_addr;
#endif
}
```

### rte_mbuf_to_priv

```c
/**
 * Return the starting address of the private data area embedded in
 * the given mbuf.
 * 
 * 返回嵌入在给定 mbuf 中的私有数据区的起始地址。
 *
 * Note that no check is made to ensure that a private data area
 * actually exists in the supplied mbuf.
 * 
 * 请注意，不进行检查以确保提供的 mbuf 中确实存在私有数据区域。
 *
 * @param m
 *   The pointer to the mbuf.
 *   指向 mbuf 的指针。
 * @return
 *   The starting address of the private data area of the given mbuf.
 *   给定 mbuf 的私有数据区的起始地址。
 */
__rte_experimental
static inline void *
rte_mbuf_to_priv(struct rte_mbuf *m)
{
	return RTE_PTR_ADD(m, sizeof(struct rte_mbuf));
}
```


### rte_pktmbuf_priv_flags

```c
/**
 * Return the flags from private data in an mempool structure.
 * 
 * 从内存池结构中的私有数据返回标志。
 *
 * @param mp
 *   A pointer to the mempool structure.
 *   指向内存池结构的指针。
 * @return
 *   The flags from the private data structure.
 *   来自私有数据结构的标志。
 */
static inline uint32_t
rte_pktmbuf_priv_flags(struct rte_mempool *mp)
{
	struct rte_pktmbuf_pool_private *mbp_priv;

	mbp_priv = (struct rte_pktmbuf_pool_private *)rte_mempool_get_priv(mp);
	return mbp_priv->flags;
}
```


### rte_mbuf_refcnt_update

```c
/**
 * Adds given value to an mbuf's refcnt and returns its new value.
 * 
 * 将给定值添加到mbuf的refcnt字段并返回其新值。
 */
static inline uint16_t
rte_mbuf_refcnt_update(struct rte_mbuf *m, int16_t value)
{
	return __rte_mbuf_refcnt_update(m, value);
}
```


### rte_mbuf_refcnt_read

```c
/**
 * Reads the value of an mbuf's refcnt.
 * 
 * 读取mbuf的refcnt字段的值。
 */
static inline uint16_t
rte_mbuf_refcnt_read(const struct rte_mbuf *m)
{
	return m->refcnt;
}
```


### rte_mbuf_refcnt_set

```c
/**
 * Sets an mbuf's refcnt to the defined value.
 * 
 * 将mbuf的refcnt字段设置为指定的值。
 */
static inline void
rte_mbuf_refcnt_set(struct rte_mbuf *m, uint16_t new_value)
{
	m->refcnt = new_value;
}
```

### rte_mbuf_ext_refcnt_read

```c
/**
 * Reads the refcnt of an external buffer.
 * 
 * 读取外部缓冲区的 refcnt。
 * 
 *
 * @param shinfo
 *   Shared data of the external buffer.
 *   外部缓冲区的共享数据。
 * @return
 *   Reference count number.
 *   引用个数。
 */
static inline uint16_t
rte_mbuf_ext_refcnt_read(const struct rte_mbuf_ext_shared_info *shinfo)
{
	return __atomic_load_n(&shinfo->refcnt, __ATOMIC_RELAXED);
}
```

### rte_mbuf_ext_refcnt_set

```c
/**
 * Set refcnt of an external buffer.
 * 
 * 设置外部缓冲区的 refcnt。
 *
 * @param shinfo
 *   Shared data of the external buffer.
 *   外部缓冲区的共享数据。
 * @param new_value
 *   Value set
 *   设置的新值
 */
static inline void
rte_mbuf_ext_refcnt_set(struct rte_mbuf_ext_shared_info *shinfo,
	uint16_t new_value)
{
	__atomic_store_n(&shinfo->refcnt, new_value, __ATOMIC_RELAXED);
}
```


### rte_mbuf_ext_refcnt_update

```c
/**
 * Add given value to refcnt of an external buffer and return its new
 * value.
 * 将给定值添加到外部缓冲区的 refcnt 并返回其新值。
 * @param shinfo
 *   Shared data of the external buffer.
 *   外部缓冲区的共享数据。
 * @param value
 *   Value to add/subtract
 *   加/减值
 * @return
 *   Updated value
 *   更新后的值
 */
static inline uint16_t
rte_mbuf_ext_refcnt_update(struct rte_mbuf_ext_shared_info *shinfo,
	int16_t value)
{
	if (likely(rte_mbuf_ext_refcnt_read(shinfo) == 1)) {
		++value;
		rte_mbuf_ext_refcnt_set(shinfo, (uint16_t)value);
		return (uint16_t)value;
	}

	return __atomic_add_fetch(&shinfo->refcnt, (uint16_t)value,
				 __ATOMIC_ACQ_REL);
}
```

### rte_mbuf_sanity_check

```c
/**
 * Sanity checks on an mbuf.
 * 
 * 对 mbuf 的健全性检查
 *
 * Check the consistency of the given mbuf. The function will cause a
 * panic if corruption is detected.
 * 
 * 检查给定mbuf的一致性。如果检测到损坏，该函数将导致程序崩溃。
 *
 * @param m
 *   The mbuf to be checked.
 *   要检查的 mbuf。
 * @param is_header
 *   True if the mbuf is a packet header, false if it is a sub-segment
 *   of a packet (in this case, some fields like nb_segs are not checked)
 *   
 *   如果mbuf是数据包头，则为 true，如果是数据包的子段，
 *   则为 false（在这种情况下，不检查 nb_segs 等某些字段）
 */
void
rte_mbuf_sanity_check(const struct rte_mbuf *m, int is_header);
```

### rte_mbuf_check

```c
/**
 * Sanity checks on a mbuf.
 * 
 * 对 mbuf 的健全性检查。
 *
 * Almost like rte_mbuf_sanity_check(), but this function gives the reason
 * if corruption is detected rather than panic.
 * 
 * 几乎像 rte_mbuf_sanity_check()，但是这个函数给出了检测到损坏的原因，而不是直接崩溃。
 *
 * @param m
 *   The mbuf to be checked.
 *   要检查的 mbuf。
 * @param is_header
 *   True if the mbuf is a packet header, false if it is a sub-segment
 *   of a packet (in this case, some fields like nb_segs are not checked)
 *   
 *   如果 mbuf 是数据包头，则为 true，如果是数据包的子段，则为 false（在这种情况下，不检查 nb_segs 等某些字段）
 *   
 * @param reason
 *   A reference to a string pointer where to store the reason why a mbuf is
 *   considered invalid.
 *   
 *   用于存储 mbuf 被视为无效的原因。
 *   
 * @return
 *   - 0 if no issue has been found, reason is left untouched.
 *   - 0 如果没有发现问题，则不触及原因。
 *   - -1 if a problem is detected, reason then points to a string describing
 *     the reason why the mbuf is deemed invalid.
 *     -1 如果检测到问题，则 reason 将指向一个字符串，该字符串描述了 mbuf 被视为无效的原因。
 */
__rte_experimental
int rte_mbuf_check(const struct rte_mbuf *m, int is_header,
		   const char **reason);
```


### rte_mbuf_raw_alloc

```c
/**
 * Allocate an uninitialized mbuf from mempool *mp*.
 * 
 * 从 mempool mp 分配一个未初始化的 mbuf。
 *
 * This function can be used by PMDs (especially in RX functions) to
 * allocate an uninitialized mbuf. The driver is responsible of
 * initializing all the required fields. See rte_pktmbuf_reset().
 * For standard needs, prefer rte_pktmbuf_alloc().
 * 
 * PMD（尤其是在 RX 函数中）可以使用此函数来分配未初始化的 mbuf。
 * 驱动程序负责初始化所有必需的字段。
 * 请参见 rte_pktmbuf_reset()。对于标准需求，首选 rte_pktmbuf_alloc()。
 *
 * The caller can expect that the following fields of the mbuf structure
 * are initialized: buf_addr, buf_iova, buf_len, refcnt=1, nb_segs=1,
 * next=NULL, pool, priv_size. The other fields must be initialized
 * by the caller.
 * 
 * 调用者可以预期mbuf结构的以下字段被初始化：buf_addr、buf_iova、
 * buf_len、refcnt=1、nb_segs=1、next=NULL、pool、priv_size。
 * 其他字段必须由调用者初始化。
 *
 * @param mp
 *   The mempool from which mbuf is allocated.
 *   分配 mbuf 的内存池。
 * @return
 *   - The pointer to the new mbuf on success.
 *     成功时指向新 mbuf 的指针。
 *   - NULL if allocation failed.
 *     失败返回NULL
 */
static inline struct rte_mbuf *rte_mbuf_raw_alloc(struct rte_mempool *mp)
{
	struct rte_mbuf *m;

	if (rte_mempool_get(mp, (void **)&m) < 0)
		return NULL;
	__rte_mbuf_raw_sanity_check(m);
	return m;
}
```


### rte_mbuf_raw_free

```c
/**
 * Put mbuf back into its original mempool.
 * 
 * 将 mbuf 放回原来的内存池。
 *
 * The caller must ensure that the mbuf is direct and properly
 * reinitialized (refcnt=1, next=NULL, nb_segs=1), as done by
 * rte_pktmbuf_prefree_seg().
 * 
 * 调用者必须确保 mbuf 是直接的并且正确地重新初始化（refcnt=1，next=NULL，nb_segs=1），
 * 正如 rte_pktmbuf_prefree_seg() 所做的那样。
 *
 * This function should be used with care, when optimization is
 * required. For standard needs, prefer rte_pktmbuf_free() or
 * rte_pktmbuf_free_seg().
 * 
 * 当需要优化时，应谨慎使用此功能。
 * 对于标准需求，首选 rte_pktmbuf_free() 或 rte_pktmbuf_free_seg()。
 *
 * @param m
 *   The mbuf to be freed.
 */
static __rte_always_inline void
rte_mbuf_raw_free(struct rte_mbuf *m)
{
	RTE_ASSERT(!RTE_MBUF_CLONED(m) &&
		  (!RTE_MBUF_HAS_EXTBUF(m) || RTE_MBUF_HAS_PINNED_EXTBUF(m)));
	__rte_mbuf_raw_sanity_check(m);
	rte_mempool_put(m->pool, m);
}
```

### rte_pktmbuf_init

```c
/**
 * The packet mbuf constructor.
 * 
 * 数据包 mbuf 构造函数。
 *
 * This function initializes some fields in the mbuf structure that are
 * not modified by the user once created (origin pool, buffer start
 * address, and so on). This function is given as a callback function to
 * rte_mempool_obj_iter() or rte_mempool_create() at pool creation time.
 * 
 * 该函数初始化mbuf结构中的一些字段，一旦创建就不会被用户修改（源池、缓冲区起始地址等）。
 * 该函数在创建池时作为回调函数提供给 rte_mempool_obj_iter() 或 rte_mempool_create()。
 *
 * @param mp
 *   The mempool from which mbufs originate.
 *   
 *   mbuf 源自的内存池。
 *   
 * @param opaque_arg
 *   A pointer that can be used by the user to retrieve useful information
 *   for mbuf initialization. This pointer is the opaque argument passed to
 *   rte_mempool_obj_iter() or rte_mempool_create().
 *   
 *   一个指针，用户可以使用它来检索用于 mbuf 初始化的有用信息。
 *   该指针是传递给 rte_mempool_obj_iter() 或 rte_mempool_create() 的不透明参数。
 *   
 * @param m
 *   The mbuf to initialize.
 *   
 *   要初始化的 mbuf。
 *   
 * @param i
 *   The index of the mbuf in the pool table.
 *   
 *   池表中 mbuf 的索引。
 */
void rte_pktmbuf_init(struct rte_mempool *mp, void *opaque_arg,
		      void *m, unsigned i);
```

### rte_pktmbuf_pool_init

```c
/**
 * A  packet mbuf pool constructor.
 * 
 * 一个数据包mbuf池的构造函数。
 *
 * This function initializes the mempool private data in the case of a
 * pktmbuf pool. This private data is needed by the driver. The
 * function must be called on the mempool before it is used, or it
 * can be given as a callback function to rte_mempool_create() at
 * pool creation. It can be extended by the user, for example, to
 * provide another packet size.
 * 
 * 此函数在 pktmbuf 池的情况下初始化 mempool 私有数据,驱动程序需要此私有数据。
 * 该函数必须在使用之前在内存池上调用，或者可以在创建池时作为回调函数
 * 提供给 rte_mempool_create()。它可以由用户扩展，例如，提供另一种数据包大小。
 *
 * @param mp
 *   The mempool from which mbufs originate.
 *   
 *   mbuf 源自的内存池。
 *   
 * @param opaque_arg
 *   A pointer that can be used by the user to retrieve useful information
 *   for mbuf initialization. This pointer is the opaque argument passed to
 *   rte_mempool_create().
 *   
 *   一个指针，用户可以使用它来检索用于 mbuf 初始化的有用信息。
 *   该指针是传递给 rte_mempool_create() 的不透明参数。
 */
void rte_pktmbuf_pool_init(struct rte_mempool *mp, void *opaque_arg);
```


### rte_pktmbuf_pool_create

```c
/**
 * Create a mbuf pool.
 *
 * 创建一个mbuf池
 *
 * This function creates and initializes a packet mbuf pool. It is
 * a wrapper to rte_mempool functions.
 * 
 * 此函数创建并初始化一个数据包 mbuf 池。它是 rte_mempool 函数的包装器。
 *
 * @param name
 *   The name of the mbuf pool.
 *   
 *   mbuf 池的名称。
 *   
 * @param n
 *   The number of elements in the mbuf pool. The optimum size (in terms
 *   of memory usage) for a mempool is when n is a power of two minus one:
 *   n = (2^q - 1).
 *   
 *   mbuf 池中的元素数。内存池的最佳大小（就内存使用而言）是当n = (2^q - 1)。
 *   
 * @param cache_size
 *   Size of the per-core object cache. See rte_mempool_create() for
 *   details.
 *   
 *   每个核心对象缓存的大小。有关详细信息，请参阅 rte_mempool_create()
 *   
 * @param priv_size
 *   Size of application private are between the rte_mbuf structure
 *   and the data buffer. This value must be aligned to RTE_MBUF_PRIV_ALIGN.
 *   
 *   应用程序在rte_mbuf结构和数据缓冲区之间的私有大小。此值必须与RTE_MBUF_PRIV_ALIGN对齐。
 *   
 * @param data_room_size
 *   Size of data buffer in each mbuf, including RTE_PKTMBUF_HEADROOM.
 *   
 *   每个mbuf中数据缓冲区的大小，包括 RTE_PKTMBUF_HEADROOM。
 *   
 * @param socket_id
 *   The socket identifier where the memory should be allocated. The
 *   value can be *SOCKET_ID_ANY* if there is no NUMA constraint for the
 *   reserved zone.
 *   
 *   应分配内存的套接字标识符。如果保留区域没有NUMA约束，则该值可以是SOCKET_ID_ANY。
 *   
 * @return
 *   The pointer to the new allocated mempool, on success. NULL on error
 *   成功时指向新分配的内存池的指针，错误时为NULL。
 *   
 *   with rte_errno set appropriately. Possible rte_errno values include:
 *   可能的 rte_errno 值包括：
 *    - E_RTE_NO_CONFIG - function could not get pointer to rte_config structure
 *      E_RTE_NO_CONFIG - 函数无法获得指向 rte_config 结构的指针
 *    - E_RTE_SECONDARY - function was called from a secondary process instance
 *    - E_RTE_SECONDARY - 从辅助进程实例调用函数
 *    - EINVAL - cache size provided is too large, or priv_size is not aligned.
 *    - EINVAL - 提供的缓存大小太大，或 priv_size 未对齐。
 *    - ENOSPC - the maximum number of memzones has already been allocated
 *    - ENOSPC - 已经分配了最大数量的memzone
 *    - EEXIST - a memzone with the same name already exists
 *    - EEXIST - 已存在同名的 memzone
 *    - ENOMEM - no appropriate memory area found in which to create memzone
 *    - ENOMEM - 没有找到合适的内存区域来创建 memzone
 */
struct rte_mempool *
rte_pktmbuf_pool_create(const char *name, unsigned n,
	unsigned cache_size, uint16_t priv_size, uint16_t data_room_size,
	int socket_id);
```

### rte_pktmbuf_pool_create_by_ops

```c

/**
 * Create a mbuf pool with a given mempool ops name
 * 
 * 使用给定的内存池操作名称创建一个mbuf池
 *
 * This function creates and initializes a packet mbuf pool. It is
 * a wrapper to rte_mempool functions.
 * 
 * 此函数创建并初始化一个数据包 mbuf 池。它是 rte_mempool 函数的包装器。
 *
 * @param name
 *   The name of the mbuf pool.
 *   
 *   mbuf 池的名称。
 *   
 * @param n
 *   The number of elements in the mbuf pool. The optimum size (in terms
 *   of memory usage) for a mempool is when n is a power of two minus one:
 *   n = (2^q - 1).
 *   
 *   mbuf 池中的元素数。内存池的最佳大小（就内存使用而言）是当n = (2^q - 1)。
 *   
 * @param cache_size
 *   Size of the per-core object cache. See rte_mempool_create() for
 *   details.
 *   
 *   每个核心对象缓存的大小。有关详细信息，请参阅 rte_mempool_create()
 *   
 * @param priv_size
 *   Size of application private are between the rte_mbuf structure
 *   and the data buffer. This value must be aligned to RTE_MBUF_PRIV_ALIGN.
 *   
 *   应用程序在rte_mbuf结构和数据缓冲区之间的私有大小。此值必须与RTE_MBUF_PRIV_ALIGN对齐。
 *   
 * @param data_room_size
 *   Size of data buffer in each mbuf, including RTE_PKTMBUF_HEADROOM.
 *   
 *   每个mbuf中数据缓冲区的大小，包括 RTE_PKTMBUF_HEADROOM。
 *   
 * @param socket_id
 *   The socket identifier where the memory should be allocated. The
 *   value can be *SOCKET_ID_ANY* if there is no NUMA constraint for the
 *   reserved zone.
 *   
 *   应分配内存的套接字标识符。如果保留区域没有NUMA约束，则该值可以是SOCKET_ID_ANY。
 *   
 * @param ops_name
 *   The mempool ops name to be used for this mempool instead of
 *   default mempool. The value can be *NULL* to use default mempool.
 *   
 *   用于此内存池的内存池操作名称。
 *   该值可以为 NULL 以使用默认内存池。
 *   
 * @return
 *   The pointer to the new allocated mempool, on success. NULL on error
 *   成功时指向新分配的内存池的指针。错误时为 NULL
 *   可能的 rte_errno 值包括：
 *    - E_RTE_NO_CONFIG - function could not get pointer to rte_config structure
 *      E_RTE_NO_CONFIG - 函数无法获得指向 rte_config 结构的指针
 *    - E_RTE_SECONDARY - function was called from a secondary process instance
 *    - E_RTE_SECONDARY - 从辅助进程实例调用函数
 *    - EINVAL - cache size provided is too large, or priv_size is not aligned.
 *    - EINVAL - 提供的缓存大小太大，或 priv_size 未对齐。
 *    - ENOSPC - the maximum number of memzones has already been allocated
 *    - ENOSPC - 已经分配了最大数量的memzone
 *    - EEXIST - a memzone with the same name already exists
 *    - EEXIST - 已存在同名的 memzone
 *    - ENOMEM - no appropriate memory area found in which to create memzone
 *    - ENOMEM - 没有找到合适的内存区域来创建 memzone
 */
struct rte_mempool *
rte_pktmbuf_pool_create_by_ops(const char *name, unsigned int n,
	unsigned int cache_size, uint16_t priv_size, uint16_t data_room_size,
	int socket_id, const char *ops_name);
```

### rte_pktmbuf_pool_create_extbuf

```c

/**
 * Create a mbuf pool with external pinned data buffers.
 * 
 * 创建一个带有外部固定数据缓冲区的 mbuf 池。
 *
 * This function creates and initializes a packet mbuf pool that contains
 * only mbufs with external buffer. It is a wrapper to rte_mempool functions.
 *
 * 此函数创建并初始化一个数据包 mbuf 池，该池仅包含带有外部缓冲区的 mbuf。
 * 它是 rte_mempool 函数的包装器。
 * 
 * @param name
 *   The name of the mbuf pool.
 *   
 *   mbuf 池的名称。
 *   
 * @param n
 *   The number of elements in the mbuf pool. The optimum size (in terms
 *   of memory usage) for a mempool is when n is a power of two minus one:
 *   n = (2^q - 1).
 *   
 *   mbuf 池中的元素数。内存池的最佳大小（就内存使用而言）是当n = (2^q - 1)。
 *   
 * @param cache_size
 *   Size of the per-core object cache. See rte_mempool_create() for
 *   details.
 *   
 *   每个核心对象缓存的大小。有关详细信息，请参阅 rte_mempool_create()
 *   
 * @param priv_size
 *   Size of application private are between the rte_mbuf structure
 *   and the data buffer. This value must be aligned to RTE_MBUF_PRIV_ALIGN.
 *   
 *   应用程序在rte_mbuf结构和数据缓冲区之间的私有大小。此值必须与RTE_MBUF_PRIV_ALIGN对齐。
 *   
 * @param data_room_size
 *   Size of data buffer in each mbuf, including RTE_PKTMBUF_HEADROOM.
 *   
 *   每个mbuf中数据缓冲区的大小，包括 RTE_PKTMBUF_HEADROOM。
 *   
 * @param socket_id
 *   The socket identifier where the memory should be allocated. The
 *   value can be *SOCKET_ID_ANY* if there is no NUMA constraint for the
 *   reserved zone.
 *   
 *   应分配内存的套接字标识符。如果保留区域没有NUMA约束，则该值可以是SOCKET_ID_ANY。
 *   
 * @param ext_mem
 *   Pointer to the array of structures describing the external memory
 *   for data buffers. It is caller responsibility to register this memory
 *   with rte_extmem_register() (if needed), map this memory to appropriate
 *   physical device, etc.
 *   
 *   指向描述数据缓冲区外部存储器的结构数组的指针。
 *   调用者负责使用 rte_extmem_register() 注册此内存（如果需要），将此内存映射到适当的物理设备等。
 *   
 * @param ext_num
 *   Number of elements in the ext_mem array.
 *   
 *   ext_mem 数组中的元素个数。
 *   
 * @return
 *   The pointer to the new allocated mempool, on success. NULL on error
 *   成功时指向新分配的内存池的指针。错误时为 NULL
 *   可能的 rte_errno 值包括：
 *    - E_RTE_NO_CONFIG - function could not get pointer to rte_config structure
 *      E_RTE_NO_CONFIG - 函数无法获得指向 rte_config 结构的指针
 *    - E_RTE_SECONDARY - function was called from a secondary process instance
 *    - E_RTE_SECONDARY - 从辅助进程实例调用函数
 *    - EINVAL - cache size provided is too large, or priv_size is not aligned.
 *    - EINVAL - 提供的缓存大小太大，或 priv_size 未对齐。
 *    - ENOSPC - the maximum number of memzones has already been allocated
 *    - ENOSPC - 已经分配了最大数量的memzone
 *    - EEXIST - a memzone with the same name already exists
 *    - EEXIST - 已存在同名的 memzone
 *    - ENOMEM - no appropriate memory area found in which to create memzone
 *    - ENOMEM - 没有找到合适的内存区域来创建 memzone
 */
__rte_experimental
struct rte_mempool *
rte_pktmbuf_pool_create_extbuf(const char *name, unsigned int n,
	unsigned int cache_size, uint16_t priv_size,
	uint16_t data_room_size, int socket_id,
	const struct rte_pktmbuf_extmem *ext_mem,
	unsigned int ext_num);
```

### rte_pktmbuf_data_room_size

```c
/**
 * Get the data room size of mbufs stored in a pktmbuf_pool
 * 
 * 获取存储在 pktmbuf_pool 中的 mbuf 的数据大小
 *
 * The data room size is the amount of data that can be stored in a
 * mbuf including the headroom (RTE_PKTMBUF_HEADROOM).
 * 
 * 数据大小是可以存储在mbuf中的数据量，包括headroom (RTE_PKTMBUF_HEADROOM)。
 *
 * @param mp
 *   The packet mbuf pool.
 *   数据包 mbuf 池。
 *   
 * @return
 *   The data room size of mbufs stored in this mempool.
 *   此内存池中存储的mbuf的数据大小。
 */
static inline uint16_t
rte_pktmbuf_data_room_size(struct rte_mempool *mp)
{
	struct rte_pktmbuf_pool_private *mbp_priv;

	mbp_priv = (struct rte_pktmbuf_pool_private *)rte_mempool_get_priv(mp);
	return mbp_priv->mbuf_data_room_size;
}
```


### rte_pktmbuf_priv_size

```c
/**
 * Get the application private size of mbufs stored in a pktmbuf_pool
 * 
 * 获取存储在 pktmbuf_pool 中mbuf的应用程序私有大小
 *
 * The private size of mbuf is a zone located between the rte_mbuf
 * structure and the data buffer where an application can store data
 * associated to a packet.
 * 
 * mbuf 的私有大小是位于 rte_mbuf 结构和数据缓冲区之间的区域，
 * 应用程序可以在其中存储与数据包关联的数据。
 *
 * @param mp
 *   The packet mbuf pool.
 *   数据包 mbuf 池。
 * @return
 *   The private size of mbufs stored in this mempool.
 *   存储在此内存池中的 mbuf 的私有大小。
 */
static inline uint16_t
rte_pktmbuf_priv_size(struct rte_mempool *mp)
{
	struct rte_pktmbuf_pool_private *mbp_priv;

	mbp_priv = (struct rte_pktmbuf_pool_private *)rte_mempool_get_priv(mp);
	return mbp_priv->mbuf_priv_size;
}
```


### rte_pktmbuf_reset_headroom

```c
/**
 * Reset the data_off field of a packet mbuf to its default value.
 * 
 * 将数据包 mbuf 的 data_off 字段重置为其默认值。
 *
 * The given mbuf must have only one segment, which should be empty.
 * 
 * 给定的 mbuf 必须只有一个段，该段应为空。
 *
 * @param m
 *   The packet mbuf's data_off field has to be reset.
 *   重置data_off字段的mbuf。
 */
static inline void rte_pktmbuf_reset_headroom(struct rte_mbuf *m)
{
	m->data_off = (uint16_t)RTE_MIN((uint16_t)RTE_PKTMBUF_HEADROOM,
					(uint16_t)m->buf_len);
}
```

### rte_pktmbuf_reset

```c
/**
 * Reset the fields of a packet mbuf to their default values.
 * 
 * 将数据包 mbuf 的字段重置为其默认值。
 *
 * The given mbuf must have only one segment.
 * 
 * 给定的 mbuf 必须只有一个段。
 *
 * @param m
 *   The packet mbuf to be reset.
 *   要重置的数据包 mbuf。
 */
static inline void rte_pktmbuf_reset(struct rte_mbuf *m)
{
	m->next = NULL;
	m->pkt_len = 0;
	m->tx_offload = 0;
	m->vlan_tci = 0;
	m->vlan_tci_outer = 0;
	m->nb_segs = 1;
	m->port = RTE_MBUF_PORT_INVALID;

	m->ol_flags &= EXT_ATTACHED_MBUF;
	m->packet_type = 0;
	rte_pktmbuf_reset_headroom(m);

	m->data_len = 0;
	__rte_mbuf_sanity_check(m, 1);
}
```


### rte_pktmbuf_alloc

```c
/**
 * Allocate a new mbuf from a mempool.
 * 
 * 从内存池中分配一个新的 mbuf。
 *
 * This new mbuf contains one segment, which has a length of 0. The pointer
 * to data is initialized to have some bytes of headroom in the buffer
 * (if buffer size allows).
 * 
 * 这个新的 mbuf 包含一个长度为 0 的段。
 * 指向数据的指针被初始化为在缓冲区中有一些字节的空间（如果缓冲区大小允许）。
 *
 * @param mp
 *   The mempool from which the mbuf is allocated.
 *   从中分配 mbuf 的内存池。
 * @return
 *   - The pointer to the new mbuf on success.
 *   - 成功时指向新 mbuf 的指针。
 *   - NULL if allocation failed.
 *   - 如果分配失败，则为 NULL。
 */
static inline struct rte_mbuf *rte_pktmbuf_alloc(struct rte_mempool *mp)
{
	struct rte_mbuf *m;
	if ((m = rte_mbuf_raw_alloc(mp)) != NULL)
		rte_pktmbuf_reset(m);
	return m;
}
```

### rte_pktmbuf_alloc_bulk

```c
/**
 * Allocate a bulk of mbufs, initialize refcnt and reset the fields to default
 * values.
 * 
 * 分配大量 mbuf，初始化 refcnt 并将字段重置为默认值。
 *
 *  @param pool
 *    The mempool from which mbufs are allocated.
 *    从中分配 mbuf 的内存池。
 *  @param mbufs
 *    Array of pointers to mbufs
 *    指向mbufs的指针数组
 *  @param count
 *    Array size
 *    数组大小
 *  @return
 *   - 0: Success
 *   - 0表示成功
 *   - -ENOENT: Not enough entries in the mempool; no mbufs are retrieved.
 *   - -ENOENT: 内存池中没有足够的条目；未生成任何 mbuf。
 */
static inline int rte_pktmbuf_alloc_bulk(struct rte_mempool *pool,
	 struct rte_mbuf **mbufs, unsigned count)
{
	unsigned idx = 0;
	int rc;

	rc = rte_mempool_get_bulk(pool, (void **)mbufs, count);
	if (unlikely(rc))
		return rc;

	/* To understand duff's device on loop unwinding optimization, see
	 * https://en.wikipedia.org/wiki/Duff's_device.
	 * Here while() loop is used rather than do() while{} to avoid extra
	 * check if count is zero.
	 */
	switch (count % 4) {
	case 0:
		while (idx != count) {
			__rte_mbuf_raw_sanity_check(mbufs[idx]);
			rte_pktmbuf_reset(mbufs[idx]);
			idx++;
			/* fall-through */
	case 3:
			__rte_mbuf_raw_sanity_check(mbufs[idx]);
			rte_pktmbuf_reset(mbufs[idx]);
			idx++;
			/* fall-through */
	case 2:
			__rte_mbuf_raw_sanity_check(mbufs[idx]);
			rte_pktmbuf_reset(mbufs[idx]);
			idx++;
			/* fall-through */
	case 1:
			__rte_mbuf_raw_sanity_check(mbufs[idx]);
			rte_pktmbuf_reset(mbufs[idx]);
			idx++;
			/* fall-through */
		}
	}
	return 0;
}
```


### rte_pktmbuf_ext_shinfo_init_helper

```c

/**
 * Initialize shared data at the end of an external buffer before attaching
 * to a mbuf by ``rte_pktmbuf_attach_extbuf()``. This is not a mandatory
 * initialization but a helper function to simply spare a few bytes at the
 * end of the buffer for shared data. If shared data is allocated
 * separately, this should not be called but application has to properly
 * initialize the shared data according to its need.
 * 
 * 在通过 rte_pktmbuf_attach_extbuf() 附加到 mbuf 之前，在外部缓冲区的末尾初始化共享数据。
 * 这不是强制初始化，而是一个辅助函数，用于在缓冲区末尾为共享数据简单地保留几个字节。
 * 如果共享数据是单独分配的，则不应调用，但应用程序必须根据需要正确初始化共享数据。
 *
 * Free callback and its argument is saved and the refcnt is set to 1.
 * 
 * 释放回调及其参数被保存，并且 refcnt 设置为 1。
 *
 * @warning
 * The value of buf_len will be reduced to RTE_PTR_DIFF(shinfo, buf_addr)
 * after this initialization. This shall be used for
 * ``rte_pktmbuf_attach_extbuf()``
 * 
 * @warning 
 * 此初始化后，buf_len 的值将减少为 RTE_PTR_DIFF(shinfo, buf_addr)。
 * 这将用于“rte_pktmbuf_attach_extbuf()”
 *
 * @param buf_addr
 *   The pointer to the external buffer.
 *   
 *   指向外部缓冲区的指针。
 *   
 * @param [in,out] buf_len
 *   The pointer to length of the external buffer. Input value must be
 *   larger than the size of ``struct rte_mbuf_ext_shared_info`` and
 *   padding for alignment. If not enough, this function will return NULL.
 *   Adjusted buffer length will be returned through this pointer.
 *   
 *   指向外部缓冲区长度的指针。输入值必须大于“struct rte_mbuf_ext_shared_info”的大小和对齐的填充。
 *   如果不够，此函数将返回 NULL。调整后的缓冲区长度将通过该指针返回。
 *   
 * @param free_cb
 *   Free callback function to call when the external buffer needs to be
 *   freed.
 *   
 *   需要释放外部缓冲区时调用的释放回调函数。
 *   
 * @param fcb_opaque
 *   Argument for the free callback function.
 *   
 *   释放回调函数的参数。
 *
 * @return
 *   A pointer to the initialized shared data on success, return NULL
 *   otherwise.
 *   
 *   成功时指向已初始化共享数据的指针，否则返回 NULL。
 */
static inline struct rte_mbuf_ext_shared_info *
rte_pktmbuf_ext_shinfo_init_helper(void *buf_addr, uint16_t *buf_len,
	rte_mbuf_extbuf_free_callback_t free_cb, void *fcb_opaque)
{
	struct rte_mbuf_ext_shared_info *shinfo;
	void *buf_end = RTE_PTR_ADD(buf_addr, *buf_len);
	void *addr;

	addr = RTE_PTR_ALIGN_FLOOR(RTE_PTR_SUB(buf_end, sizeof(*shinfo)),
				   sizeof(uintptr_t));
	if (addr <= buf_addr)
		return NULL;

	shinfo = (struct rte_mbuf_ext_shared_info *)addr;
	shinfo->free_cb = free_cb;
	shinfo->fcb_opaque = fcb_opaque;
	rte_mbuf_ext_refcnt_set(shinfo, 1);

	*buf_len = (uint16_t)RTE_PTR_DIFF(shinfo, buf_addr);
	return shinfo;
}
```

### rte_pktmbuf_attach_extbuf

```c
/**
 * Attach an external buffer to a mbuf.
 * 
 * 将外部缓冲区附加到 mbuf。
 *
 * User-managed anonymous buffer can be attached to an mbuf. When attaching
 * it, corresponding free callback function and its argument should be
 * provided via shinfo. This callback function will be called once all the
 * mbufs are detached from the buffer (refcnt becomes zero).
 * 
 * 用户管理的匿名缓冲区可以附加到 mbuf。附加时，需要通过 shinfo 提供相应的释放回调函数及其参数。
 * 一旦所有 mbuf 都从缓冲区中分离出来（refcnt 变为零），就会调用这个回调函数。
 *
 * The headroom length of the attaching mbuf will be set to zero and this
 * can be properly adjusted after attachment. For example, ``rte_pktmbuf_adj()``
 * or ``rte_pktmbuf_reset_headroom()`` might be used.
 * 
 * 附加 mbuf 的headroom长度将设置为零，并且可以在附加后适当调整。例如，可以使用``rte_pktmbuf_adj()`` 
 * 或 ``rte_pktmbuf_reset_headroom()``。
 *
 * Similarly, the packet length is initialized to 0. If the buffer contains
 * data, the user has to adjust ``data_len`` and the ``pkt_len`` field of
 * the mbuf accordingly.
 * 
 * 同样，数据包长度初始化为 0。如果缓冲区包含数据，
 * 用户必须相应调整 mbuf 的“data_len”和“pkt_len”字段。
 *
 * More mbufs can be attached to the same external buffer by
 * ``rte_pktmbuf_attach()`` once the external buffer has been attached by
 * this API.
 * 
 * 一旦外部缓冲区被此 API 附加，更多的 mbuf 可以通过 ``rte_pktmbuf_attach()`` 附加到同一个外部缓冲区。
 *
 * Detachment can be done by either ``rte_pktmbuf_detach_extbuf()`` or
 * ``rte_pktmbuf_detach()``.
 * 
 * 分离可以通过“rte_pktmbuf_detach_extbuf()”或“rte_pktmbuf_detach()”来完成
 *
 * Memory for shared data must be provided and user must initialize all of
 * the content properly, especially free callback and refcnt. The pointer
 * of shared data will be stored in m->shinfo.
 * 
 * 必须为共享数据提供内存，并且用户必须正确初始化所有内容，尤其是释放回调和 refcnt。
 * 共享数据的指针将存储在 m->shinfo 中。
 * 
 * ``rte_pktmbuf_ext_shinfo_init_helper`` can help to simply spare a few
 * bytes at the end of buffer for the shared data, store free callback and
 * its argument and set the refcnt to 1. The following is an example:
 * 
 * ``rte_pktmbuf_ext_shinfo_init_helper`` 可以帮助简单地在缓冲区末尾为共享数据预留几个字节，
 * 存储释放回调及其参数并将 refcnt 设置为 1。以下是示例：
 *
 *   struct rte_mbuf_ext_shared_info *shinfo =
 *          rte_pktmbuf_ext_shinfo_init_helper(buf_addr, &buf_len,
 *                                             free_cb, fcb_arg);
 *   rte_pktmbuf_attach_extbuf(m, buf_addr, buf_iova, buf_len, shinfo);
 *   rte_pktmbuf_reset_headroom(m);
 *   rte_pktmbuf_adj(m, data_len);
 *
 * Attaching an external buffer is quite similar to mbuf indirection in
 * replacing buffer addresses and length of a mbuf, but a few differences:
 * - When an indirect mbuf is attached, refcnt of the direct mbuf would be
 *   2 as long as the direct mbuf itself isn't freed after the attachment.
 *   In such cases, the buffer area of a direct mbuf must be read-only. But
 *   external buffer has its own refcnt and it starts from 1. Unless
 *   multiple mbufs are attached to a mbuf having an external buffer, the
 *   external buffer is writable.
 * - There's no need to allocate buffer from a mempool. Any buffer can be
 *   attached with appropriate free callback and its IO address.
 * - Smaller metadata is required to maintain shared data such as refcnt.
 * 
 * 附加外部缓冲区与 mbuf 间接缓冲区地址和长度非常相似，但有一些区别：
 * - 附加间接 mbuf 时，只要在附加后未释放直接 mbuf 本身，则直接 mbuf 的 refcnt 将为 2。
 *   在这种情况下，直接 mbuf 的缓冲区必须是只读的。但是外部缓冲区有自己的 refcnt，它从 1 开始。
 *   除非多个 mbuf 附加到具有外部缓冲区的 mbuf，否则外部缓冲区是可写的。
 * - 无需从内存池分配缓冲区。任何缓冲区都可以附加适当的释放回调及其IO地址。
 * - 需要更小的元数据来维护共享数据，例如 refcnt。
 * 
 * @param m
 *   The pointer to the mbuf.
 *   指向 mbuf 的指针。
 * @param buf_addr
 *   The pointer to the external buffer.
 *   指向外部缓冲区的指针。
 * @param buf_iova
 *   IO address of the external buffer.
 *   外部缓冲区的 IO 地址。
 * @param buf_len
 *   The size of the external buffer.
 *   外部缓冲区的大小。
 * @param shinfo
 *   User-provided memory for shared data of the external buffer.
 *   用户提供的内存，用于外部缓冲区的共享数据。
 */
static inline void
rte_pktmbuf_attach_extbuf(struct rte_mbuf *m, void *buf_addr,
	rte_iova_t buf_iova, uint16_t buf_len,
	struct rte_mbuf_ext_shared_info *shinfo)
{
	/* mbuf should not be read-only */
	RTE_ASSERT(RTE_MBUF_DIRECT(m) && rte_mbuf_refcnt_read(m) == 1);
	RTE_ASSERT(shinfo->free_cb != NULL);

	m->buf_addr = buf_addr;
	m->buf_iova = buf_iova;
	m->buf_len = buf_len;

	m->data_len = 0;
	m->data_off = 0;

	m->ol_flags |= EXT_ATTACHED_MBUF;
	m->shinfo = shinfo;
}
```

### rte_mbuf_dynfield_copy

```c
/**
 * Copy dynamic fields from msrc to mdst.
 * 
 * 将动态字段从 msrc 复制到 mdst。
 *
 * @param mdst
 *   The destination mbuf.
 *   目标mbuf
 * @param msrc
 *   The source mbuf.
 *   源mbuf
 */
static inline void
rte_mbuf_dynfield_copy(struct rte_mbuf *mdst, const struct rte_mbuf *msrc)
{
	memcpy(&mdst->dynfield1, msrc->dynfield1, sizeof(mdst->dynfield1));
}
```

### rte_pktmbuf_attach

```c
/**
 * Attach packet mbuf to another packet mbuf.
 * 
 * 将数据包 mbuf 附加到另一个数据包 mbuf。
 *
 * If the mbuf we are attaching to isn't a direct buffer and is attached to
 * an external buffer, the mbuf being attached will be attached to the
 * external buffer instead of mbuf indirection.
 * 
 * 如果我们附加的 mbuf 不是直接缓冲区并且附加到了外部缓冲区，
 * 则附加的mbuf将附加到外部缓冲区而不是间接缓冲区。
 *
 * Otherwise, the mbuf will be indirectly attached. After attachment we
 * refer the mbuf we attached as 'indirect', while mbuf we attached to as
 * 'direct'.  The direct mbuf's reference counter is incremented.
 * 
 * 否则，mbuf将被间接附加。附加后，我们将附加的mbuf称为“间接”，
 * 而将附加的mbuf称为“直接”。直接mbuf的引用计数器递增。
 *
 * Right now, not supported:
 *  - attachment for already indirect mbuf (e.g. - mi has to be direct).
 *  - mbuf we trying to attach (mi) is used by someone else
 *    e.g. it's reference counter is greater then 1.
 *    
 * 目前，不支持：
 *  - 附加已经是间接的mbuf（例如 - mi 必须是直接的）。
 *  - 我们试图附加的mbuf（mi）被其他人使用，例如它的参考计数器大于 1。
 *
 * @param mi
 *   The indirect packet mbuf.
 *   间接数据包mbuf。
 * @param m
 *   The packet mbuf we're attaching to.
 *   我们要附加到的数据包 mbuf。
 */
static inline void rte_pktmbuf_attach(struct rte_mbuf *mi, struct rte_mbuf *m)
{
	RTE_ASSERT(RTE_MBUF_DIRECT(mi) &&
	    rte_mbuf_refcnt_read(mi) == 1);

	if (RTE_MBUF_HAS_EXTBUF(m)) {
		rte_mbuf_ext_refcnt_update(m->shinfo, 1);
		mi->ol_flags = m->ol_flags;
		mi->shinfo = m->shinfo;
	} else {
		/* if m is not direct, get the mbuf that embeds the data */
		rte_mbuf_refcnt_update(rte_mbuf_from_indirect(m), 1);
		mi->priv_size = m->priv_size;
		mi->ol_flags = m->ol_flags | IND_ATTACHED_MBUF;
	}

	__rte_pktmbuf_copy_hdr(mi, m);

	mi->data_off = m->data_off;
	mi->data_len = m->data_len;
	mi->buf_iova = m->buf_iova;
	mi->buf_addr = m->buf_addr;
	mi->buf_len = m->buf_len;

	mi->next = NULL;
	mi->pkt_len = mi->data_len;
	mi->nb_segs = 1;

	__rte_mbuf_sanity_check(mi, 1);
	__rte_mbuf_sanity_check(m, 0);
}
```


### rte_pktmbuf_detach

```c

/**
 * Detach a packet mbuf from external buffer or direct buffer.
 * 
 * 从外部缓冲区或直接缓冲区中分离数据包 mbuf。
 *
 *  - decrement refcnt and free the external/direct buffer if refcnt
 *    becomes zero.
 *    
 *    如果 refcnt 变为零，则减少 refcnt 并释放 externaldirect 缓冲区。
 *    
 *  - restore original mbuf address and length values.
 *  
 *    恢复原始 mbuf 地址和长度值。
 *    
 *  - reset pktmbuf data and data_len to their default values.
 *  
 *    将 pktmbuf 数据和 data_len 重置为其默认值。
 *
 * All other fields of the given packet mbuf will be left intact.
 * 
 * 给定数据包 mbuf 的所有其他字段将保持不变。
 *
 * If the packet mbuf was allocated from the pool with pinned
 * external buffers the rte_pktmbuf_detach does nothing with the
 * mbuf of this kind, because the pinned buffers are not supposed
 * to be detached.
 * 
 * 如果数据包 mbuf 是从带有固定外部缓冲区的池中分配的，则 rte_pktmbuf_detach 
 * 不会对此类 mbuf 执行任何操作，因为不应该分离固定缓冲区。
 *
 * @param m
 *   The indirect attached packet mbuf.
 *   间接附加的数据包mbuf。
 */
static inline void rte_pktmbuf_detach(struct rte_mbuf *m)
{
	struct rte_mempool *mp = m->pool;
	uint32_t mbuf_size, buf_len;
	uint16_t priv_size;

	if (RTE_MBUF_HAS_EXTBUF(m)) {
		/*
		 * The mbuf has the external attached buffer,
		 * we should check the type of the memory pool where
		 * the mbuf was allocated from to detect the pinned
		 * external buffer.
		 */
		uint32_t flags = rte_pktmbuf_priv_flags(mp);

		if (flags & RTE_PKTMBUF_POOL_F_PINNED_EXT_BUF) {
			/*
			 * The pinned external buffer should not be
			 * detached from its backing mbuf, just exit.
			 */
			return;
		}
		__rte_pktmbuf_free_extbuf(m);
	} else {
		__rte_pktmbuf_free_direct(m);
	}
	priv_size = rte_pktmbuf_priv_size(mp);
	mbuf_size = (uint32_t)(sizeof(struct rte_mbuf) + priv_size);
	buf_len = rte_pktmbuf_data_room_size(mp);

	m->priv_size = priv_size;
	m->buf_addr = (char *)m + mbuf_size;
	m->buf_iova = rte_mempool_virt2iova(m) + mbuf_size;
	m->buf_len = (uint16_t)buf_len;
	rte_pktmbuf_reset_headroom(m);
	m->data_len = 0;
	m->ol_flags = 0;
}
```


### rte_pktmbuf_prefree_seg

```c
/**
 * Decrease reference counter and unlink a mbuf segment
 * 
 * 减少引用计数器并取消链接 mbuf 段
 *
 * This function does the same than a free, except that it does not
 * return the segment to its pool.
 * 
 * 此函数的作用与 free 相同，只是它不将段返回到其池中。
 * 
 * It decreases the reference counter, and if it reaches 0, it is
 * detached from its parent for an indirect mbuf.
 * 
 * 它减少了引用计数器，如果它达到 0，它就会从它的父级中分离出来，以获得一个间接的 mbuf。
 *
 * @param m
 *   The mbuf to be unlinked
 *   要取消链接的 mbuf
 * @return
 *   - (m) if it is the last reference. It can be recycled or freed.
 *   - (m) 返回m,如果它是最后一个引用。它可以回收或释放。
 *   - (NULL) if the mbuf still has remaining references on it.
 *   - (NULL) 如果 mbuf 上仍有剩余引用，返回NULL。
 */
static __rte_always_inline struct rte_mbuf *
rte_pktmbuf_prefree_seg(struct rte_mbuf *m)
{
	__rte_mbuf_sanity_check(m, 0);

	if (likely(rte_mbuf_refcnt_read(m) == 1)) {

		if (!RTE_MBUF_DIRECT(m)) {
			rte_pktmbuf_detach(m);
			if (RTE_MBUF_HAS_EXTBUF(m) &&
			    RTE_MBUF_HAS_PINNED_EXTBUF(m) &&
			    __rte_pktmbuf_pinned_extbuf_decref(m))
				return NULL;
		}

		if (m->next != NULL) {
			m->next = NULL;
			m->nb_segs = 1;
		}

		return m;

	} else if (__rte_mbuf_refcnt_update(m, -1) == 0) {

		if (!RTE_MBUF_DIRECT(m)) {
			rte_pktmbuf_detach(m);
			if (RTE_MBUF_HAS_EXTBUF(m) &&
			    RTE_MBUF_HAS_PINNED_EXTBUF(m) &&
			    __rte_pktmbuf_pinned_extbuf_decref(m))
				return NULL;
		}

		if (m->next != NULL) {
			m->next = NULL;
			m->nb_segs = 1;
		}
		rte_mbuf_refcnt_set(m, 1);

		return m;
	}
	return NULL;
}
```

### rte_pktmbuf_free_seg

```c
/**
 * Free a segment of a packet mbuf into its original mempool.
 * 
 * 将数据包 mbuf 的一部分释放到其原始内存池中。
 *
 * Free an mbuf, without parsing other segments in case of chained
 * buffers.
 * 
 * 释放一个 mbuf，在链式缓冲区的情况下不解析其他段。
 *
 * @param m
 *   The packet mbuf segment to be freed.
 *   要释放的数据包 mbuf 段。
 *   
 */
static __rte_always_inline void
rte_pktmbuf_free_seg(struct rte_mbuf *m)
{
	m = rte_pktmbuf_prefree_seg(m);
	if (likely(m != NULL))
		rte_mbuf_raw_free(m);
}
```

### rte_pktmbuf_free

```c
/**
 * Free a packet mbuf back into its original mempool.
 * 
 * 将数据包 mbuf 释放回其原始内存池。
 *
 * Free an mbuf, and all its segments in case of chained buffers. Each
 * segment is added back into its original mempool.
 * 
 * 如果是链式缓冲区，则释放 mbuf 及其所有段。每个段都被添加回其原始内存池。
 *
 * @param m
 *   The packet mbuf to be freed. If NULL, the function does nothing.
 *   要释放的数据包 mbuf。如果为 NULL，则该函数不执行任何操作。
 */
static inline void rte_pktmbuf_free(struct rte_mbuf *m)
{
	struct rte_mbuf *m_next;

	if (m != NULL)
		__rte_mbuf_sanity_check(m, 1);

	while (m != NULL) {
		m_next = m->next;
		rte_pktmbuf_free_seg(m);
		m = m_next;
	}
}
```

### rte_pktmbuf_free_bulk

```c
/**
 * Free a bulk of packet mbufs back into their original mempools.
 * 
 * 将大量数据包 mbuf 释放回其原始内存池。
 *
 * Free a bulk of mbufs, and all their segments in case of chained buffers.
 * Each segment is added back into its original mempool.
 * 
 * 在链式缓冲区的情况下，释放大量 mbuf 及其所有段。每个段都被添加回其原始内存池。
 *
 *  @param mbufs
 *    Array of pointers to packet mbufs.
 *    The array may contain NULL pointers.
 *    指向数据包 mbuf 的指针数组。数组可能包含 NULL 指针。
 *  @param count
 *    Array size.
 *    数组的大小
 */
__rte_experimental
void rte_pktmbuf_free_bulk(struct rte_mbuf **mbufs, unsigned int count);
```


### rte_pktmbuf_clone

```c
/**
 * Create a "clone" of the given packet mbuf.
 * 
 * 创建给定数据包 mbuf 的“克隆”。
 *
 * Walks through all segments of the given packet mbuf, and for each of them:
 *  - Creates a new packet mbuf from the given pool.
 *  - Attaches newly created mbuf to the segment.
 * Then updates pkt_len and nb_segs of the "clone" packet mbuf to match values
 * from the original packet mbuf.
 * 
 * 遍历给定数据包 mbuf 的所有段，并针对每个段：
 *  - 从给定的池中创建一个新的数据包 mbuf。
 *  - 将新创建的 mbuf 附加到段。
 * 然后更新“克隆”数据包 mbuf 的 pkt_len 和 nb_segs 以匹配来自原始数据包 mbuf 的值。
 *
 * @param md
 *   The packet mbuf to be cloned.
 *   要克隆的数据包 mbuf。
 * @param mp
 *   The mempool from which the "clone" mbufs are allocated.
 *   从中分配“克隆”mbuf 的内存池。
 * @return
 *   - The pointer to the new "clone" mbuf on success.
 *   成功时指向新“克隆”mbuf 的指针。
 *   - NULL if allocation fails.
 *   失败返回NULL
 */
struct rte_mbuf *
rte_pktmbuf_clone(struct rte_mbuf *md, struct rte_mempool *mp);
```

### rte_pktmbuf_copy

```c
/**
 * Create a full copy of a given packet mbuf.
 * 
 * 创建给定数据包 mbuf 的完整副本。
 *
 * Copies all the data from a given packet mbuf to a newly allocated
 * set of mbufs. The private data are is not copied.
 * 
 * 将给定数据包 mbuf 中的所有数据复制到新分配的一组 mbuf。不复制私有数据。
 *
 * @param m
 *   The packet mbuf to be copiedd.
 *   要复制的数据包 mbuf。
 * @param mp
 *   The mempool from which the "clone" mbufs are allocated.
 *   从中分配“克隆”mbuf 的内存池。
 * @param offset
 *   The number of bytes to skip before copying.
 *   复制前要跳过的字节数。
 *   If the mbuf does not have that many bytes, it is an error
 *   and NULL is returned.
 *   如果 mbuf 没有那么多字节，则为错误并返回 NULL。
 * @param length
 *   The upper limit on bytes to copy.  Passing UINT32_MAX
 *   means all data (after offset).
 *   要复制的字节数上限。传递 UINT32_MAX 表示所有数据（偏移后）。
 * @return
 *   - The pointer to the new "clone" mbuf on success.
 *   成功时指向新“拷贝”mbuf 的指针。
 *   - NULL if allocation fails.
 *   失败返回NULL
 */
__rte_experimental
struct rte_mbuf *
rte_pktmbuf_copy(const struct rte_mbuf *m, struct rte_mempool *mp,
		 uint32_t offset, uint32_t length);
```


### rte_pktmbuf_refcnt_update

```c
/**
 * Adds given value to the refcnt of all packet mbuf segments.
 * 
 * 将给定值增加到所有数据包mbuf段的refcnt。
 *
 * Walks through all segments of given packet mbuf and for each of them
 * invokes rte_mbuf_refcnt_update().
 * 
 * 遍历给定数据包mbuf的所有段，并为每个段调用 rte_mbuf_refcnt_update()。
 *
 * @param m
 *   The packet mbuf whose refcnt to be updated.
 *   要更新其 refcnt 的数据包 mbuf。
 * @param v
 *   The value to add to the mbuf's segments refcnt.
 *   增加到 mbuf 的段 refcnt 的值。
 */
static inline void rte_pktmbuf_refcnt_update(struct rte_mbuf *m, int16_t v)
{
	__rte_mbuf_sanity_check(m, 1);

	do {
		rte_mbuf_refcnt_update(m, v);
	} while ((m = m->next) != NULL);
}
```


### rte_pktmbuf_headroom

```c
/**
 * Get the headroom in a packet mbuf.
 * 
 * 获取数据包 mbuf 中的headroom的大小。
 *
 * @param m
 *   The packet mbuf.
 *   数据包mbuf。
 * @return
 *   The length of the headroom.
 *   headroom的长度
 */
static inline uint16_t rte_pktmbuf_headroom(const struct rte_mbuf *m)
{
	__rte_mbuf_sanity_check(m, 0);
	return m->data_off;
}
```


### rte_pktmbuf_tailroom

```c
/**
 * Get the tailroom of a packet mbuf.
 * 
 * 获取数据包mbuf的tailroom大小。
 *
 * @param m
 *   The packet mbuf.
 *   数据包mbuf。
 * @return
 *   The length of the tailroom.
 *   tailroom大小
 */
static inline uint16_t rte_pktmbuf_tailroom(const struct rte_mbuf *m)
{
	__rte_mbuf_sanity_check(m, 0);
	return (uint16_t)(m->buf_len - rte_pktmbuf_headroom(m) -
			  m->data_len);
}
```


### rte_pktmbuf_lastseg

```c
/**
 * Get the last segment of the packet.
 * 
 * 获取数据包的最后一段。
 *
 * @param m
 *   The packet mbuf.
 *   数据包mbuf
 * @return
 *   The last segment of the given mbuf.
 *   给定 mbuf 的最后一段。
 */
static inline struct rte_mbuf *rte_pktmbuf_lastseg(struct rte_mbuf *m)
{
	__rte_mbuf_sanity_check(m, 1);
	while (m->next != NULL)
		m = m->next;
	return m;
}
```


### rte_pktmbuf_prepend


```c
/**
 * Prepend len bytes to an mbuf data area.
 * 
 * 将 len 个字节添加到 mbuf 数据区。
 *
 * Returns a pointer to the new
 * data start address. If there is not enough headroom in the first
 * segment, the function will return NULL, without modifying the mbuf.
 * 
 * 返回指向新数据起始地址的指针。如果第一段没有足够的空间，函数将返回NULL，而不修改mbuf。
 *
 * @param m
 *   The pkt mbuf.
 *   要操作的mbuf
 * @param len
 *   The amount of data to prepend (in bytes).
 *   要添加的数据量（以字节为单位）。
 * @return
 *   A pointer to the start of the newly prepended data, or
 *   NULL if there is not enough headroom space in the first segment
 *   
 *   指向新添加数据开头的指针，如果第一个段中没有足够的headroom，则为 NULL
 */
static inline char *rte_pktmbuf_prepend(struct rte_mbuf *m,
					uint16_t len)
{
	__rte_mbuf_sanity_check(m, 1);

	if (unlikely(len > rte_pktmbuf_headroom(m)))
		return NULL;

	/* NB: elaborating the subtraction like this instead of using
	 *     -= allows us to ensure the result type is uint16_t
	 *     avoiding compiler warnings on gcc 8.1 at least */
	m->data_off = (uint16_t)(m->data_off - len);
	m->data_len = (uint16_t)(m->data_len + len);
	m->pkt_len  = (m->pkt_len + len);

	return (char *)m->buf_addr + m->data_off;
}
```


### rte_pktmbuf_append

```c
/**
 * Append len bytes to an mbuf.
 * 
 * 将 len 个字节附加到 mbuf。
 *
 * Append len bytes to an mbuf and return a pointer to the start address
 * of the added data. If there is not enough tailroom in the last
 * segment, the function will return NULL, without modifying the mbuf.
 * 
 * 将 len 个字节附加到 mbuf 并返回指向添加数据的起始地址的指针。
 * 如果最后一段没有足够的tailroom，该函数将返回NULL，而不修改mbuf。
 *
 * @param m
 *   The packet mbuf.
 *   要操作的mbuf
 * @param len
 *   The amount of data to append (in bytes).
 *   要附加的数据量（以字节为单位）。
 * @return
 *   A pointer to the start of the newly appended data, or
 *   NULL if there is not enough tailroom space in the last segment
 *   
 *   指向新附加数据开头的指针，如果最后一段中没有足够的tailroom，则为 NULL
 */
static inline char *rte_pktmbuf_append(struct rte_mbuf *m, uint16_t len)
{
	void *tail;
	struct rte_mbuf *m_last;

	__rte_mbuf_sanity_check(m, 1);

	m_last = rte_pktmbuf_lastseg(m);
	if (unlikely(len > rte_pktmbuf_tailroom(m_last)))
		return NULL;

	tail = (char *)m_last->buf_addr + m_last->data_off + m_last->data_len;
	m_last->data_len = (uint16_t)(m_last->data_len + len);
	m->pkt_len  = (m->pkt_len + len);
	return (char*) tail;
}
```


### rte_pktmbuf_adj

```c
/**
 * Remove len bytes at the beginning of an mbuf.
 * 
 * 删除 mbuf 开头的 len 个字节。
 *
 * Returns a pointer to the start address of the new data area. If the
 * length is greater than the length of the first segment, then the
 * function will fail and return NULL, without modifying the mbuf.
 * 
 * 返回指向新数据区起始地址的指针。如果长度大于第一个段的长度，
 * 则函数将失败并返回 NULL，而不修改 mbuf。
 *
 * @param m
 *   The packet mbuf.
 *   要操作的mbuf
 * @param len
 *   The amount of data to remove (in bytes).
 *   要删除的数据量（以字节为单位）。
 * @return
 *   A pointer to the new start of the data.
 *   指向数据新起点的指针。
 */
static inline char *rte_pktmbuf_adj(struct rte_mbuf *m, uint16_t len)
{
	__rte_mbuf_sanity_check(m, 1);

	if (unlikely(len > m->data_len))
		return NULL;

	/* NB: elaborating the addition like this instead of using
	 *     += allows us to ensure the result type is uint16_t
	 *     avoiding compiler warnings on gcc 8.1 at least */
	m->data_len = (uint16_t)(m->data_len - len);
	m->data_off = (uint16_t)(m->data_off + len);
	m->pkt_len  = (m->pkt_len - len);
	return (char *)m->buf_addr + m->data_off;
}
```


### rte_pktmbuf_trim

```c
/**
 * Remove len bytes of data at the end of the mbuf.
 * 
 * 删除 mbuf 末尾的 len 个字节的数据。
 *
 * If the length is greater than the length of the last segment, the
 * function will fail and return -1 without modifying the mbuf.
 * 
 * 如果长度大于最后一段的长度，函数将失败并返回-1而不修改mbuf。
 *
 * @param m
 *   The packet mbuf.
 *   要操作的mbuf
 * @param len
 *   The amount of data to remove (in bytes).
 *   要删除的数据量（以字节为单位）。
 * @return
 *   - 0: On success.
 *   - 0: 成功
 *   - -1: On error.
 *   - -1：失败
 */
static inline int rte_pktmbuf_trim(struct rte_mbuf *m, uint16_t len)
{
	struct rte_mbuf *m_last;

	__rte_mbuf_sanity_check(m, 1);

	m_last = rte_pktmbuf_lastseg(m);
	if (unlikely(len > m_last->data_len))
		return -1;

	m_last->data_len = (uint16_t)(m_last->data_len - len);
	m->pkt_len  = (m->pkt_len - len);
	return 0;
}
```

### rte_pktmbuf_is_contiguous

```c
/**
 * Test if mbuf data is contiguous.
 * 
 * 测试 mbuf 数据是否连续。
 *
 * @param m
 *   The packet mbuf.
 *   要测试的mbuf
 * @return
 *   - 1, if all data is contiguous (one segment).
 *   - 如果所有数据都是连续的（一个段）返回1
 *   - 0, if there is several segments.
 *   - 如果有几个段返回0。
 */
static inline int rte_pktmbuf_is_contiguous(const struct rte_mbuf *m)
{
	__rte_mbuf_sanity_check(m, 1);
	return m->nb_segs == 1;
}
```


### rte_pktmbuf_read

```c
/**
 * Read len data bytes in a mbuf at specified offset.
 * 
 * 在指定偏移量处读取 mbuf 中的 len 个数据字节。
 *
 * If the data is contiguous, return the pointer in the mbuf data, else
 * copy the data in the buffer provided by the user and return its
 * pointer.
 * 
 * 如果数据是连续的，则返回mbuf数据中的指针，否则复制用户提供的缓冲区中的数据并返回其指针。
 *
 * @param m
 *   The pointer to the mbuf.
 *   指向 mbuf 的指针。
 * @param off
 *   The offset of the data in the mbuf.
 *   mbuf 中数据的偏移量。
 * @param len
 *   The amount of bytes to read.
 *   要读取的字节数。
 * @param buf
 *   The buffer where data is copied if it is not contiguous in mbuf
 *   data. Its length should be at least equal to the len parameter.
 *   如果在 mbuf 数据中不连续，则复制数据的缓冲区。它的长度至少应该等于 len 参数。
 * @return
 *   The pointer to the data, either in the mbuf if it is contiguous,
 *   or in the user buffer. If mbuf is too small, NULL is returned.
 *   
 *   指向数据的指针，如果它是连续的，则在 mbuf 中，或者在用户缓冲区中。
 *   如果 mbuf 太小，则返回 NULL。
 */
static inline const void *rte_pktmbuf_read(const struct rte_mbuf *m,
	uint32_t off, uint32_t len, void *buf)
{
	if (likely(off + len <= rte_pktmbuf_data_len(m)))
		return rte_pktmbuf_mtod_offset(m, char *, off);
	else
		return __rte_pktmbuf_read(m, off, len, buf);
}
```


### rte_pktmbuf_chain

```c
/**
 * Chain an mbuf to another, thereby creating a segmented packet.
 * 
 * 将一个 mbuf 链接到另一个，从而创建一个分段数据包。
 *
 * Note: The implementation will do a linear walk over the segments to find
 * the tail entry. For cases when there are many segments, it's better to
 * chain the entries manually.
 * 
 * 注意：该实现将对段进行线性遍历以找到尾部条目。对于有很多段的情况，最好手动链接条目。
 *
 * @param head
 *   The head of the mbuf chain (the first packet)
 *   mbuf 链的头部（第一个数据包）
 * @param tail
 *   The mbuf to put last in the chain
 *   最后放入链中的 mbuf
 *
 * @return
 *   - 0, on success.
 *   - 0表示成功
 *   - -EOVERFLOW, if the chain segment limit exceeded
 *   - 如果超过链段限制返回-EOVERFLOW
 */
static inline int rte_pktmbuf_chain(struct rte_mbuf *head, struct rte_mbuf *tail)
{
	struct rte_mbuf *cur_tail;

	/* Check for number-of-segments-overflow */
	if (head->nb_segs + tail->nb_segs > RTE_MBUF_MAX_NB_SEGS)
		return -EOVERFLOW;

	/* Chain 'tail' onto the old tail */
	cur_tail = rte_pktmbuf_lastseg(head);
	cur_tail->next = tail;

	/* accumulate number of segments and total length.
	 * NB: elaborating the addition like this instead of using
	 *     -= allows us to ensure the result type is uint16_t
	 *     avoiding compiler warnings on gcc 8.1 at least */
	head->nb_segs = (uint16_t)(head->nb_segs + tail->nb_segs);
	head->pkt_len += tail->pkt_len;

	/* pkt_len is only set in the head */
	tail->pkt_len = tail->data_len;

	return 0;
}
```


### rte_mbuf_tx_offload

```c

/*
 * @warning
 * @b EXPERIMENTAL: This API may change without prior notice.
 *
 * For given input values generate raw tx_offload value.
 * 对于给定的输入值，生成原始 tx_offload 值。
 * Note that it is caller responsibility to make sure that input parameters
 * don't exceed maximum bit-field values.
 * 请注意，调用者有责任确保输入参数不超过最大位域值。
 * @param il2
 *   l2_len value.
 *   l2_len 值。
 * @param il3
 *   l3_len value.
 *   l3_len 值。
 * @param il4
 *   l4_len value.
 *   l4_len 值。
 * @param tso
 *   tso_segsz value.
 *   tso_segsz 值。
 * @param ol3
 *   outer_l3_len value.
 *   outer_l3_len 值。
 * @param ol2
 *   outer_l2_len value.
 *   outer_l2_len 值。
 * @param unused
 *   unused value.
 *   unused 值。
 * @return
 *   raw tx_offload value.
 *   原始 tx_offload 值。
 */
static __rte_always_inline uint64_t
rte_mbuf_tx_offload(uint64_t il2, uint64_t il3, uint64_t il4, uint64_t tso,
	uint64_t ol3, uint64_t ol2, uint64_t unused)
{
	return il2 << RTE_MBUF_L2_LEN_OFS |
		il3 << RTE_MBUF_L3_LEN_OFS |
		il4 << RTE_MBUF_L4_LEN_OFS |
		tso << RTE_MBUF_TSO_SEGSZ_OFS |
		ol3 << RTE_MBUF_OUTL3_LEN_OFS |
		ol2 << RTE_MBUF_OUTL2_LEN_OFS |
		unused << RTE_MBUF_TXOFLD_UNUSED_OFS;
}
```


### rte_validate_tx_offload

```c
/**
 * Validate general requirements for Tx offload in mbuf.
 * 
 * 在 mbuf 中验证 Tx 卸载的一般要求。
 *
 * This function checks correctness and completeness of Tx offload settings.
 * 
 * 此函数检查 Tx 卸载设置的正确性和完整性。
 *
 * @param m
 *   The packet mbuf to be validated.
 *   要验证的数据包 mbuf。
 * @return
 *   0 if packet is valid
 *   如果数据包有效返回0
 */
static inline int
rte_validate_tx_offload(const struct rte_mbuf *m)
{
	uint64_t ol_flags = m->ol_flags;

	/* Does packet set any of available offloads? */
	if (!(ol_flags & PKT_TX_OFFLOAD_MASK))
		return 0;

	/* IP checksum can be counted only for IPv4 packet */
	if ((ol_flags & PKT_TX_IP_CKSUM) && (ol_flags & PKT_TX_IPV6))
		return -EINVAL;

	/* IP type not set when required */
	if (ol_flags & (PKT_TX_L4_MASK | PKT_TX_TCP_SEG))
		if (!(ol_flags & (PKT_TX_IPV4 | PKT_TX_IPV6)))
			return -EINVAL;

	/* Check requirements for TSO packet */
	if (ol_flags & PKT_TX_TCP_SEG)
		if ((m->tso_segsz == 0) ||
				((ol_flags & PKT_TX_IPV4) &&
				!(ol_flags & PKT_TX_IP_CKSUM)))
			return -EINVAL;

	/* PKT_TX_OUTER_IP_CKSUM set for non outer IPv4 packet. */
	if ((ol_flags & PKT_TX_OUTER_IP_CKSUM) &&
			!(ol_flags & PKT_TX_OUTER_IPV4))
		return -EINVAL;

	return 0;
}
```


### rte_pktmbuf_linearize

```c
/**
 * Linearize data in mbuf.
 * 
 * 将 mbuf 中的数据线性化。
 *
 * This function moves the mbuf data in the first segment if there is enough
 * tailroom. The subsequent segments are unchained and freed.
 * 
 * 如果有足够的tailroom，此函数将数据移动到第一段中。随后的段被解开并释放。
 *
 * @param mbuf
 *   mbuf to linearize
 *   mbuf 线性化
 * @return
 *   - 0, on success
 *   - 0 成功
 *   - -1, on error
 *   - -1 失败
 */
static inline int
rte_pktmbuf_linearize(struct rte_mbuf *mbuf)
{
	if (rte_pktmbuf_is_contiguous(mbuf))
		return 0;
	return __rte_pktmbuf_linearize(mbuf);
}
```


### rte_pktmbuf_dump

```c
/**
 * Dump an mbuf structure to a file.
 * 
 * 将 mbuf 结构转储到文件中。
 *
 * Dump all fields for the given packet mbuf and all its associated
 * segments (in the case of a chained buffer).
 * 
 * 转储给定数据包 mbuf 及其所有相关段的所有字段（在链式缓冲区的情况下）。
 *
 * @param f
 *   A pointer to a file for output
 *   指向输出文件的指针
 * @param m
 *   The packet mbuf.
 *   数据包mbuf
 * @param dump_len
 *   If dump_len != 0, also dump the "dump_len" first data bytes of
 *   the packet.
 *   如果dump_len != 0，还转储数据包的“dump_len”第一个数据字节。
 */
void rte_pktmbuf_dump(FILE *f, const struct rte_mbuf *m, unsigned dump_len);
```


### rte_mbuf_sched_queue_get

```c
/**
 * Get the value of mbuf sched queue_id field.
 * 
 * 获取 mbuf sched queue_id 字段的值。
 * 
 */
static inline uint32_t
rte_mbuf_sched_queue_get(const struct rte_mbuf *m)
{
	return m->hash.sched.queue_id;
}
```


### rte_mbuf_sched_traffic_class_get

```c
/**
 * Get the value of mbuf sched traffic_class field.
 * 获取 mbuf sched traffic_class 字段的值。
 */
static inline uint8_t
rte_mbuf_sched_traffic_class_get(const struct rte_mbuf *m)
{
	return m->hash.sched.traffic_class;
}
```

### rte_mbuf_sched_color_get

```c
/**
 * Get the value of mbuf sched color field.
 * 
 * 获取 mbuf sched color字段的值。
 */
static inline uint8_t
rte_mbuf_sched_color_get(const struct rte_mbuf *m)
{
	return m->hash.sched.color;
}
```


### rte_mbuf_sched_get

```c
/**
 * Get the values of mbuf sched queue_id, traffic_class and color.
 * 
 * 获取 mbuf sched queue_id、traffic_class 和 color 的值。
 *
 * @param m
 *   Mbuf to read
 *   要读取的mbuf
 * @param queue_id
 *  Returns the queue id
 *  返回queue id
 * @param traffic_class
 *  Returns the traffic class id
 *  返回traffic class id
 * @param color
 *  Returns the colour id
 *  返回 colour id
 */
static inline void
rte_mbuf_sched_get(const struct rte_mbuf *m, uint32_t *queue_id,
			uint8_t *traffic_class,
			uint8_t *color)
{
	struct rte_mbuf_sched sched = m->hash.sched;

	*queue_id = sched.queue_id;
	*traffic_class = sched.traffic_class;
	*color = sched.color;
}
```

### rte_mbuf_sched_queue_set

```c
/**
 * Set the mbuf sched queue_id to the defined value.
 * 
 * 将 mbuf sched queue_id 设置为指定的值。
 */
static inline void
rte_mbuf_sched_queue_set(struct rte_mbuf *m, uint32_t queue_id)
{
	m->hash.sched.queue_id = queue_id;
}
```

### rte_mbuf_sched_traffic_class_set

```c
/**
 * Set the mbuf sched traffic_class id to the defined value.
 * 
 * 将 mbuf sched traffic_class id 设置为指定的值。
 */
static inline void
rte_mbuf_sched_traffic_class_set(struct rte_mbuf *m, uint8_t traffic_class)
{
	m->hash.sched.traffic_class = traffic_class;
}
```


### rte_mbuf_sched_color_set

```c
/**
 * Set the mbuf sched color id to the defined value.
 * 
 * 将 mbuf sched color id 设置为指定的值。
 */
static inline void
rte_mbuf_sched_color_set(struct rte_mbuf *m, uint8_t color)
{
	m->hash.sched.color = color;
}
```


### rte_mbuf_sched_set

```c
/**
 * Set the mbuf sched queue_id, traffic_class and color.
 * 
 * 设置 mbuf sched queue_id、traffic_class 和 color。
 *
 * @param m
 *   Mbuf to set
 *   要设置的mbuf
 * @param queue_id
 *  Queue id value to be set
 *  要设置的queue id 值
 * @param traffic_class
 *  Traffic class id value to be set
 *  要设置的traffic class id 值
 * @param color
 *  Color id to be set
 *  要设置的color id 值
 */
static inline void
rte_mbuf_sched_set(struct rte_mbuf *m, uint32_t queue_id,
			uint8_t traffic_class,
			uint8_t color)
{
	m->hash.sched = (struct rte_mbuf_sched){
				.queue_id = queue_id,
				.traffic_class = traffic_class,
				.color = color,
				.reserved = 0,
			};
}
```