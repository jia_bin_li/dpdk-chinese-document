[TOC]

# cfgfile

配置文件操作相关API

```c
#include<rte_cfgfile.h>
```

> 源码位置：src/lib/librte_cfgfile/rte_cfgfile.h

## 结构体

### rte_cfgfile_entry

```c

/** Configuration file entry */
/** 配置文件的每一项数据 */
struct rte_cfgfile_entry {
char name[CFG_NAME_LEN]; /**< Name 名称 */
char value[CFG_VALUE_LEN]; /**< Value 值 */
};

```

### rte_cfgfile_parameters

```c

/** Configuration file operation optional arguments */
/** 配置文件操作可选参数 */
struct rte_cfgfile_parameters {
/** Config file comment character; one of '!', '#', '%', ';', '@' */
/** 配置文件注释字符； '!'、'#'、'%'、';'、'@' 选择其中一个 */
char comment_character;
};

```

## 宏

### CFG_DEFAULT_COMMENT_CHARACTER

```c

/** Defines the default comment character used for parsing config files. */
/** 定义用于解析配置文件的默认注释字符。 */
#define
CFG_DEFAULT_COMMENT_CHARACTER ';'

```

## 枚举

### CFG_FLAG_GLOBAL_SECTION

### CFG_FLAG_EMPTY_VALUES

```c
/**@{ cfgfile load operation flags */
/**@{ cfgfile加载时设置的操作标志 */
enum {
/**
 * Indicates that the file supports key value entries before the first
 * defined section.  These entries can be accessed in the "GLOBAL"
 * section.
 * 
 * 表示文件支持在第一个块(section)部分之前定义键值条目。
 * 这些条目可以在“GLOBAL”块(section)中访问。
 */
CFG_FLAG_GLOBAL_SECTION = 1,

/**
 * Indicates that file supports key value entries where the value can
 * be zero length (e.g., "key=").
 * 
 * 表示文件支持键值条目值为空字符串（例如，“key=”）。
 */
CFG_FLAG_EMPTY_VALUES = 2,
};
/**@} */
```

## 函数

### rte_cfgfile_load

```c

/**
 * Open config file
 *
 * 打开配置文件
 *
 * @param filename
 *   Config file name
 * @param flags
 *   Config file flags
 *
 * @参数 filename
 *   配置文件路径
 * @参数 flags
 *   配置文件标记 CFG_FLAG_GLOBAL_SECTION CFG_FLAG_EMPTY_VALUES
 *   填写0表示不添加任何标记
 *
 * @return
 *   Handle to configuration file on success, NULL otherwise
 *
 * @返回值
 *   成功则返回配置结构体指针，否则返回NULL
 * 
*/
struct rte_cfgfile *rte_cfgfile_load(const char *filename, int flags);

```

### rte_cfgfile_load_with_params

```c

/**
 * Open config file with specified optional parameters.
 * 
 * 使用指定的可选参数打开配置文件。
 *
 * @param filename
 *   Config file name
 * @param flags
 *   Config file flags
 * @param params
 *   Additional configuration attributes.  Must be configured with desired
 *   values prior to invoking this API.
 *   
 * @参数 filename
 *   配置文件路径
 * @参数 flags
 *   配置文件标记 CFG_FLAG_GLOBAL_SECTION CFG_FLAG_EMPTY_VALUES
 *   填写0表示不添加任何标记
 * @参数 params
 *   附加配置参数,必须使用所需的值进行配置。
 *   配置文件注释字符； '!'、'#'、'%'、';'、'@' 选择其中一个
 *   
 * @return
 *   Handle to configuration file on success, NULL otherwise
 *   
 * @返回值
 *   成功则返回配置结构体指针，否则返回NULL
 *   
 */
struct rte_cfgfile *rte_cfgfile_load_with_params(const char *filename,
                                                  int flags, 
                                                  const struct rte_cfgfile_parameters *params);
```

### rte_cfgfile_create

```c

/**
 * Create new cfgfile instance with empty sections and entries
 * 
 * 创建一个包含空块和空条目的 cfgfile 实例
 *
 * @param flags
 *   - CFG_FLAG_GLOBAL_SECTION
 *     Indicates that the file supports key value entries before the first
 *     defined section.  These entries can be accessed in the "GLOBAL"
 *     section.
 *   - CFG_FLAG_EMPTY_VALUES
 *     Indicates that file supports key value entries where the value can
 *     be zero length (e.g., "key=").
 *     
 * @参数 flags
 *   - CFG_FLAG_GLOBAL_SECTION
 *     表示文件支持在第一个块(section)部分之前定义键值条目。
 *     这些条目可以在“GLOBAL”块(section)中访问。
 *   - CFG_FLAG_EMPTY_VALUES
 *     表示文件支持键值条目值为空字符串（例如，“key=”）。
 *     
 * @return
 *   Handle to cfgfile instance on success, NULL otherwise
 *   
 * @返回值
 *   成功则返回配置结构体指针，否则返回NULL
 *   
 */
struct rte_cfgfile *rte_cfgfile_create(int flags);
```


### rte_cfgfile_add_section

```c

/**
 * Add section in cfgfile instance.
 * 
 * 在 cfgfile 实例中添加块。
 *
 * @param cfg
 *   Pointer to the cfgfile structure.
 * @param sectionname
 *   Section name which will be add to cfgfile.
 *   
 * @参数 cfg
 *   cfgfile 实例指针
 * @参数 sectionname
 *   将要添加的块名称
 *   
 * @return
 *   0 on success, -ENOMEM if can't add section
 *   
 * @返回值
 *   返回0表示成功, 返回-ENOMEM表示失败
 */
int rte_cfgfile_add_section(struct rte_cfgfile *cfg, const char *sectionname);
```



