[TOC]

# random

RTE中的伪随机生成器

```c
#include<rte_random.h>
```

> 源码位置：src/lib/librte_eal/include/rte_random.h

## 函数

### rte_srand

```c
/**
 * Seed the pseudo-random generator.
 * 
 * 给伪随机生成器设定随机种子。
 *
 * The generator is automatically seeded by the EAL init with a timer
 * value. It may need to be re-seeded by the user with a real random
 * value.
 * 
 * 生成器默认由EAL初始化时候使用时间作为种子，用户可能需要使用该函数对生成器重新
 * 设定种子。
 *
 * This function is not multi-thread safe in regards to other
 * rte_srand() calls, nor is it in relation to concurrent rte_rand()
 * calls.
 * 
 * 该函数不是多线程安全的，rte_rand()也不是
 *
 * @param seedval
 *   The value of the seed.
 * 
 * @参数 seedval
 *   设置的生成器得到新种子值。
 * 
 */
void rte_srand (uint64_t seedval)
```

### rte_rand

```c
/**
 * Get a pseudo-random value.
 * 
 * 获取一个伪随机值
 *
 * The generator is not cryptographically secure.
 *
 *
 * If called from lcore threads, this function is thread-safe.
 *
 * 如果该函数是在lcore线程中调用的，则是线程安全的
 * 
 * @return
 *   A pseudo-random value between 0 and (1<<64)-1.
 * 
 * @返回值
 *   位于0到(1<<64)-1之间的伪随机值
 */
uint64_t rte_rand(void);
```

### rte_rand_max

```c
/**
 * Generates a pseudo-random number with an upper bound.
 * 
 * 生成具有上限的伪随机数。
 *
 * This function returns an uniformly distributed (unbiased) random
 * number less than a user-specified maximum value.
 * 
 * 函数返回小于用户指定最大值的均匀分布随机数。
 *
 * If called from lcore threads, this function is thread-safe.
 * 
 * 如果从 lcore 线程调用，此函数是线程安全的。
 *
 * @param upper_bound
 *   The upper bound of the generated number.
 *   
 * @参数 upper_bound
 *   生成伪随机数字的上限。
 *   
 * @return
 *   A pseudo-random value between 0 and (upper_bound-1).
 * 
 * @返回值
 *   介于 0 和 (upper_bound-1) 之间的伪随机值。
 */
__rte_experimental uint64_t rte_rand_max(uint64_t upper_bound);
```

