[TOC]

# kni

```c
#include<rte_kni.h>
```

> 源码位置：src/lib/librte_kni/rte_kni.h


## 数据结构

### struct rte_kni_ops

```c
/**
 * Structure which has the function pointers for KNI interface.
 * 
 * 具有 KNI 相关操作接口的函数指针的结构体。
 * 
 */
struct rte_kni_ops {
    //端口ID
	uint16_t port_id; /* Port ID */

	/* Pointer to function of changing MTU */
    // 更改MTU的操作函数
	int (*change_mtu)(uint16_t port_id, unsigned int new_mtu);

	/* Pointer to function of configuring network interface */
    // 配置网络接口的操作函数
	int (*config_network_if)(uint16_t port_id, uint8_t if_up);

	/* Pointer to function of configuring mac address */
    // 配置MAC地址的操作函数
	int (*config_mac_address)(uint16_t port_id, uint8_t mac_addr[]);

	/* Pointer to function of configuring promiscuous mode */
    // 配置混杂模式的操作函数
	int (*config_promiscusity)(uint16_t port_id, uint8_t to_on);

	/* Pointer to function of configuring allmulticast mode */
    // 配置组播模式的操作函数
	int (*config_allmulticast)(uint16_t port_id, uint8_t to_on);
};
```


### struct rte_kni_conf

```c
/**
 * Structure for configuring KNI device.
 * 
 * 配置KNI设备参数的结构体
 * 
 */
struct rte_kni_conf {
	/*
	 * KNI name which will be used in relevant network device.
	 * Let the name as short as possible, as it will be part of
	 * memzone name.
	 * 
	 * KNI名称将在KNI相关的网络设备中使用
	 * 让名称尽可能短，因为它将是 memzone 名称的一部分。
	 */
	char name[RTE_KNI_NAMESIZE];
    // 绑定到内核线程的核ID
	uint32_t core_id;   /* Core ID to bind kernel thread on */
    // 组ID
	uint16_t group_id;  /* Group ID */
    // mbuf的大小
	unsigned mbuf_size; /* mbuf size */
    //过时
	struct rte_pci_addr addr; /* depreciated */
    //过时
	struct rte_pci_id id; /* depreciated */

	__extension__
    //是否绑定内核线程
	uint8_t force_bind : 1; /* Flag to bind kernel thread */
    //分配给KNI的MAC地址
	uint8_t mac_addr[RTE_ETHER_ADDR_LEN]; /* MAC address assigned to KNI */
    //KNI设备的MTU值
	uint16_t mtu;
    //KNI设备的最小MTU值
	uint16_t min_mtu;
    //KNI设备的最大MTU值
	uint16_t max_mtu;
};
```


## 函数

### rte_kni_init

```c
/**
 * Initialize and preallocate KNI subsystem
 * 
 * 初始化和预分配 KNI 子系统
 *
 * This function is to be executed on the main lcore only, after EAL
 * initialization and before any KNI interface is attempted to be
 * allocated
 * 
 * 此函数仅在主lcore上，并且要在EAL初始化之后和尝试分配任何KNI接口之前执行
 *
 * @param max_kni_ifaces
 *  The maximum number of KNI interfaces that can coexist concurrently
 *  可同时共存的最大KNI接口数
 *
 * @return
 *  - 0 indicates success.
 *    0表示成功
 *  - negative value indicates failure.
 *    负值表示失败
 */
int rte_kni_init(unsigned int max_kni_ifaces);
```

### rte_kni_alloc

```c
/**
 * Allocate KNI interface according to the port id, mbuf size, mbuf pool,
 * configurations and callbacks for kernel requests.The KNI interface created
 * in the kernel space is the net interface the traditional Linux application
 * talking to.
 * 
 * 根据配置的端口id、mbuf大小、mbuf池、KNI接口的回调函数等对KNI进行分配
 * 在内核空间中创建的KNI接口是传统Linux应用程序与之通信的网络接口。
 *
 * The rte_kni_alloc shall not be called before rte_kni_init() has been
 * called. rte_kni_alloc is thread safe.
 * 
 * 在调用 rte_kni_init() 之前不应调用 rte_kni_alloc()函数。
 * rte_kni_alloc函数是线程安全的
 *
 * The mempool should have capacity of more than "2 x KNI_FIFO_COUNT_MAX"
 * elements for each KNI interface allocated.
 * 
 * 对于分配的每个 KNI 接口，内存池的容量应超过“2 x KNI_FIFO_COUNT_MAX”个元素。
 * 默认 KNI_FIFO_COUNT_MAX=1024
 *
 * @param pktmbuf_pool
 *  The mempool for allocating mbufs for packets.
 *  用于为数据包分配 mbuf 的内存池。
 * @param conf
 *  The pointer to the configurations of the KNI device.
 *  配置KNI设备的配置信息结构体指针
 * @param ops
 *  The pointer to the callbacks for the KNI kernel requests.
 *  KNI内核请求的回调函数
 *
 * @return
 *  - The pointer to the context of a KNI interface.
 *    分配成功的KNI接口上下文结构体指针
 *  - NULL indicate error.
 *    返回NULL表示错误
 */
struct rte_kni *rte_kni_alloc(struct rte_mempool *pktmbuf_pool,
		const struct rte_kni_conf *conf, struct rte_kni_ops *ops);
```


### rte_kni_release

```c
/**
 * Release KNI interface according to the context. It will also release the
 * paired KNI interface in kernel space. All processing on the specific KNI
 * context need to be stopped before calling this interface.
 * 
 * 通过KNI上下文结构体指针释放KNI接口。它还将在内核空间中释放与之配对的KNI接口。
 * 在调用此接口之前，需要停止对特定KNI上下文的所有处理。
 *
 * rte_kni_release is thread safe.
 * 
 * rte_kni_release是线程安全的
 *
 * @param kni
 *  The pointer to the context of an existent KNI interface.
 *  已经存在的KNI接口上下文指针
 *
 * @return
 *  - 0 indicates success.
 *    0 表示成功
 *  - negative value indicates failure.
 *    负数表示失败
 */
int rte_kni_release(struct rte_kni *kni);
```


### rte_kni_handle_request


```c
/**
 * It is used to handle the request mbufs sent from kernel space.
 * Then analyzes it and calls the specific actions for the specific requests.
 * Finally constructs the response mbuf and puts it back to the resp_q.
 * 
 * 它用于处理从内核空间发送的mbufs。
 * 然后对其进行分析并针对特定请求调用特定操作。
 * 最后构造响应mbuf并放回响应队列中
 *
 * @param kni
 *  The pointer to the context of an existent KNI interface.
 *  已经存在的KNI接口上下文
 *
 * @return
 *  - 0
 *    0 表示成功
 *  - negative value indicates failure.
 *    负数表示失败
 */
int rte_kni_handle_request(struct rte_kni *kni);
```

### rte_kni_rx_burst

```c
/**
 * Retrieve a burst of packets from a KNI interface. The retrieved packets are
 * stored in rte_mbuf structures whose pointers are supplied in the array of
 * mbufs, and the maximum number is indicated by num. It handles allocating
 * the mbufs for KNI interface alloc queue.
 * 
 * 从KNI接口取出突发数据包。
 * 取出的数据包存储在rte_mbuf结构中，其指针在mbufs数组中存储，最大数量由参数num指定
 * 分配mbuf是在KNI接口分配队列中
 *
 * @param kni
 *  The KNI interface context.
 *  KNI接口上下文
 * @param mbufs
 *  The array to store the pointers of mbufs.
 *  要存储的mbuf指针数组
 * @param num
 *  The maximum number per burst.
 *  每次突发取回的最大数量
 *
 * @return
 *  The actual number of packets retrieved.
 *  实际取回的包数量
 */
unsigned rte_kni_rx_burst(struct rte_kni *kni, struct rte_mbuf **mbufs,
		unsigned num);
```

### rte_kni_tx_burst

```c
/**
 * Send a burst of packets to a KNI interface. The packets to be sent out are
 * stored in rte_mbuf structures whose pointers are supplied in the array of
 * mbufs, and the maximum number is indicated by num. It handles the freeing of
 * the mbufs in the free queue of KNI interface.
 * 
 * 将数据包发送到KNI接口。
 * 要发送的数据包存储在 rte_mbuf 结构中，其指针在mbufs数组中存储，最大数量由参数num指定。
 * 释放mbuf在KNI接口的释放队列中
 *
 * @param kni
 *  The KNI interface context.
 *  KNI接口上下文
 * @param mbufs
 *  The array to store the pointers of mbufs.
 *  要存储的mbuf指针数组
 * @param num
 *  The maximum number per burst.
 *  每次突发发送的最大数量
 *
 * @return
 *  The actual number of packets sent.
 */
unsigned rte_kni_tx_burst(struct rte_kni *kni, struct rte_mbuf **mbufs,
		unsigned num);
```

### rte_kni_get

```c
/**
 * Get the KNI context of its name.
 * 根据名字获取KNI上下文指针
 *
 * @param name
 *  pointer to the KNI device name.
 *  KNI设备名称
 *
 * @return
 *  On success: Pointer to KNI interface.
 *  成功返回KNI接口指针
 *  On failure: NULL.
 *  失败返回NULL
 */
struct rte_kni *rte_kni_get(const char *name);
```

### rte_kni_get_name

```c
/**
 * Get the name given to a KNI device
 * 
 * 获取给定的KNI设备的名称
 *
 * @param kni
 *   The KNI instance to query
 *   需要查询的KNI接口
 * @return
 *   The pointer to the KNI name
 *   KNI接口的名称
 */
const char *rte_kni_get_name(const struct rte_kni *kni);
```

### rte_kni_register_handlers

```c
/**
 * Register KNI request handling for a specified port,and it can
 * be called by primary process or secondary process.
 * 
 * 注册指定端口的KNI请求处理回调函数，可以被主进程或次进程调用
 *
 * @param kni
 *  pointer to struct rte_kni.
 *  kni接口指针
 * @param ops
 *  pointer to struct rte_kni_ops.
 *  KNI操作结构体指针
 *
 * @return
 *  On success: 0
 *  成功返回0
 *  On failure: -1
 *  失败返回-1
 */
int rte_kni_register_handlers(struct rte_kni *kni, struct rte_kni_ops *ops);
```

### rte_kni_unregister_handlers

```c
/**
 *  Unregister KNI request handling for a specified port.
 *  
 *  取消注册指定端口的 KNI 请求处理。
 *
 *  @param kni
 *   pointer to struct rte_kni.
 *   KNI接口指针
 *
 *  @return
 *   On success: 0
 *   成功返回0
 *   On failure: -1
 *   失败返回-1
 */
int rte_kni_unregister_handlers(struct rte_kni *kni);
```


### rte_kni_update_link

```c
/**
 * Update link carrier state for KNI port.
 * 
 * 更新 KNI 端口的链路载波状态。
 *
 * Update the linkup/linkdown state of a KNI interface in the kernel.
 * 
 * 更新内核中KNI接口的 linkup/linkdown 状态。
 *
 * @param kni
 *  pointer to struct rte_kni.
 *  KNI接口指针
 *  
 * @param linkup
 *  New link state:
 *  新的链接状态
 *  0 for linkdown.
 *  0 表示down
 *  > 0 for linkup.
 *  > 0 表示up
 *
 * @return
 *  On failure: -1
 *  -1 表示错误
 *  Previous link state == linkdown: 0
 *  上一个状态是down则返回0
 *  Previous link state == linkup: 1
 *  上一个状态是up则返回1
 */
__rte_experimental
int
rte_kni_update_link(struct rte_kni *kni, unsigned int linkup);
```

### rte_kni_close

```c
/**
 *  Close KNI device.
 *  关闭KNI设备
 */
void rte_kni_close(void);
```