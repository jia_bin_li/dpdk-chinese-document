[TOC]

# vhost

```c
#include<rte_vhost.h>
```

> 源码位置：src/lib/librte_vhost/rte_vhost.h


## 数据结构

### rte_vhost_mem_region


```c
/**
 * Information relating to memory regions including offsets to
 * addresses in QEMUs memory file.
 * 
 * 与内存区域相关的信息，包括 QEMU 内存文件中地址的偏移量。
 */
struct rte_vhost_mem_region {
	uint64_t guest_phys_addr;
	uint64_t guest_user_addr;
	uint64_t host_user_addr;
	uint64_t size;
	void	 *mmap_addr;
	uint64_t mmap_size;
	int fd;
};
```


### rte_vhost_memory

```c
/**
 * Memory structure includes region and mapping information.
 * 
 * 内存结构包括区域和映射信息。
 */
struct rte_vhost_memory {
	uint32_t nregions;
	struct rte_vhost_mem_region regions[];
};
```


## 函数

### rte_vhost_gpa_to_vva


```c
/**
 * Convert guest physical address to host virtual address
 * 
 * 将访客物理地址转换为主机虚拟地址
 *
 * This function is deprecated because unsafe.
 * New rte_vhost_va_from_guest_pa() should be used instead to ensure
 * guest physical ranges are fully and contiguously mapped into
 * process virtual address space.
 * 
 * 此功能已弃用，因为不安全。
 * 应该使用新的 rte_vhost_va_from_guest_pa() 来确保客户物理范围完全且连续地映射到进程虚拟地址空间。
 * 
 * @param mem
 *  the guest memory regions
 * @param gpa
 *  the guest physical address for querying
 * @return
 *  the host virtual address on success, 0 on failure
 */
__rte_deprecated
static __rte_always_inline uint64_t
rte_vhost_gpa_to_vva(struct rte_vhost_memory *mem, uint64_t gpa)
{
	struct rte_vhost_mem_region *reg;
	uint32_t i;

	for (i = 0; i < mem->nregions; i++) {
		reg = &mem->regions[i];
		if (gpa >= reg->guest_phys_addr &&
		    gpa <  reg->guest_phys_addr + reg->size) {
			return gpa - reg->guest_phys_addr +
			       reg->host_user_addr;
		}
	}

	return 0;
}
```


### rte_vhost_va_from_guest_pa

```c

/**
 * Convert guest physical address to host virtual address safely
 * 
 * 安全地将访客物理地址转换为主机虚拟地址
 *
 * This variant of rte_vhost_gpa_to_vva() takes care all the
 * requested length is mapped and contiguous in process address
 * space.
 * 
 * rte_vhost_gpa_to_vva() 的这个变体负责将所有请求的长度映射并在进程地址空间中连续。
 *
 * @param mem
 *  the guest memory regions
 *  
 *  来宾内存区域
 *  
 * @param gpa
 *  the guest physical address for querying
 *  
 *  查询的来宾物理地址
 *  
 * @param len
 *  the size of the requested area to map, updated with actual size mapped
 *  
 *  要映射的请求区域的大小，更新为映射的实际大小
 *  
 * @return
 *  the host virtual address on success, 0 on failure
 *  
 *  成功时返回主机虚拟地址，失败时为 0
 */
__rte_experimental
static __rte_always_inline uint64_t
rte_vhost_va_from_guest_pa(struct rte_vhost_memory *mem,
						   uint64_t gpa, uint64_t *len)
{
	struct rte_vhost_mem_region *r;
	uint32_t i;

	for (i = 0; i < mem->nregions; i++) {
		r = &mem->regions[i];
		if (gpa >= r->guest_phys_addr &&
		    gpa <  r->guest_phys_addr + r->size) {

			if (unlikely(*len > r->guest_phys_addr + r->size - gpa))
				*len = r->guest_phys_addr + r->size - gpa;

			return gpa - r->guest_phys_addr +
			       r->host_user_addr;
		}
	}
	*len = 0;

	return 0;
}
```

### rte_vhost_log_write

```c
/**
 * Log the memory write start with given address.
 * 
 * 记录从给定地址开始的内存写入。
 *
 * This function only need be invoked when the live migration starts.
 * Therefore, we won't need call it at all in the most of time. For
 * making the performance impact be minimum, it's suggested to do a
 * check before calling it:
 * 
 * 只有在实时迁移开始时才需要调用此函数。因此，在大多数情况下，我们根本不需要调用它。
 * 为了使性能影响最小化，建议在调用之前进行检查：
 *
 *        if (unlikely(RTE_VHOST_NEED_LOG(features)))
 *                rte_vhost_log_write(vid, addr, len);
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param addr
 *  the starting address for write (in guest physical address space)
 *  
 *  写入的起始地址（在客户物理地址空间中）
 *  
 * @param len
 *  the length to write
 *  
 *  写的长度
 *  
 */
void rte_vhost_log_write(int vid, uint64_t addr, uint64_t len);
```


### rte_vhost_log_used_vring

```c
/**
 * Log the used ring update start at given offset.
 * 
 * 在给定偏移处开始记录使用的环更新。
 *
 * Same as rte_vhost_log_write, it's suggested to do a check before
 * calling it:
 * 
 * 与 rte_vhost_log_write 相同，建议在调用之前进行检查：
 *
 *        if (unlikely(RTE_VHOST_NEED_LOG(features)))
 *                rte_vhost_log_used_vring(vid, vring_idx, offset, len);
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param vring_idx
 *  the vring index
 *  
 *  vring 索引
 *  
 * @param offset
 *  the offset inside the used ring
 *  
 *  使用过的环内的偏移量
 *  
 * @param len
 *  the length to write
 *  
 *  写的长度
 *  
 */
void rte_vhost_log_used_vring(int vid, uint16_t vring_idx,
			      uint64_t offset, uint64_t len);
```



### rte_vhost_driver_register

```c
/**
 * Register vhost driver. path could be different for multiple
 * instance support.
 * 
 * 注册虚拟主机驱动程序。多实例支持的路径可能不同。
 * 
 */
int rte_vhost_driver_register(const char *path, uint64_t flags);
```

### rte_vhost_driver_unregister

```c
/* Unregister vhost driver. This is only meaningful to vhost user. */
// 注销虚拟主机驱动程序。这仅对 vhost 用户有意义
int rte_vhost_driver_unregister(const char *path);
```


### rte_vhost_driver_attach_vdpa_device

```c
/**
 * Set the vdpa device id, enforce single connection per socket
 * 
 * 设置vdpa设备ID，每个套接字强制单个连接
 *
 * @param path
 *  The vhost-user socket file path
 *  
 *  vhost-user 套接字文件路径
 *  
 * @param dev
 *  vDPA device pointer
 *  
 *  vDPA 设备指针
 *  
 * @return
 *  0 on success, -1 on failure
 *  成功返回0 失败返回-1
 */
int
rte_vhost_driver_attach_vdpa_device(const char *path,
		struct rte_vdpa_device *dev);
```


### rte_vhost_driver_detach_vdpa_device

```c
/**
 * Unset the vdpa device id
 * 
 * 取消设置 vdpa 设备 ID
 *
 * @param path
 *  The vhost-user socket file path
 *  
 *  vhost-user 套接字文件路径
 *  
 * @return
 *  0 on success, -1 on failure
 *  成功返回0，失败返回-1
 */
int
rte_vhost_driver_detach_vdpa_device(const char *path);
```


### rte_vhost_driver_get_vdpa_device

```c
/**
 * Get the device id
 * 
 * 获取vDPA设备
 *
 * @param path
 *  The vhost-user socket file path
 *  
 *  vhost-user 套接字文件路径
 *  
 * @return
 *  vDPA device pointer, NULL on failure
 *  失败返回NULL,成功返回vDPA设备指针
 */
struct rte_vdpa_device *
rte_vhost_driver_get_vdpa_device(const char *path);
```


### rte_vhost_driver_set_features

```c
/**
 * Set the feature bits the vhost-user driver supports.
 * 
 * 设置 vhost-user 驱动程序支持的功能标记。
 *
 * @param path
 *  The vhost-user socket file path
 *  
 *  vhost-user 套接字文件路径
 *  
 * @param features
 *  Supported features
 *  
 *  支持的功能
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 */
int rte_vhost_driver_set_features(const char *path, uint64_t features);
```

### rte_vhost_driver_enable_features

```c
/**
 * Enable vhost-user driver features.
 * 
 * 启用 vhost-user 驱动程序功能。
 *
 * Note that
 * - the param features should be a subset of the feature bits provided
 *   by rte_vhost_driver_set_features().
 *   
 *   参数特性应该是 rte_vhost_driver_set_features() 提供的特性位的子集。
 *   即必须先使用rte_vhost_driver_set_features进行设置，然后才能启用
 *   
 * - it must be invoked before vhost-user negotiation starts.
 * 
 *   必须在 vhost-user 协商开始之前调用它
 *
 * @param path
 *  The vhost-user socket file path
 *  
 *  vhost-user 套接字文件路径
 *  
 * @param features
 *  Features to enable
 *  
 *  要启用的功能
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
int rte_vhost_driver_enable_features(const char *path, uint64_t features);
```


### rte_vhost_driver_disable_features

```c
/**
 * Disable vhost-user driver features.
 * 
 * 禁用 vhost-user 驱动程序功能。
 *
 * The two notes at rte_vhost_driver_enable_features() also apply here.
 * 
 * rte_vhost_driver_enable_features() 中的两个注释也适用于此。
 *
 * @param path
 *  The vhost-user socket file path
 *  
 *  vhost-user 套接字文件路径
 *  
 * @param features
 *  Features to disable
 *  
 *  要禁用的功能
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *   成功返回0，失败返回-1
 *  
 */
int rte_vhost_driver_disable_features(const char *path, uint64_t features);
```

### rte_vhost_driver_get_features

```c
/**
 * Get the feature bits before feature negotiation.
 * 
 * 在功能协商之前获取功能位。
 *
 * @param path
 *  The vhost-user socket file path
 *  
 *  vhost-user 套接字文件路径
 *  
 * @param features
 *  A pointer to store the queried feature bits
 *  
 *  存储查询的特征位的指针
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
int rte_vhost_driver_get_features(const char *path, uint64_t *features);
```


### rte_vhost_driver_set_protocol_features


```c
/**
 * Set the protocol feature bits before feature negotiation.
 * 
 * 在特性协商之前设置协议特性位。
 *
 * @param path
 *  The vhost-user socket file path
 *  
 *  vhost-user 套接字文件路径
 *  
 * @param protocol_features
 *  Supported protocol features
 *  
 *  支持的协议特性
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
__rte_experimental
int
rte_vhost_driver_set_protocol_features(const char *path,
		uint64_t protocol_features);
```


### rte_vhost_driver_get_protocol_features

```c
/**
 * Get the protocol feature bits before feature negotiation.
 * 
 * 在特性协商之前获取协议特性位。
 *
 * @param path
 *  The vhost-user socket file path
 *  
 *  vhost-user 套接字文件路径
 *  
 * @param protocol_features
 *  A pointer to store the queried protocol feature bits
 *  
 *  存储查询的协议特征位的指针
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
__rte_experimental
int
rte_vhost_driver_get_protocol_features(const char *path,
		uint64_t *protocol_features);
```


### rte_vhost_driver_get_queue_num

```c
/**
 * Get the queue number bits before feature negotiation.
 * 
 * 在功能协商之前获取队列号。
 *
 * @param path
 *  The vhost-user socket file path
 *  
 *  vhost-user 套接字文件路径
 *  
 * @param queue_num
 *  A pointer to store the queried queue number bits
 *  
 *  存储查询队列号的指针
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
__rte_experimental
int
rte_vhost_driver_get_queue_num(const char *path, uint32_t *queue_num);
```

### rte_vhost_get_negotiated_features

```c
/**
 * Get the feature bits after negotiation
 * 
 * 协商后获取特征位
 *
 * @param vid
 *  Vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param features
 *  A pointer to store the queried feature bits
 *  
 *  存储查询的特征位的指针
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
int rte_vhost_get_negotiated_features(int vid, uint64_t *features);
```


### rte_vhost_driver_callback_register

```c
/* Register callbacks. */
//注册回调函数
int rte_vhost_driver_callback_register(const char *path,
	struct vhost_device_ops const * const ops);
```

### rte_vhost_driver_start

```c
/**
 *
 * Start the vhost-user driver.
 * 
 * 启动 vhost-user 驱动程序。
 *
 * This function triggers the vhost-user negotiation.
 * 
 * 此函数触发 vhost-user 协商。
 *
 * @param path
 *  The vhost-user socket file path
 *  
 *  vhost-user 套接字文件路径
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 */
int rte_vhost_driver_start(const char *path);
```

### rte_vhost_get_mtu

```c
/**
 * Get the MTU value of the device if set in QEMU.
 * 
 * 如果在 QEMU 中设置了MTU，则获取设备的 MTU 值。
 *
 * @param vid
 *  virtio-net device ID
 *  
 *  virtio-net 设备 ID
 *  
 * @param mtu
 *  The variable to store the MTU value
 *  
 *  存储 MTU 值的变量
 *
 * @return
 *  0: success
 *  -EAGAIN: device not yet started
 *  -ENOTSUP: device does not support MTU feature
 *  
 *  成功返回0
 *  
 */
int rte_vhost_get_mtu(int vid, uint16_t *mtu);
```

### rte_vhost_get_numa_node

```c
/**
 * Get the numa node from which the virtio net device's memory
 * is allocated.
 * 
 * 获取分配virtio net设备内存的numa节点
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *
 * @return
 *  The numa node, -1 on failure
 *  
 *  成功返回numa节点ID,否则返回-1
 */
int rte_vhost_get_numa_node(int vid);
```


### rte_vhost_get_vring_num

```c
/**
 * Get the number of vrings the device supports.
 * 
 * 获取设备支持的 vring 数量。
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *
 * @return
 *  The number of vrings, 0 on failure
 *  
 *  成功返回vring数量，失败返回0
 *  
 */
uint16_t rte_vhost_get_vring_num(int vid);
```

### rte_vhost_get_ifname

```c
/**
 * Get the virtio net device's ifname, which is the vhost-user socket
 * file path.
 * 
 * 获取 virtio net 设备的 ifname，即 vhost-user 套接字文件路径。
 * 
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param buf
 *  The buffer to stored the queried ifname
 *  
 *  存储查询的 ifname 的缓冲区
 *  
 * @param len
 *  The length of buf
 *  
 *  buf的长度
 *  
 *
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
int rte_vhost_get_ifname(int vid, char *buf, size_t len);
```

### rte_vhost_avail_entries

```c
/**
 * Get how many avail entries are left in the queue
 * 
 * 获取队列中剩余的可用条目数
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param queue_id
 *  virtio queue index
 *  
 *  virtio 队列索引
 *
 * @return
 *  num of avail entries left
 *  
 *  剩余的可用条目数
 *  
 */
uint16_t rte_vhost_avail_entries(int vid, uint16_t queue_id);
```

### rte_vhost_enqueue_burst


```c
/**
 * This function adds buffers to the virtio devices RX virtqueue. Buffers can
 * be received from the physical port or from another virtual device. A packet
 * count is returned to indicate the number of packets that were successfully
 * added to the RX queue.
 * 
 * 此函数将缓冲区添加到 virtio 设备 RX virtqueue。缓冲区可以从物理端口或另一个虚拟设备接收。
 * 返回数据包计数以指示成功添加到 RX 队列的数据包数量。
 * 
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param queue_id
 *  virtio queue index in mq case
 *  
 *  mq 情况下的 virtio 队列索引
 *  
 * @param pkts
 *  array to contain packets to be enqueued
 *  
 *  包含要入队的数据包的数组
 *  
 * @param count
 *  packets num to be enqueued
 *  
 *  要入队的数据包数
 *  
 * @return
 *  num of packets enqueued
 *  
 *  成功入队的数据包数
 *  
 */
uint16_t rte_vhost_enqueue_burst(int vid, uint16_t queue_id,
	struct rte_mbuf **pkts, uint16_t count);
```

### rte_vhost_dequeue_burst

```c
/**
 * This function gets guest buffers from the virtio device TX virtqueue,
 * construct host mbufs, copies guest buffer content to host mbufs and
 * store them in pkts to be processed.
 * 
 * 该函数从 virtio 设备 TX virtqueue 获取访客缓冲区，
 * 构造主机 mbuf，将访客缓冲区内容复制到主机 mbuf 并将它们
 * 存储在 pkts 中以进行处理。
 * 
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param queue_id
 *  virtio queue index in mq case
 *  
 *  mq 情况下的 virtio 队列索引
 *  
 * @param mbuf_pool
 *  mbuf_pool where host mbuf is allocated.
 *  
 *  分配主机 mbuf 的 mbuf_pool。
 *  
 * @param pkts
 *  array to contain packets to be dequeued
 *  
 *  包含要出队的数据包的数组
 *  
 * @param count
 *  packets num to be dequeued
 *  
 *  要出队的数据包数量
 *  
 * @return
 *  num of packets dequeued
 *  
 *  出队的数据包数
 *  
 */
uint16_t rte_vhost_dequeue_burst(int vid, uint16_t queue_id,
	struct rte_mempool *mbuf_pool, struct rte_mbuf **pkts, uint16_t count);
```

### rte_vhost_get_mem_table

```c
/**
 * Get guest mem table: a list of memory regions.
 * 
 * 获取访客内存表：内存区域列表。
 *
 * An rte_vhost_vhost_memory object will be allocated internally, to hold the
 * guest memory regions. Application should free it at destroy_device()
 * callback.
 * 
 * 将在内部分配一个 rte_vhost_vhost_memory 对象，以保存访客内存区域。
 * 应用程序应在 destroy_device() 回调中释放它。
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param mem
 *  To store the returned mem regions
 *  
 *  存储返回的内存区域
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
int rte_vhost_get_mem_table(int vid, struct rte_vhost_memory **mem);
```

### rte_vhost_get_vhost_vring

```c
/**
 * Get guest vring info, including the vring address, vring size, etc.
 * 
 * 获取访客 vring信息，包括vring地址、vring大小等
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param vring_idx
 *  vring index
 *  
 *  vring 索引
 *  
 * @param vring
 *  the structure to hold the requested vring info
 *  
 *  保存返回的数据的 vring 信息结构体
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
int rte_vhost_get_vhost_vring(int vid, uint16_t vring_idx,
			      struct rte_vhost_vring *vring);
```

### rte_vhost_get_vhost_ring_inflight

```c
/**
 * Get guest inflight vring info, including inflight ring and resubmit list.
 * 
 * 获取访客机上 vring 信息，包括机上ring队列和重新提交列表
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param vring_idx
 *  vring index
 *  
 *  vring索引
 *  
 * @param vring
 *  the structure to hold the requested inflight vring info
 *  
 *  保存vring数据的结构体
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
__rte_experimental
int
rte_vhost_get_vhost_ring_inflight(int vid, uint16_t vring_idx,
	struct rte_vhost_ring_inflight *vring);
```

### rte_vhost_set_inflight_desc_split

```c
/**
 * Set split inflight descriptor.
 *
 * This function save descriptors that has been comsumed in available
 * ring
 * 
 * 该函数设置可用ring中已消耗的描述符
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param vring_idx
 *  vring index
 *  
 *  vring索引
 *  
 * @param idx
 *  inflight entry index
 *  
 *  实例索引
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
__rte_experimental
int
rte_vhost_set_inflight_desc_split(int vid, uint16_t vring_idx,
	uint16_t idx);
```

### rte_vhost_set_inflight_desc_packed

```c
/**
 * Set packed inflight descriptor and get corresponding inflight entry
 *
 * This function save descriptors that has been comsumed
 * 
 * 该函数设置已被消费的描述符
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param vring_idx
 *  vring index
 *  
 *  vring 索引
 *  
 * @param head
 *  head of descriptors
 *  
 *  开始的描述符
 *  
 * @param last
 *  last of descriptors
 *  
 *  结束的描述符
 *  
 * @param inflight_entry
 *  corresponding inflight entry
 *  
 *  相应的实例
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 */
__rte_experimental
int
rte_vhost_set_inflight_desc_packed(int vid, uint16_t vring_idx,
	uint16_t head, uint16_t last, uint16_t *inflight_entry);
```

### rte_vhost_set_last_inflight_io_split

```c
/**
 * Save the head of list that the last batch of used descriptors.
 * 
 * 保存最后一批使用的描述符的列表头。
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param vring_idx
 *  vring index
 *  
 *  vring索引
 *  
 * @param idx
 *  descriptor entry index
 *  
 *  描述符实例索引
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
__rte_experimental
int
rte_vhost_set_last_inflight_io_split(int vid,
	uint16_t vring_idx, uint16_t idx);
```

### rte_vhost_set_last_inflight_io_packed

```c
/**
 * Update the inflight free_head, used_idx and used_wrap_counter.
 *
 * This function will update status first before updating descriptors
 * to used
 * 
 * 此函数将在更新描述符被使用之前先更新状态
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param vring_idx
 *  vring index
 *  
 *  vring 索引
 *  
 * @param head
 *  head of descriptors
 *  
 *  描述符头
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
__rte_experimental
int
rte_vhost_set_last_inflight_io_packed(int vid,
	uint16_t vring_idx, uint16_t head);
```


### rte_vhost_clr_inflight_desc_split

```c
/**
 * Clear the split inflight status.
 * 
 * 清除拆分飞行状态。
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param vring_idx
 *  vring index
 *  
 *  vring 索引
 *  
 * @param last_used_idx
 *  last used idx of used ring
 *  
 *  最后使用的ring的最后使用的索引
 *  
 * @param idx
 *  inflight entry index
 *  
 *  飞行模式实例索引
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
__rte_experimental
int
rte_vhost_clr_inflight_desc_split(int vid, uint16_t vring_idx,
	uint16_t last_used_idx, uint16_t idx);
```

### rte_vhost_clr_inflight_desc_packed

```c
/**
 * Clear the packed inflight status.
 * 
 * 清除已打包的飞行模式状态。
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param vring_idx
 *  vring index
 *  
 *  vring 索引
 *  
 * @param head
 *  inflight entry index
 *  
 *  飞行模式实例索引
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
__rte_experimental
int
rte_vhost_clr_inflight_desc_packed(int vid, uint16_t vring_idx,
	uint16_t head);
```


### rte_vhost_vring_call

```c
/**
 * Notify the guest that used descriptors have been added to the vring.  This
 * function acts as a memory barrier.
 * 
 * 通知访客使用的描述符已添加到 vring。此函数充当内存屏障。
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param vring_idx
 *  vring index
 *  
 *  vring 索引
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
int rte_vhost_vring_call(int vid, uint16_t vring_idx);
```

### rte_vhost_rx_queue_count

```c
/**
 * Get vhost RX queue avail count.
 * 
 * 获取 vhost RX 队列可用个数。
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param qid
 *  virtio queue index in mq case
 *  
 *  mq 情况下的 virtio 队列索引
 *  
 * @return
 *  num of desc available
 *  
 *  可用数量
 *  
 */
uint32_t rte_vhost_rx_queue_count(int vid, uint16_t qid);
```

### rte_vhost_get_log_base

```c
/**
 * Get log base and log size of the vhost device
 * 
 * 获取 vhost 设备的日志基数和日志大小
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param log_base
 *  vhost log base
 *  
 *  日志基数
 *  
 * @param log_size
 *  vhost log size
 *  
 *  日志大小
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 */
int
rte_vhost_get_log_base(int vid, uint64_t *log_base, uint64_t *log_size);
```


### rte_vhost_get_vring_base

```c
/**
 * Get last_avail/used_idx of the vhost virtqueue
 * 
 * 获取 vhost virtqueue 的 last_avail/used_idx
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param queue_id
 *  vhost queue index
 *  
 *  虚拟主机队列索引
 *  
 * @param last_avail_idx
 *  vhost last_avail_idx to get
 *  
 *  last_avail_idx
 *  
 * @param last_used_idx
 *  vhost last_used_idx to get
 *  
 *  last_used_idx
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
int
rte_vhost_get_vring_base(int vid, uint16_t queue_id,
		uint16_t *last_avail_idx, uint16_t *last_used_idx);
```


### rte_vhost_get_vring_base_from_inflight

```c
/**
 * Get last_avail/last_used of the vhost virtqueue
 * 
 * 获取 vhost virtqueue 的 last_avail/last_used
 *
 * This function is designed for the reconnection and it's specific for
 * the packed ring as we can get the two parameters from the inflight
 * queueregion
 * 
 * 这个函数是为重新连接而设计的，它特定于打包的ring，因为我们可以从飞行模式队列区域中获取两个参数
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param queue_id
 *  vhost queue index
 *  
 *  虚拟主机队列索引
 *  
 * @param last_avail_idx
 *  vhost last_avail_idx to get
 *  
 *  last_avail_idx
 *  
 * @param last_used_idx
 *  vhost last_used_idx to get
 *  
 *  last_used_idx
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
__rte_experimental
int
rte_vhost_get_vring_base_from_inflight(int vid,
	uint16_t queue_id, uint16_t *last_avail_idx, uint16_t *last_used_idx);
```


### rte_vhost_set_vring_base

```c
/**
 * Set last_avail/used_idx of the vhost virtqueue
 * 
 * 设置 vhost virtqueue 的 last_avail/used_idx
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param queue_id
 *  vhost queue index
 *  
 *  虚拟主机队列索引
 *  
 * @param last_avail_idx
 *  last_avail_idx to set
 *  
 *  last_avail_idx
 *  
 * @param last_used_idx
 *  last_used_idx to set
 *  
 *  last_used_idx
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
int
rte_vhost_set_vring_base(int vid, uint16_t queue_id,
		uint16_t last_avail_idx, uint16_t last_used_idx);
```

### rte_vhost_extern_callback_register

```c
/**
 * Register external message handling callbacks
 * 
 * 注册外部消息处理回调
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param ops
 *  virtio external callbacks to register
 *  
 *  virtio 外部回调注册
 *  
 * @param ctx
 *  additional context passed to the callbacks
 *  
 *  传递给回调的附加上下文
 *  
 * @return
 *  0 on success, -1 on failure
 *  
 *  成功返回0，失败返回-1
 *  
 */
__rte_experimental
int
rte_vhost_extern_callback_register(int vid,
		struct rte_vhost_user_extern_ops const * const ops, void *ctx);
```

### rte_vhost_get_vdpa_device

```c
/**
 * Get vdpa device id for vhost device.
 * 
 * 获取 vhost 设备的 vdpa 设备 ID。
 *
 * @param vid
 *  vhost device id
 *  
 *  虚拟主机设备 ID
 *  
 * @return
 *  vDPA device pointer on success, NULL on failure
 *  
 *  成功返回vDPA 设备指针，失败返回NULL 
 */
struct rte_vdpa_device *
rte_vhost_get_vdpa_device(int vid);
```


### rte_vhost_slave_config_change

```c
/**
 * Notify the guest that should get virtio configuration space from backend.
 * 
 * 通知应该从后端获取 virtio 配置空间的来宾。
 *
 * @param vid
 *  vhost device ID
 *  
 *  虚拟主机设备 ID
 *  
 * @param need_reply
 *  wait for the master response the status of this operation.
 *  
 *  等待master响应本次操作的状态
 *  
 * @return
 *  0 on success, < 0 on failure
 *  
 *  成功返回0，失败返回<0的数
 */
__rte_experimental
int
rte_vhost_slave_config_change(int vid, bool need_reply);
```