[TOC]

# kni

```c
#include<rte_ipsec_sad.h>
```

> 源码位置：src/lib/librte_ipsec/rte_ipsec_sad.h


## 数据结构


### rte_ipsec_sad_conf

```c
/** IPsec SAD configuration structure */
//IPsec SAD 配置结构体
struct rte_ipsec_sad_conf {
	/** CPU socket ID where rte_ipsec_sad should be allocated */
    //应分配 rte_ipsec_sad 的 CPU 套接字 ID
	int		socket_id;
	/** maximum number of SA for each type of key */
    // 每种密钥的最大 SA 数
	uint32_t	max_sa[RTE_IPSEC_SAD_KEY_TYPE_MASK];
	/** RTE_IPSEC_SAD_FLAG_* flags */
    // RTE_IPSEC_SAD_FLAG_* 标记
	uint32_t	flags;
};
```


### rte_ipsec_sad_add

```c
/**
 * Add a rule into the SAD. Could be safely called with concurrent lookups
 *  if RTE_IPSEC_SAD_FLAG_RW_CONCURRENCY flag was configured on creation time.
 *  While with this flag multi-reader - one-writer model Is MT safe,
 *  multi-writer model is not and required extra synchronisation.
 *  
 *  将规则添加到 SAD。如果在创建时配置了 RTE_IPSEC_SAD_FLAG_RW_CONCURRENCY 标志，则可以通过并发查找安全地调用。
 *  使用此标志时，多读 - 单写模型是多线程安全的，多写入器模型不是线程安全的并且需要额外的同步。
 *
 * @param sad
 *   SAD object handle
 *   
 *   SAD 对象指针
 *   
 * @param key
 *   pointer to the key
 *   
 *   要添加的key
 *   
 * @param key_type
 *   key type (spi only/spi+dip/spi+dip+sip)
 *   
 *   要添加的key类型
 *   spi only
 *   spi+dip
 *   spi+dip+sip
 *   
 * @param sa
 *   Pointer associated with the key to save in a SAD
 *   Must be 4 bytes aligned.
 *   
 *   与要保存在 SAD 中的键关联的指针 必须对齐 4 个字节。
 *   
 * @return
 *   0 on success, negative value otherwise
 *   
 *   0表示成功，失败返回负值
 *   
 */
int
rte_ipsec_sad_add(struct rte_ipsec_sad *sad,
	const union rte_ipsec_sad_key *key,
	int key_type, void *sa);
```


### rte_ipsec_sad_del

```c
/**
 * Delete a rule from the SAD. Could be safely called with concurrent lookups
 *  if RTE_IPSEC_SAD_FLAG_RW_CONCURRENCY flag was configured on creation time.
 *  While with this flag multi-reader - one-writer model Is MT safe,
 *  multi-writer model is not and required extra synchronisation.
 *  
 *  将规则从SAD删除。如果在创建时配置了 RTE_IPSEC_SAD_FLAG_RW_CONCURRENCY 标志，则可以通过并发查找安全地调用。
 *  使用此标志时，多读 - 单写模型是多线程安全的，多写入器模型不是线程安全的并且需要额外的同步。
 *
 * @param sad
 *   SAD object handle
 * @param key
 *   pointer to the key
 * @param key_type
 *   key type (spi only/spi+dip/spi+dip+sip)
 * @return
 *   0 on success, negative value otherwise
 */
int
rte_ipsec_sad_del(struct rte_ipsec_sad *sad,
	const union rte_ipsec_sad_key *key,
	int key_type);
```


### rte_ipsec_sad_create

```c
/*
 * Create SAD
 * 
 * 创建SAD
 *
 * @param name
 *  SAD name
 *  名称
 * @param conf
 *  Structure containing the configuration
 *  
 *  创建的参数
 *  
 * @return
 *  Handle to SAD object on success
 *  NULL otherwise with rte_errno set to an appropriate values.
 *  
 *  成功则返回SAD对象指针，否则返回NULL
 *  
 */
struct rte_ipsec_sad *
rte_ipsec_sad_create(const char *name, const struct rte_ipsec_sad_conf *conf);
```


### rte_ipsec_sad_find_existing


```c
/**
 * Find an existing SAD object and return a pointer to it.
 * 
 * 找到一个现有的 SAD 对象并返回一个指向它的指针。
 *
 * @param name
 *  Name of the SAD object as passed to rte_ipsec_sad_create()
 *  
 *  要查找的SAD名称
 *  
 * @return
 *  Pointer to sad object or NULL if object not found with rte_errno
 *  set appropriately. Possible rte_errno values include:
 *   - ENOENT - required entry not available to return.
 *   
 *   成功返回SAD指针，否则返回NULL
 */
struct rte_ipsec_sad *
rte_ipsec_sad_find_existing(const char *name);
```


### rte_ipsec_sad_destroy

```c
/**
 * Destroy SAD object.
 * 
 * 销毁SAD对象
 *
 * @param sad
 *   pointer to the SAD object
 *   
 *   SAD对象指针
 *   
 * @return
 *   None
 */
void
rte_ipsec_sad_destroy(struct rte_ipsec_sad *sad);
```


### rte_ipsec_sad_lookup


```c
/**
 * Lookup multiple keys in the SAD.
 * 
 * 在 SAD 中查找多个键。
 *
 * @param sad
 *   SAD object handle
 *   
 *   SAD对象指针
 *   
 * @param keys
 *   Array of keys to be looked up in the SAD
 *   
 *   要查找的key数组
 *   
 * @param sa
 *   Pointer assocoated with the keys.
 *   If the lookup for the given key failed, then corresponding sa
 *   will be NULL
 *   
 *   与键关联的指针。如果给定键的查找失败，则对应的 sa 将为 NULL
 *   
 * @param n
 *   Number of elements in keys array to lookup.
 *   
 *   要查找的key的个数
 *   
 *  @return
 *   -EINVAL for incorrect arguments, otherwise number of successful lookups.
 *   
 *   成功则返回查找到的key的个数，否则返回-EINVAL
 *   
 */
int
rte_ipsec_sad_lookup(const struct rte_ipsec_sad *sad,
	const union rte_ipsec_sad_key *keys[],
	void *sa[], uint32_t n);
```