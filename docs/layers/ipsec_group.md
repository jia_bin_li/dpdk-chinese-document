[TOC]

# kni

```c
#include<rte_ipsec.h>
```

> 源码位置：src/lib/librte_ipsec/rte_ipsec_group.h


## 数据结构


### rte_ipsec_group

```c
/**
 * Used to group mbufs by some id.
 * See below for particular usage.
 */
struct rte_ipsec_group {
    //组ID
	union {
		uint64_t val;
		void *ptr;
	} id; /**< grouped by value */
    //组开始指针
	struct rte_mbuf **m;  /**< start of the group */
    //组元素个数
	uint32_t cnt;         /**< number of entries in the group */
    //与组关联的状态码
	int32_t rc;           /**< status code associated with the group */
};
```

## 函数

### rte_ipsec_ses_from_crypto

```c
/**
 * Take crypto-op as an input and extract pointer to related ipsec session.
 * 
 * 通过加密操作对象指针获得该操作对象指向的ipsec会话指针。
 * 
 * @param cop
 *   The address of an input *rte_crypto_op* structure.
 *   
 *   rte_crypto_op 加密操作对象指针
 *   
 * @return
 *   The pointer to the related *rte_ipsec_session* structure.
 *   
 *   该操作对象指向的ipsec会话指针。
 *   
 */
static inline struct rte_ipsec_session *
rte_ipsec_ses_from_crypto(const struct rte_crypto_op *cop)
{
	const struct rte_security_session *ss;
	const struct rte_cryptodev_sym_session *cs;

	if (cop->sess_type == RTE_CRYPTO_OP_SECURITY_SESSION) {
		ss = cop->sym[0].sec_session;
		return (void *)(uintptr_t)ss->opaque_data;
	} else if (cop->sess_type == RTE_CRYPTO_OP_WITH_SESSION) {
		cs = cop->sym[0].session;
		return (void *)(uintptr_t)cs->opaque_data;
	}
	return NULL;
}
```


### rte_ipsec_pkt_crypto_group

```c

/**
 * Take as input completed crypto ops, extract related mbufs
 * and group them by rte_ipsec_session they belong to.
 * For mbuf which crypto-op wasn't completed successfully
 * PKT_RX_SEC_OFFLOAD_FAILED will be raised in ol_flags.
 * Note that mbufs with undetermined SA (session-less) are not freed
 * by the function, but are placed beyond mbufs for the last valid group.
 * It is a user responsibility to handle them further.
 * 
 * 将已完成的加密操作作为输入，提取相关的 mbuf 并按它们所属的 rte_ipsec_session 对它们进行分组。
 * 对于未成功完成加密操作的 mbuf，PKT_RX_SEC_OFFLOAD_FAILED 将在 ol_flags 中体现。
 * 
 * 请注意，具有未确定 SA（无会话）的 mbuf 不会被函数释放，而是放置在最后一个有效组的 mbuf 之外。
 * 进一步处理它们是用户的责任。
 * 
 * 
 * @param cop
 *   The address of an array of *num* pointers to the input *rte_crypto_op*
 *   structures.
 *   
 *   已经完成的加密操作指针数组，长度为num
 *   
 * @param mb
 *   The address of an array of *num* pointers to output *rte_mbuf* structures.
 *   
 *   输出的mbuf指针数组
 *   
 * @param grp
 *   The address of an array of *num* to output *rte_ipsec_group* structures.
 *   
 *   输出的的ipsec组数组
 *   
 * @param num
 *   The maximum number of crypto-ops to process.
 *   
 *   要处理的最大的加密操作对象个数
 *   
 * @return
 *   Number of filled elements in *grp* array.
 *   
 *   填充到组对象数组的元素个数
 *   
 */
static inline uint16_t
rte_ipsec_pkt_crypto_group(const struct rte_crypto_op *cop[],
	struct rte_mbuf *mb[], struct rte_ipsec_group grp[], uint16_t num)
{
	uint32_t i, j, k, n;
	void *ns, *ps;
	struct rte_mbuf *m, *dr[num];

	j = 0;
	k = 0;
	n = 0;
	ps = NULL;

	for (i = 0; i != num; i++) {

		m = cop[i]->sym[0].m_src;
		ns = cop[i]->sym[0].session;

		m->ol_flags |= PKT_RX_SEC_OFFLOAD;
		if (cop[i]->status != RTE_CRYPTO_OP_STATUS_SUCCESS)
			m->ol_flags |= PKT_RX_SEC_OFFLOAD_FAILED;

		/* no valid session found */
		if (ns == NULL) {
			dr[k++] = m;
			continue;
		}

		/* different SA */
		if (ps != ns) {

			/*
			 * we already have an open group - finalize it,
			 * then open a new one.
			 */
			if (ps != NULL) {
				grp[n].id.ptr =
					rte_ipsec_ses_from_crypto(cop[i - 1]);
				grp[n].cnt = mb + j - grp[n].m;
				n++;
			}

			/* start new group */
			grp[n].m = mb + j;
			ps = ns;
		}

		mb[j++] = m;
	}

	/* finalise last group */
	if (ps != NULL) {
		grp[n].id.ptr = rte_ipsec_ses_from_crypto(cop[i - 1]);
		grp[n].cnt = mb + j - grp[n].m;
		n++;
	}

	/* copy mbufs with unknown session beyond recognised ones */
	if (k != 0 && k != num) {
		for (i = 0; i != k; i++)
			mb[j + i] = dr[i];
	}

	return n;
}

```

