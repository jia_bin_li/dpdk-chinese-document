[TOC]

# kni

```c
#include<rte_ipsec.h>
```

> 源码位置：src/lib/librte_ipsec/rte_ipsec.h


## 数据结构


### rte_ipsec_sa_pkt_func

```c

/**
 * IPsec session specific functions that will be used to:
 * - prepare - for input mbufs and given IPsec session prepare crypto ops
 *   that can be enqueued into the cryptodev associated with given session
 *   (see *rte_ipsec_pkt_crypto_prepare* below for more details).
 * - process - finalize processing of packets after crypto-dev finished
 *   with them or process packets that are subjects to inline IPsec offload
 *   (see rte_ipsec_pkt_process for more details).
 *   
 *   IPsec 会话特定函数将用于：
 *   
 *   - 准备 - 为输入的mbuf和给定的IPsec会话准备加密操作对象，使之加入给定会话关联的
 *   加密设备队列中进行加密（有关更多详细信息，请参见下面的 rte_ipsec_pkt_crypto_prepare）
 *   
 *   - 处理 - 在加密设备中完成数据包处理后，完成数据包处理或处理内联IPsec卸载的数据包
 *   （有关更多详细信息，请参阅 rte_ipsec_pkt_process）。
 *   
 *   
 */
struct rte_ipsec_sa_pkt_func {
	union {
		uint16_t (*async)(const struct rte_ipsec_session *ss,
				struct rte_mbuf *mb[],
				struct rte_crypto_op *cop[],
				uint16_t num);
		uint16_t (*sync)(const struct rte_ipsec_session *ss,
				struct rte_mbuf *mb[],
				uint16_t num);
	} prepare;
	uint16_t (*process)(const struct rte_ipsec_session *ss,
				struct rte_mbuf *mb[],
				uint16_t num);
};
```

### rte_ipsec_session

```c
/**
 * rte_ipsec_session is an aggregate structure that defines particular
 * IPsec Security Association IPsec (SA) on given security/crypto device:
 * - pointer to the SA object
 * - security session action type
 * - pointer to security/crypto session, plus other related data
 * - session/device specific functions to prepare/process IPsec packets.
 * 
 * 
 * rte_ipsec_session 是一个聚合结构，它在给定的安全加密设备上定义特定的 IPsec 安全关联 IPsec (SA)：
 * 
 * - 指向 SA（安全关联） 对象的指针  struct rte_ipsec_sa *sa;
 * - 安全会话操作类型 enum rte_security_session_action_type type;
 * - 指向安全/加密会话的指针，以及其他相关数据 
 * - 用于准备处理 IPsec 数据包的会话设备特定功能。
 * 
 */
struct rte_ipsec_session {
	/**
	 * SA that session belongs to.
	 * Note that multiple sessions can belong to the same SA.
	 * 
	 * 会话所属的 SA。
	 * 注意，多个会话可以属于同一个 SA
	 * 
	 */
	struct rte_ipsec_sa *sa;
	/** session action type */
    // 会话动作类型
	enum rte_security_session_action_type type;
	/** session and related data */
    // 加密会话和相关数据
	union {
		struct {
			struct rte_cryptodev_sym_session *ses;
			uint8_t dev_id;
		} crypto;
		struct {
			struct rte_security_session *ses;
			struct rte_security_ctx *ctx;
			uint32_t ol_flags;
		} security;
	};
	/** functions to prepare/process IPsec packets */
    // 准备处理 IPsec 数据包的函数
	struct rte_ipsec_sa_pkt_func pkt_func;
} __rte_cache_aligned;
```



## 函数

### rte_ipsec_session_prepare

```c
/**
 * Checks that inside given rte_ipsec_session crypto/security fields
 * are filled correctly and setups function pointers based on these values.
 * Expects that all fields except IPsec processing function pointers
 * (*pkt_func*) will be filled correctly by caller.
 * 
 * 检查给定的 rte_ipsec_session 加密安全字段是否正确填写，并根据这些值设置函数指针。
 * 期望调用者正确填写除IPsec处理函数指针 (pkt_func) 之外的所有字段。
 * 
 * @param ss
 *   Pointer to the *rte_ipsec_session* object
 *   
 *   rte_ipsec_session对象指针
 *   
 * @return
 *   - Zero if operation completed successfully.
 *   - -EINVAL if the parameters are invalid.
 *   
 *   - 没有错误返回0
 *   - 参数错误返回-EINVAL
 */
int
rte_ipsec_session_prepare(struct rte_ipsec_session *ss);
```


### rte_ipsec_pkt_crypto_prepare

```c
/**
 * For input mbufs and given IPsec session prepare crypto ops that can be
 * enqueued into the cryptodev associated with given session.
 * expects that for each input packet:
 *      - l2_len, l3_len are setup correctly
 * Note that erroneous mbufs are not freed by the function,
 * but are placed beyond last valid mbuf in the *mb* array.
 * It is a user responsibility to handle them further.
 * 
 * 为输入的mbufs和给定的IPsec会话准备加密操作对象，这些操作对象可以排队
 * 到与给定会话关联的加密设备中。
 * 期望对于每个输入数据包： - l2_len、l3_len 设置正确 
 * 
 * 注意，错误的 mbuf 不会被函数释放，而是放置在 mb 数组中最后一个有效的 mbuf 之外。
 * 进一步处理它们是用户的责任。
 * 
 * @param ss
 *   Pointer to the *rte_ipsec_session* object the packets belong to.
 *   
 *   rte_ipsec_session对象指针
 *   
 * @param mb
 *   The address of an array of *num* pointers to *rte_mbuf* structures
 *   which contain the input packets.
 *   
 *   指向包含输入数据包的 num个rte_mbuf结构的指针数组的地址
 *   
 * @param cop
 *   The address of an array of *num* pointers to the output *rte_crypto_op*
 *   structures.
 *   
 *   指向输出 rte_crypto_op 结构的 num 指针数组的地址。
 *   
 * @param num
 *   The maximum number of packets to process.
 *   
 *   要处理的最大数据包数。
 *   
 * @return
 *   Number of successfully processed packets, with error code set in rte_errno.
 *   
 *   成功处理的数据包数，错误代码设置在 rte_errno 中。
 *   
 */
static inline uint16_t
rte_ipsec_pkt_crypto_prepare(const struct rte_ipsec_session *ss,
	struct rte_mbuf *mb[], struct rte_crypto_op *cop[], uint16_t num)
{
	return ss->pkt_func.prepare.async(ss, mb, cop, num);
}
```


### rte_ipsec_pkt_process

```c
/**
 * Finalise processing of packets after crypto-dev finished with them or
 * process packets that are subjects to inline IPsec offload.
 * Expects that for each input packet:
 *      - l2_len, l3_len are setup correctly
 * Output mbufs will be:
 * inbound - decrypted & authenticated, ESP(AH) related headers removed,
 * *l2_len* and *l3_len* fields are updated.
 * outbound - appropriate mbuf fields (ol_flags, tx_offloads, etc.)
 * properly setup, if necessary - IP headers updated, ESP(AH) fields added,
 * Note that erroneous mbufs are not freed by the function,
 * but are placed beyond last valid mbuf in the *mb* array.
 * It is a user responsibility to handle them further.
 * 
 * 在加密设备处理完数据包后完成数据包的处理，或处理内联IPsec卸载的数据包。
 * 期望对于每个输入数据包： - l2_len、l3_len 设置正确
 * 输出的mbuf将是：
 * 入站 - 解密和验证，ESP(AH) 相关标头被删除，l2_len 和 l3_len 字段被更新。
 * 出站 - 一些mbuf 字段（ol_flags、tx_offloads 等）正确设置，如有必要将
 * 更新IP数据头，添加 ESP(AH) 字段。
 * 
 * 注意错误的 mbuf 不会被函数释放，而是放置在最后一个有效 mbuf 之外mb 数组。
 * 进一步处理它们是用户的责任。
 * 
 * 
 * @param ss
 *   Pointer to the *rte_ipsec_session* object the packets belong to.
 *   
 *   rte_ipsec_session对象指针
 *   
 * @param mb
 *   The address of an array of *num* pointers to *rte_mbuf* structures
 *   which contain the input packets.
 *   
 *   指向包含输入数据包的 rte_mbuf 结构的 num 指针数组的地址。
 *   
 * @param num
 *   The maximum number of packets to process.
 *   
 *   要处理的最大数据包数
 *   
 * @return
 *   Number of successfully processed packets, with error code set in rte_errno.
 *   
 *   成功处理的数据包数，错误代码设置在 rte_errno 中。
 *   
 */
static inline uint16_t
rte_ipsec_pkt_process(const struct rte_ipsec_session *ss, struct rte_mbuf *mb[],
	uint16_t num)
{
	return ss->pkt_func.process(ss, mb, num);
}
```