[TOC]

# kni

```c
#include<rte_ipsec_sa.h>
```

> 源码位置：src/lib/librte_ipsec/rte_ipsec_sa.h


## 数据结构


### rte_ipsec_sa_prm

```c

/**
 * SA initialization parameters.
 * 
 * SA 初始化参数。
 * 
 */
struct rte_ipsec_sa_prm {
    //该字段由用户提供和解释
	uint64_t userdata; /**< provided and interpreted by user */
    //标志
	uint64_t flags;  /**< see RTE_IPSEC_SAFLAG_* below */
	/** ipsec configuration */
    // ipsec 配置
	struct rte_security_ipsec_xform ipsec_xform;
	/** crypto session configuration */
    // 加密会话配置
	struct rte_crypto_sym_xform *crypto_xform;
	union {
        //隧道模式相关参数
		struct {
            //隧道头长度
			uint8_t hdr_len;     /**< tunnel header len */
            //IPv4/IPv6头的偏移量
			uint8_t hdr_l3_off;  /**< offset for IPv4/IPv6 header */
            //载荷数据头的协议类型
			uint8_t next_proto;  /**< next header protocol */
            // 隧道头指针
			const void *hdr;     /**< tunnel header template */
		} tun; /**< tunnel mode related parameters */

        //传输模式相关参数
		struct {
            // 载荷数据头的协议类型
			uint8_t proto;  /**< next header protocol */
		} trs; /**< transport mode related parameters */
	};
};
```


### rte_ipsec_sa_type

```c
/**
 * get type of given SA
 * 
 * 获取给定 SA 的类型
 * 
 * @return
 *   SA type value.
 *   SA类型值
 */
uint64_t
rte_ipsec_sa_type(const struct rte_ipsec_sa *sa);
```


### rte_ipsec_sa_size


```c
/**
 * Calculate required SA size based on provided input parameters.
 * 
 * 根据提供的输入参数计算所需的 SA 大小。
 * 
 * @param prm
 *   Parameters that will be used to initialise SA object.
 *   
 *   将用于初始化 SA 对象的参数。
 *   
 * @return
 *   - Actual size required for SA with given parameters.
 *   具有给定参数的 SA 所需的实际大小。
 *   - -EINVAL if the parameters are invalid.
 *   参数有误返回-EINVAL
 */
int
rte_ipsec_sa_size(const struct rte_ipsec_sa_prm *prm);
```


### rte_ipsec_sa_init

```c
/**
 * initialise SA based on provided input parameters.
 * 
 * 根据提供的输入参数初始化 SA。
 * 
 * @param sa
 *   SA object to initialise.
 *   
 *   要初始化的 SA 对象
 *   
 * @param prm
 *   Parameters used to initialise given SA object.
 *   
 *   用于初始化给定 SA 对象的参数。
 *   
 * @param size
 *   size of the provided buffer for SA.
 *   
 *   为 SA 提供的缓冲区的大小。
 *   
 * @return
 *   - Actual size of SA object if operation completed successfully.
 *   
 *   如果操作成功完成，SA 对象的实际大小
 *   
 *   - -EINVAL if the parameters are invalid..
 *   
 *   如果参数无效，返回-EINVAL
 *   
 *   - -ENOSPC if the size of the provided buffer is not big enough.
 *   
 *   如果提供的缓冲区的大小不够大。返回-ENOSPC
 */
int
rte_ipsec_sa_init(struct rte_ipsec_sa *sa, const struct rte_ipsec_sa_prm *prm,
	uint32_t size);
```


### rte_ipsec_sa_fini

```c
/**
 * cleanup SA
 * 
 * 清理 SA
 * 
 * @param sa
 *   Pointer to SA object to de-initialize.
 *   
 *   指向要销毁的 SA 对象的指针
 */
void
rte_ipsec_sa_fini(struct rte_ipsec_sa *sa);
```