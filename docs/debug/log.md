[TOC]

# log

```c
#include<rte_log.h>
```

> 源码位置：src/lib/librte_eal/include/rte_log.h


## 宏

### RTE_LOG(l, t, ...)

```c
/**
 * Generates a log message.
 * 
 * 输出一个日志消息
 *
 * The RTE_LOG() is a helper that prefixes the string with the log level
 * and type, and call rte_log().
 * 
 * 这个RTE_LOG()宏调用了rte_log()函数，并在此基础上加上了日志级别和类型的输出
 *
 * @param l
 *   Log level. A value between EMERG (1) and DEBUG (8). The short name is
 *   expanded by the macro, so it cannot be an integer value.
 *   日志级别 1-8
 * @param t
 *   The log type, for example, EAL. The short name is expanded by the
 *   macro, so it cannot be an integer value.
 *   日志类型，比如 EAL
 * @param ...
 *   The fmt string, as in printf(3), followed by the variable arguments
 *   required by the format.
 *   格式化字符串
 * @return
 *   - 0: Success.
 *     0：成功
 *   - Negative on error.
 *     其它值失败
 */
#define RTE_LOG(l, t, ...)					\
	 rte_log(RTE_LOG_ ## l,					\
		 RTE_LOGTYPE_ ## t, # t ": " __VA_ARGS__)
```

### RTE_LOG_DP(l, t, ...)

```c
/**
 * Generates a log message for data path.
 * 
 * 为数据路径生成日志消息
 *
 * Similar to RTE_LOG(), except that it is removed at compilation time
 * if the RTE_LOG_DP_LEVEL configuration option is lower than the log
 * level argument.
 * 
 * 与RTE_LOG（）类似，不同的是，如果RTE_LOG_DP_LEVEL配置选项低于LOG LEVEL参数，
 * 则在编译时会删除它
 *
* @param l
 *   Log level. A value between EMERG (1) and DEBUG (8). The short name is
 *   expanded by the macro, so it cannot be an integer value.
 *   日志级别 1-8
 * @param t
 *   The log type, for example, EAL. The short name is expanded by the
 *   macro, so it cannot be an integer value.
 *   日志类型，比如 EAL
 * @param ...
 *   The fmt string, as in printf(3), followed by the variable arguments
 *   required by the format.
 *   格式化字符串
 * @return
 *   - 0: Success.
 *     0：成功
 *   - Negative on error.
 *     其它值失败
 */
#define RTE_LOG_DP(l, t, ...)					\
	(void)((RTE_LOG_ ## l <= RTE_LOG_DP_LEVEL) ?		\
	 rte_log(RTE_LOG_ ## l,					\
		 RTE_LOGTYPE_ ## t, # t ": " __VA_ARGS__) :	\
	 0)
```


## 函数

### rte_openlog_stream

```c
/**
 * Change the stream that will be used by the logging system.
 * 
 * 更改日志存储的流指针
 *
 * This can be done at any time. The f argument represents the stream
 * to be used to send the logs. If f is NULL, the default output is
 * used (stderr).
 * 
 * 这个方法可以在任何时候调用。f参数表示用于发送日志的流。
 * 如果f为NULL，则使用默认输出（stderr）。
 *
 * @param f
 *   Pointer to the stream.
 *   输出流指针
 * @return
 *   - 0 on success.
 *     0 表示成功
 *   - Negative on error.
 *     其它表示出错
 *     
 */
int rte_openlog_stream(FILE *f);
```


### rte_log_get_stream

```c
/**
 * Retrieve the stream used by the logging system (see rte_openlog_stream()
 * to change it).
 * 
 * 获取目前使用的日志输出流，可以使用rte_openlog_stream函数更改
 *  
 * @return
 *   Pointer to the stream.
 *   日志输出流指针
 */
FILE *rte_log_get_stream(void);
```

### rte_log_set_global_level

```c
/**
 * Set the global log level.
 * 
 * 设置全局日志级别
 *
 * After this call, logs with a level lower or equal than the level
 * passed as argument will be displayed.
 * 
 * 调用之后，将会把小于或等于该级别的日志进行输出
 *
 * @param level
 *   Log level. A value between RTE_LOG_EMERG (1) and RTE_LOG_DEBUG (8).
 *   日志级别，这个值位于1-8之间
 */
void rte_log_set_global_level(uint32_t level);
```

### rte_log_get_global_level

```c
/**
 * Get the global log level.
 * 
 * 获取当前的全局日志级别
 *
 * @return
 *   The current global log level.
 *   当前全局日志级别
 */
uint32_t rte_log_get_global_level(void);
```

### rte_log_get_level

```c
/**
 * Get the log level for a given type.
 * 
 * 获取指定日志类型的日志级别
 *
 * @param logtype
 *   The log type identifier.
 *   日志类型
 * @return
 *   0 on success, a negative value if logtype is invalid.
 *   -1表示失败，>0 则表示日志级别
 */
int rte_log_get_level(uint32_t logtype);
```

### rte_log_can_log

```c
/**
 * For a given `logtype`, check if a log with `loglevel` can be printed.
 * 
 * 对于给定的日志类型，检查该日志类型指定的日志级别是否能够被打印
 *
 * @param logtype
 *   The log type identifier
 *   日志类型
 * @param loglevel
 *   Log level. A value between RTE_LOG_EMERG (1) and RTE_LOG_DEBUG (8).
 *   日志级别 1-8之间
 * @return
 * Returns 'true' if log can be printed and 'false' if it can't.
 * 返回true则表示能被打印，false表示不能被打印
 */
__rte_experimental
bool rte_log_can_log(uint32_t logtype, uint32_t loglevel);
```


### rte_log_set_level_pattern

```c
/**
 * Set the log level for a given type based on globbing pattern.
 * 
 * 使用通配符表达式批量设置日志级别
 *
 * @param pattern
 *   The globbing pattern identifying the log type.
 *   通配符表达式包含的日志类型
 * @param level
 *   The level to be set.
 *   日志级别
 * @return
 *   0 on success, a negative value if level is invalid.
 *   0表示成功，其它表示失败
 */
int rte_log_set_level_pattern(const char *pattern, uint32_t level);
```

### rte_log_set_level_regexp

```c
/**
 * Set the log level for a given type based on regular expression.
 * 
 * 使用正则表达式批量设置日志级别
 *
 * @param regex
 *   The regular expression identifying the log type.
 *   正则表达式包含的日志类型
 * @param level
 *   The level to be set.
 *   日志级别
 * @return
 *   0 on success, a negative value if level is invalid.
 *   0表示成功，其它表示失败
 */
int rte_log_set_level_regexp(const char *regex, uint32_t level);
```


### rte_log_set_level

```c
/**
 * Set the log level for a given type.
 * 
 * 对给定的日志类型设置日志级别
 *
 * @param logtype
 *   The log type identifier.
 *   日志类型
 * @param level
 *   The level to be set.
 *   日志级别
 * @return
 *   0 on success, a negative value if logtype or level is invalid.
 *   0表示成功，其它表示失败
 */
int rte_log_set_level(uint32_t logtype, uint32_t level);
```

### rte_log_cur_msg_loglevel

```c
/**
 * Get the current loglevel for the message being processed.
 * 
 * 获取正在被处理的消息的日志级别
 *
 * Before calling the user-defined stream for logging, the log
 * subsystem sets a per-lcore variable containing the loglevel and the
 * logtype of the message being processed. This information can be
 * accessed by the user-defined log output function through this
 * function.
 * 
 * 在用户定义日志输出流之前，这个日志子系统设置了一个per-lcore variable，
 * 变量包含了正在处理的消息的loglevel以及logtype。这个信息可以通过这个函数进行获取
 *
 * @return
 *   The loglevel of the message being processed.
 *   正在被处理的信息的日志级别
 */
int rte_log_cur_msg_loglevel(void);
```

### rte_log_cur_msg_logtype

```c
/**
 * Get the current logtype for the message being processed.
 * 
 * 获取正在被处理的消息的日志类型
 *
 * Before calling the user-defined stream for logging, the log
 * subsystem sets a per-lcore variable containing the loglevel and the
 * logtype of the message being processed. This information can be
 * accessed by the user-defined log output function through this
 * function.
 *
 * 在用户定义日志输出流之前，这个日志子系统设置了一个per-lcore variable，
 * 变量包含了正在处理的消息的loglevel以及logtype。这个信息可以通过这个函数进行获取
 * 
 * @return
 *   The logtype of the message being processed.
 *   正在被处理的信息的日志类型
 */
int rte_log_cur_msg_logtype(void);
```

### rte_log_register

```c
/**
 * Register a dynamic log type
 * 
 * 注册一个动态日志类型
 *
 * If a log is already registered with the same type, the returned value
 * is the same than the previous one.
 * 
 * 如果注册的日志类型已经存在，则返回值和上一个已经注册的值一样
 *
 * @param name
 *   The string identifying the log type.
 *   日志类型
 * @return
 *   - >0: success, the returned value is the log type identifier.
 *     >0 成功，返回日志类型ID
 *   - (-ENOMEM): cannot allocate memory.
 *     (-ENOMEM):不能分配内存
 */
int rte_log_register(const char *name);
```


### rte_log_dump

```c
/**
 * Dump log information.
 * 
 * 转储日志信息
 *
 * Dump the global level and the registered log types.
 * 
 * 转储全局日志级别以及注册的日志类型列表
 *
 * @param f
 *   The output stream where the dump should be sent.
 *   将要转储的文件流指针
 */
void rte_log_dump(FILE *f);
```

### rte_log

```c
/**
 * Generates a log message.
 * 
 * 输出一个日志消息
 *
 * The message will be sent in the stream defined by the previous call
 * to rte_openlog_stream().
 * 
 * 这个消息将被发送到rte_openlog_stream设置的文件流中
 *
 * The level argument determines if the log should be displayed or
 * not, depending on the loglevel settings.
 * 
 * 日志能否被输出取决于指定的日志级别与设置的全局日志级别的比较
 *
 * The preferred alternative is the RTE_LOG() because it adds the
 * level and type in the logged string.
 * 
 * 首选使用RTE_LOG()进行日志输出，因为它在输出的字符串中添加了日志级别和日志类型
 *
 * @param level
 *   Log level. A value between RTE_LOG_EMERG (1) and RTE_LOG_DEBUG (8).
 *   日志级别 1-8 之间
 * @param logtype
 *   The log type, for example, RTE_LOGTYPE_EAL.
 *   日志类型，比如 RTE_LOGTYPE_EAL
 * @param format
 *   The format string, as in printf(3), followed by the variable arguments
 *   required by the format.
 *   输出的格式化字符串
 * @return
 *   - 0: Success.
 *     0 成功
 *   - Negative on error.
 *     其它值失败
 */
int rte_log(uint32_t level, uint32_t logtype, const char *format, ...)
#ifdef __GNUC__
#if (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ > 2))
	__rte_cold
#endif
#endif
	__rte_format_printf(3, 4);
```

### rte_vlog

```c
/**
 * Generates a log message.
 * 
 * 输出一个日志消息
 *
 * The message will be sent in the stream defined by the previous call
 * to rte_openlog_stream().
 *
 * 这个消息将被发送到rte_openlog_stream设置的文件流中
 * 
 * The level argument determines if the log should be displayed or
 * not, depending on the loglevel settings. A trailing
 * newline may be added if needed.
 * 
 * 日志能否被输出取决于指定的日志级别与设置的全局日志级别的比较
 *
 * The preferred alternative is the RTE_LOG() because it adds the
 * level and type in the logged string.
 * 
 * 首选使用RTE_LOG()进行日志输出，因为它在输出的字符串中添加了日志级别和日志类型
 *
 * @param level
 *   Log level. A value between RTE_LOG_EMERG (1) and RTE_LOG_DEBUG (8).
 *   日志级别 1-8 之间
 * @param logtype
 *   The log type, for example, RTE_LOGTYPE_EAL.
 *   日志类型 比如 RTE_LOGTYPE_EAL
 * @param format
 *   The format string, as in printf(3), followed by the variable arguments
 *   required by the format.
 *   输出的格式化字符串
 * @param ap
 *   The va_list of the variable arguments required by the format.
 *   格式化字符串 所需要的参数列表
 * @return
 *   - 0: Success.
 *     0 成功
 *   - Negative on error.
 *     其它值失败
 */
int rte_vlog(uint32_t level, uint32_t logtype, const char *format, va_list ap)
	__rte_format_printf(3, 0);
```