[TOC]

# trace

```c
#include<rte_trace.h>
```
> 源码位置：src/lib/librte_eal/include/rte_trace.h


## 枚举

### rte_trace_mode

```c
/**
 * Enumerate trace mode operation.
 * 
 * 枚举跟踪模式。
 */
enum rte_trace_mode {
	/**
	 * In this mode, when no space is left in the trace buffer, the
	 * subsequent events overwrite the old events.
	 * 
	 * 在这种模式下，当跟踪缓冲区中没有剩余空间时，后续事件会覆盖旧事件。
	 * 
	 */
	RTE_TRACE_MODE_OVERWRITE,
	/**
	 * In this mode, when no space is left in the trace buffer, the
	 * subsequent events shall not be recorded.
	 * 
	 * 在这种模式下，当跟踪缓冲区中没有剩余空间时，将不记录后续事件。
	 * 
	 */
	RTE_TRACE_MODE_DISCARD,
};
```

## 函数

### rte_trace_is_enabled

```c
/**
 *  Test if trace is enabled.
 *  
 *  测试是否启用了跟踪功能
 *
 *  @return
 *     true if trace is enabled, false otherwise.
 *     
 *  @return
 *     true if trace is enabled, false otherwise.
 *     true 跟踪功能已开启，false 跟踪功能未开启
 */
__rte_experimental
bool rte_trace_is_enabled(void);
```


### rte_trace_mode_set

```c
/**
 * Set the trace mode.
 * 
 * 设置跟踪模式。
 *
 * @param mode
 *   Trace mode.
 *   
 * @param mode
 *   Trace mode.
 *   要设置的跟踪模式
 */
__rte_experimental
void rte_trace_mode_set(enum rte_trace_mode mode);
```


### rte_trace_mode_get

```c
/**
 * Get the trace mode.
 * 
 * 获取跟踪模式
 *
 * @return
 *   The current trace mode.
 *   
 * @return
 *   The current trace mode.
 *   当前使用的跟踪模式
 */
__rte_experimental
enum rte_trace_mode rte_trace_mode_get(void);
```

### rte_trace_pattern

```c
/**
 * Enable/Disable a set of tracepoints based on globbing pattern.
 * 
 * 开启或禁用通过通配符匹配的一组跟踪点
 *
 * @param pattern
 *   The globbing pattern identifying the tracepoint.
 *   跟踪点的匹配规则
 * @param enable
 *   true to enable tracepoint, false to disable the tracepoint, upon match.
 *   true表示开启指定的检查点，false表示禁用指定的检查点
 * @return
 *   - 0: Success and no pattern match.
 *        成功但是没有匹配到检查点
 *   - 1: Success and found pattern match.
 *        成功并且匹配到了检查点
 *   - (-ERANGE): Tracepoint object is not registered.
 *        检查点对象未注册
 */
__rte_experimental
int rte_trace_pattern(const char *pattern, bool enable);
```

### rte_trace_regexp

```c
/**
 * Enable/Disable a set of tracepoints based on regular expression.
 * 
 * 开启或禁用通过正则表达式匹配的一组跟踪点
 *
 * @param regex
 *   A regular expression identifying the tracepoint.
 *   需要匹配的检查点的正则表达式
 * @param enable
 *   true to enable tracepoint, false to disable the tracepoint, upon match.
 *   true表示开启指定的检查点，false表示禁用指定的检查点
 * @return
 *   - 0: Success and no pattern match.
 *        成功但是没有匹配到检查点
 *   - 1: Success and found pattern match.
 *        成功并且匹配到了检查点
 *   - (-ERANGE): Tracepoint object is not registered.
 *        检查点对象未注册
 *   - (-EINVAL): Invalid regular expression rule.
 *        正则表达式解析错误
 */
__rte_experimental
int rte_trace_regexp(const char *regex, bool enable);
```

### rte_trace_save

```c
/**
 * Save the trace buffer to the trace directory.
 * 
 * 将跟踪缓冲区保存到跟踪目录中。
 *
 * By default, trace directory will be created at $HOME directory and this can
 * be overridden by --trace-dir EAL parameter.
 * 
 * 默认情况下，将在 $HOME 目录中创建跟踪目录，可以通过 --trace-dir 这个EAL参数进行覆盖
 * 
 * @return
 *   - 0: Success.
 *        成功
 *   - <0 : Failure.
 *        失败
 */
__rte_experimental
int rte_trace_save(void);
```

### rte_trace_metadata_dump

```c
/**
 * Dump the trace metadata to a file.
 * 
 * 将跟踪的元数据转储到文件中。
 *
 * @param f
 *   A pointer to a file for output
 *   转储的文件指针
 * @return
 *   - 0: Success.
 *        成功
 *   - <0 : Failure.
 *        失败
 */
__rte_experimental
int rte_trace_metadata_dump(FILE *f);
```


### rte_trace_dump

```c
/**
 * Dump the trace subsystem status to a file.
 * 
 * 将跟踪子系统当前状态转储到文件中。
 *
 * @param f
 *   A pointer to a file for output
 *   转储的文件指针
 */
__rte_experimental
void rte_trace_dump(FILE *f);
```