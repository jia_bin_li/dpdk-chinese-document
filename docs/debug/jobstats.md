[TOC]

# jobstats

```c
#include<rte_jobstats.h>
```

> 源码位置：src/lib/librte_jobstats/include/rte_jobstats.h


## 数据结构

### rte_job_update_period_cb_t

```c
/**
 * This function should calculate new period and set it using
 * rte_jobstats_set_period() function. Time spent in this function will be
 * added to job's runtime.
 * 
 * 此函数应计算新周期并使用 rte_jobstats_set_period() 函数设置它。
 * 在此函数中花费的时间将被添加到作业中
 *
 * @param job
 *  The job data structure handler.
 *  作业数据结构指针
 * @param job_result
 *  Result of calling job callback.
 *  调用作业回调的结果。
 */
typedef void (*rte_job_update_period_cb_t)(struct rte_jobstats *job,
		int64_t job_result);
```


### rte_jobstats

```c

struct rte_jobstats {
    //预计执行周期
	uint64_t period;
	/**< Estimated period of execution. */
    
    //最短周期
	uint64_t min_period;
	/**< Minimum period. */
    
    //最大周期
	uint64_t max_period;
	/**< Maximum period. */

    //该任务的期望数值
	int64_t target;
	/**< Desired value for this job. */
    
    // 周期更新回调的回调函数
	rte_job_update_period_cb_t update_period_cb;
	/**< Period update callback. */
    
    //此任务执行的总时间（总和）
	uint64_t exec_time;
	/**< Total time (sum) that this job was executing. */
    
    //最小执行时间
	uint64_t min_exec_time;
	/**< Minimum execute time. */
    
    //最大执行时间
	uint64_t max_exec_time;
	/**< Maximum execute time. */
    
    //执行次数
	uint64_t exec_cnt;
	/**< Execute count. */
    
    //任务名称
	char name[RTE_JOBSTATS_NAMESIZE];
	/**< Name of this job */
    
    //任务上下文
	struct rte_jobstats_context *context;
	/**< Job stats context object that is executing this job. */
} __rte_cache_aligned;
```


### rte_jobstats_context

```c

struct rte_jobstats_context {
	/** Variable holding time at different points:
	 * -# loop start time if loop was started but no job executed yet.
	 * -# job start time if job is currently executing.
	 * -# job finish time if job finished its execution.
	 * -# loop finish time if loop finished its execution. 
	 * 不同阶段的状态时间：
	 * 如果循环已启动但尚未执行任何作业，则为循环开始时间。
	 * 如果作业当前正在执行，则为作业开始时间。
	 * 如果作业完成执行，则作业完成时间。
	 * 如果循环完成执行，则循环完成时间。
	 * */
	uint64_t state_time;
    
    // 此循环中执行的作业数
	uint64_t loop_executed_jobs;
	/**< Count of executed jobs in this loop. */

	/* Statistics start. */

    // 执行作业所花费的总时间，不包括管理时间
	uint64_t exec_time;
	/**< Total time taken to execute jobs, not including management time. */

    // 最小循环执行时间
	uint64_t min_exec_time;
	/**< Minimum loop execute time. */

    // 最大循环执行时间
	uint64_t max_exec_time;
	/**< Maximum loop execute time. */

	/**
	 * Sum of time that is not the execute time (ex: from job finish to next
	 * job start).
	 * 
	 * 除了任务执行时间之外的时间总和（例如：从一个作业完成到下一个作业开始）
	 *
	 * This time might be considered as overhead of library + job scheduling.
	 * 
	 * 这个时间可能被认为是库+作业调度的开销。
	 */
    // 
	uint64_t management_time;

    //最小的管理（除了任务执行时间之外的时间总和）时间
	uint64_t min_management_time;
	/**< Minimum management time */

    //最大的管理（除了任务执行时间之外的时间总和）时间
	uint64_t max_management_time;
	/**< Maximum management time */

    // 自上次重置统计数据以来的时间
	uint64_t start_time;
	/**< Time since last reset stats. */
    
    // 已执行作业的总数。
	uint64_t job_exec_cnt;
	/**< Total count of executed jobs. */
    
    // 至少执行一项作业的已执行的循环总数
	uint64_t loop_cnt;
	/**< Total count of executed loops with at least one executed job. */
} __rte_cache_aligned;
```


## 函数

### rte_jobstats_context_init

```c
/**
 * Initialize given context object with default values.
 * 
 * 使用默认值初始化给定的上下文对象。
 *
 * @param ctx
 *  Job stats context object to initialize.
 *  要初始化的作业统计上下文对象。
 *
 * @return
 *  0 on success
 *  0表示成功
 *  -EINVAL if *ctx* is NULL
 *  如果ctx为NULL则返回-EINVAL
 */
int
rte_jobstats_context_init(struct rte_jobstats_context *ctx);
```

### rte_jobstats_context_start

```c
/**
 * Mark that new set of jobs start executing.
 * 
 * 标记新的作业集开始执行。
 *
 * @param ctx
 *  Job stats context object.
 *  作业统计上下文对象。
 */
void
rte_jobstats_context_start(struct rte_jobstats_context *ctx);
```

### rte_jobstats_context_finish

```c
/**
 * Mark that there is no more jobs ready to execute in this turn. Calculate
 * stats for this loop turn.
 * 
 * 标记本轮中已经没有将要执行的作业。
 * 开始计算此循环的统计数据。
 *
 * @param ctx
 *  Job stats context.
 */
void
rte_jobstats_context_finish(struct rte_jobstats_context *ctx);
```

### rte_jobstats_context_reset

```c
/**
 * Function resets job context statistics.
 * 
 * 重置作业上下文统计信息。
 *
 * @param ctx
 *  Job stats context which statistics will be reset.
 
 *  上下文统计信息对象
 *  
 */
void
rte_jobstats_context_reset(struct rte_jobstats_context *ctx);
```


### rte_jobstats_init

```c
/**
 * Initialize given job stats object.
 * 
 * 初始化给定的作业统计对象。
 *
 * @param job
 *  Job object.
 *  作业对象
 * @param name
 *  Optional job name.
 *  作业名称
 * @param min_period
 *  Minimum period that this job can accept.
 *  此作业可以接受的最小周期。
 * @param max_period
 *  Maximum period that this job can accept.
 *  此作业可以接受的最大周期。
 * @param initial_period
 *  Initial period. It will be checked against *min_period* and *max_period*.
 *  初始周期，它将根据 min_period 和 max_period 进行检查。
 * @param target
 *  Target value that this job try to achieve.
 *  该工作试图达到的目标值。
 *
 * @return
 *  0 on success
 *  0表示成功
 *  -EINVAL if *job* is NULL
 *  如果job为NULL 返回 -EINVAL
 */
int
rte_jobstats_init(struct rte_jobstats *job, const char *name,
		uint64_t min_period, uint64_t max_period, uint64_t initial_period,
		int64_t target);
```

### rte_jobstats_set_target

```c
/**
 * Set job desired target value. Difference between target and job value
 * value must be used to properly adjust job execute period value.
 * 
 * 设置作业所需的目标值。必须使用目标值和任务设置的值之间的差异来正确调整作业执行周期值。
 *
 * @param job
 *  The job object.
 *  作业对象
 * @param target
 *  New target.
 *  新的目标
 */
void
rte_jobstats_set_target(struct rte_jobstats *job, int64_t target);
```


### rte_jobstats_start

```c
/**
 * Mark that *job* is starting of its execution in context of *ctx* object.
 * 
 * 在 ctx 对象的上下文中标记该作业正在开始执行。
 *
 * @param ctx
 *  Job stats context.
 *  作业上下文
 * @param job
 *  Job object.
 *  作业对象
 * @return
 *  0 on success
 *  0表示成功
 *  -EINVAL if *ctx* or *job* is NULL or *job* is executing in another context
 *  context already,
 *  
 *  如果ctx或者job为NULL，或者 job 已经在另一个上下文上下文中执行
 */
int
rte_jobstats_start(struct rte_jobstats_context *ctx, struct rte_jobstats *job);
```


### rte_jobstats_abort

```c
/**
 * Mark that *job* finished its execution, but time of this work will be skipped
 * and added to management time.
 * 
 * 标记该作业已完成执行，但该作业的时间将被跳过并添加到管理时间中。
 *
 * @param job
 *  Job object.
 *  Job对象
 *
 * @return
 *  0 on success
 *  0表示成功
 *  -EINVAL if job is NULL or job was not started (it have no context).
 *  如果作业是NULL，或者作业没有开始则返回-EINVAL
 */
int
rte_jobstats_abort(struct rte_jobstats *job);
```

### rte_jobstats_finish

```c
/**
 * Mark that *job* finished its execution. Context in which it was executing
 * will receive stat update. After this function call *job* object is ready to
 * be executed in other context.
 * 
 * 标记job已经执行完成。它正在执行的上下文将更新统计
 * 在此函数调用后，作业对象已准备好在其他上下文中执行。
 *
 * @param job
 *  Job object.
 *  
 *  作业对象
 *  
 * @param job_value
 *  Job value. Job should pass in this parameter a value that it try to optimize
 *  for example the number of packets it processed.
 *  
 *  作业值，作业应该在这个参数中传递一个它试图优化的值，例如它处理的数据包的数量。
 *
 * @return
 *  0 if job's period was not updated (job target equals *job_value*)
 *  
 *  如果作业周期未更新（job的target值等于job_value）返回0
 *  
 *  1 if job's period was updated
 *  
 *  如果作业的周期被更新返回1
 *  
 *  -EINVAL if job is NULL or job was not started (it have no context).
 */
int
rte_jobstats_finish(struct rte_jobstats *job, int64_t job_value);
```

### rte_jobstats_set_period

```c
/**
 * Set execute period of given job.
 * 
 * 设置给定作业的执行周期。
 *
 * @param job
 *  The job object.
 *  
 *  作业对象
 *  
 * @param period
 *  New period value.
 *  
 *  新的周期值
 *  
 * @param saturate
 *  If zero, skip period saturation to min, max range.
 *  
 *  如果为零，则忽略周期在最小、最大范围的限制。
 */
void
rte_jobstats_set_period(struct rte_jobstats *job, uint64_t period,
		uint8_t saturate);
```


### rte_jobstats_set_min

```c
/**
 * Set minimum execute period of given job. Current period will be checked
 * against new minimum value.
 * 
 * 设置给定作业的最小执行周期。当前期间将根据新的最小值进行检查
 *
 * @param job
 *  The job object.
 *  作业对象
 * @param period
 *  New minimum period value.
 *  新的最小周期值。
 */
void
rte_jobstats_set_min(struct rte_jobstats *job, uint64_t period);
```

### rte_jobstats_set_max

```c
/**
 * Set maximum execute period of given job. Current period will be checked
 * against new maximum value.
 * 
 * 设置给定作业的最大执行周期。当前期间将根据新的最大值进行检查。
 *
 * @param job
 *  The job object.
 *  
 *  作业对象
 *  
 * @param period
 *  New maximum period value.
 *  
 *  新的最大周期值
 */
void
rte_jobstats_set_max(struct rte_jobstats *job, uint64_t period);
```


### rte_jobstats_set_update_period_function

```c
/**
 * Set update period callback that is invoked after job finish.
 * 
 * 设置作业完成后调用的更新周期回调。
 *
 * If application wants to do more sophisticated calculations than default
 * it can provide this handler.
 * 
 * 如果应用程序想要做比默认更复杂的计算，它可以提供这个处理回调
 *
 * @param job
 *  Job object.
 * @param update_period_cb
 *  Callback to set. If NULL restore default update function.
 */
void
rte_jobstats_set_update_period_function(struct rte_jobstats *job,
		rte_job_update_period_cb_t update_period_cb);
```

### rte_jobstats_reset

```c
/**
 * Function resets job statistics.
 * 
 * 该函数重置作业统计。
 *
 * @param job
 *  Job which statistics will be reset.
 *  
 *  将重置统计信息的作业
 *  
 */
void
rte_jobstats_reset(struct rte_jobstats *job);
```