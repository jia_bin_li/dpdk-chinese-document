[TOC]

# trace_point

```c
#include<rte_trace_point.h>
```
> 源码位置：src/lib/librte_eal/include/rte_trace_point.h

## 宏

### RTE_TRACE_POINT

```c
/**
 * Create a tracepoint.
 * 
 * 创建一个追踪点
 *
 * A tracepoint is defined by specifying:
 * - its input arguments: they are the C function style parameters to define
 *   the arguments of tracepoint function. These input arguments are embedded
 *   using the RTE_TRACE_POINT_ARGS macro.
 * - its output event fields: they are the sources of event fields that form
 *   the payload of any event that the execution of the tracepoint macro emits
 *   for this particular tracepoint. The application uses
 *   rte_trace_point_emit_* macros to emit the output event fields.
 *   
 * 通过指定以下内容定义跟踪点：
 * - 输入参数：它们是 C 函数样式参数，用于定义跟踪点函数的参数，
 *   这些输入参数使用 RTE_TRACE_POINT_ARGS 宏嵌入。
 * - 输出事件字段：它们是事件字段的来源，这些字段构成此特定跟踪点发出的所有事件的内容
 *
 * @param tp
 *   Tracepoint object. Before using the tracepoint, an application needs to
 *   define the tracepoint using RTE_TRACE_POINT_REGISTER macro.
 * @param args
 *   C function style input arguments to define the arguments to tracepoint
 *   function.
 * @param ...
 *   Define the payload of trace function. The payload will be formed using
 *   rte_trace_point_emit_* macros. Use ";" delimiter between two payloads.
 * 
 * @参数 tp
 *   跟踪点对象.在使用跟踪点之前，应用程序需要使用 RTE_TRACE_POINT_REGISTER 宏定义跟踪点。
 * @参数 args
 *   C 函数样式输入参数，用于定义跟踪点函数的参数。如 RTE_TRACE_POINT_ARGS(int a ,int b)
 * @参数 ...
 *   定义跟踪函数的调用内容。调用内容将在 rte_trace_point_emit_ 宏形成的函数调用。
 *   两个调用之间的用“;”分隔。
 * 
 * @see RTE_TRACE_POINT_ARGS, RTE_TRACE_POINT_REGISTER, rte_trace_point_emit_*
 */
#define
RTE_TRACE_POINT(tp, args, ...) \
    __RTE_TRACE_POINT(generic, tp, args, __VA_ARGS__)

#define
__RTE_TRACE_POINT(_mode, _tp, _args, ...) \
extern rte_trace_point_t __##_tp; \
static __rte_always_inline void \
_tp _args \
{
\
    __rte_trace_point_emit_header_##_mode(&__##_tp); \
    __VA_ARGS__ \

}
```
例子：
```c
RTE_TRACE_POINT(
	rte_eal_trace_intr_callback_unregister,
	RTE_TRACE_POINT_ARGS(const struct rte_intr_handle *handle,
		rte_intr_callback_fn cb, void *cb_arg, int rc),
	rte_trace_point_emit_int(rc);
	rte_trace_point_emit_int(handle->vfio_dev_fd);
	rte_trace_point_emit_int(handle->fd);
	rte_trace_point_emit_int(handle->type);
	rte_trace_point_emit_u32(handle->max_intr);
	rte_trace_point_emit_u32(handle->nb_efd);
	rte_trace_point_emit_ptr(cb);
	rte_trace_point_emit_ptr(cb_arg);
)
```


### RTE_TRACE_POINT_FP

```c
/**
 * Create a tracepoint for fast path.
 * 
 * 使用快速路径创建跟踪点。
 *
 * Similar to RTE_TRACE_POINT, except that it is removed at compilation time
 * unless the RTE_ENABLE_TRACE_FP configuration parameter is set.
 * 
 * 与 RTE_TRACE_POINT 类似，除非设置了 RTE_ENABLE_TRACE_FP 配置参数，否则将在编译时被移除
 *
 * @param tp
 *   Tracepoint object. Before using the tracepoint, an application needs to
 *   define the tracepoint using RTE_TRACE_POINT_REGISTER macro.
 * @param args
 *   C function style input arguments to define the arguments to tracepoint.
 *   function.
 * @param ...
 *   Define the payload of trace function. The payload will be formed using
 *   rte_trace_point_emit_* macros, Use ";" delimiter between two payloads.
 *   
 * @参数 tp
 *   跟踪点对象.在使用跟踪点之前，应用程序需要使用 RTE_TRACE_POINT_REGISTER 宏定义跟踪点。
 * @参数 args
 *   C 函数样式输入参数，用于定义跟踪点函数的参数。如 RTE_TRACE_POINT_ARGS(int a ,int b)
 * @参数 ...
 *   定义跟踪函数的调用内容。调用内容将在 rte_trace_point_emit_ 宏形成的函数调用。
 *   两个调用之间的用“;”分隔。
 *   
 * @see RTE_TRACE_POINT
 */
#define RTE_TRACE_POINT_FP(tp, args, ...) \
	__RTE_TRACE_POINT(fp, tp, args, __VA_ARGS__)

```

例子：
```c
RTE_TRACE_POINT_FP(
	rte_cryptodev_trace_dequeue_burst,
	RTE_TRACE_POINT_ARGS(uint8_t dev_id, uint16_t qp_id, void **ops,
		uint16_t nb_ops),
	rte_trace_point_emit_u8(dev_id);
	rte_trace_point_emit_u16(qp_id);
	rte_trace_point_emit_ptr(ops);
	rte_trace_point_emit_u16(nb_ops);
)
```

### RTE_TRACE_POINT_REGISTER

```c
/**
 * Register a tracepoint.
 * 
 * 注册一个跟踪点
 *
 * @param trace
 *   The tracepoint object created using RTE_TRACE_POINT_REGISTER.
 * @param name
 *   The name of the tracepoint object.
 * @return
 *   - 0: Successfully registered the tracepoint.
 *   - <0: Failure to register the tracepoint.
 *   
 *  @参数 trace
 *    使用 RTE_TRACE_POINT_REGISTER 创建的跟踪点对象。
 *  @参数 name
 *    跟踪点对象的名称。
 *  @返回值
 *    - 0 成功注册
 *    - <0 注册失败
 */
#define RTE_TRACE_POINT_REGISTER(trace, name)
```

### rte_trace_point_emit_u64

```c
/** Tracepoint function payload for uint64_t datatype */
// uint64_t 数据类型的跟踪点数据发送函数
#define rte_trace_point_emit_u64(val)
```

### rte_trace_point_emit_i64

```c
/** Tracepoint function payload for int64_t datatype */
// int64_t 数据类型的跟踪点数据发送函数
#define rte_trace_point_emit_i64(val)
```

### rte_trace_point_emit_u32

```c
/** Tracepoint function payload for uint32_t datatype */
// uint32_t 数据类型的跟踪点数据发送函数
#define rte_trace_point_emit_u32(val)
```

### rte_trace_point_emit_i32

```c
/** Tracepoint function payload for int32_t datatype */
// int32_t 数据类型的跟踪点数据发送函数
#define rte_trace_point_emit_i32(val)
```

### rte_trace_point_emit_u16

```c
/** Tracepoint function payload for uint16_t datatype */
// uint16_t 数据类型的跟踪点数据发送函数
#define rte_trace_point_emit_u16(val)
```

### rte_trace_point_emit_i16

```c
/** Tracepoint function payload for int16_t datatype */
// int16_t 数据类型的跟踪点数据发送函数
#define rte_trace_point_emit_i16(val)
```

### rte_trace_point_emit_u8

```c
/** Tracepoint function payload for uint8_t datatype */
// uint8_t 数据类型的跟踪点数据发送函数
#define rte_trace_point_emit_u8(val)
```

### rte_trace_point_emit_i8

```c
/** Tracepoint function payload for int8_t datatype */
// int8_t 数据类型的跟踪点数据发送函数
#define rte_trace_point_emit_i8(val)
```

### rte_trace_point_emit_int

```c
/** Tracepoint function payload for int datatype */
// int 数据类型的跟踪点数据发送函数
#define rte_trace_point_emit_int(val)
```

### rte_trace_point_emit_long

```c
/** Tracepoint function payload for long datatype */
// long 数据类型的跟踪点数据发送函数
#define rte_trace_point_emit_long(val)
```

### rte_trace_point_emit_size_t

```c
/** Tracepoint function payload for size_t datatype */
// size_t 数据类型的跟踪点数据发送函数
#define rte_trace_point_emit_size_t(val)
```

### rte_trace_point_emit_float

```c
/** Tracepoint function payload for float datatype */
// float 数据类型的跟踪点数据发送函数
#define rte_trace_point_emit_float(val)
```

### rte_trace_point_emit_double

```c
/** Tracepoint function payload for double datatype */
// double 数据类型的跟踪点数据发送函数
#define rte_trace_point_emit_double(val)
```

### rte_trace_point_emit_ptr

```c
/** Tracepoint function payload for point datatype */
// point 数据类型的跟踪点数据发送函数
#define rte_trace_point_emit_ptr(val)
```

### rte_trace_point_emit_string

```c
/** Tracepoint function payload for string datatype */
// string 数据类型的跟踪点数据发送函数
#define rte_trace_point_emit_string(val)
```

## 函数

### rte_trace_point_enable

```c
/**
 * Enable recording events of the given tracepoint in the trace buffer.
 * 
 * 在跟踪缓冲区中启用给定跟踪点的记录事件。
 *
 * @param tp
 *   The tracepoint object to enable.
 * @return
 *   - 0: Success.
 *   - (-ERANGE): Trace object is not registered.
 *   
 * @参数 tp
 *   要启用的跟踪点对象。
 * 
 * @返回值
 *   - 0 成功
 *   - (-ERANGE)： 跟踪对象未注册。
 *   
 */
__rte_experimental
int rte_trace_point_enable(rte_trace_point_t *tp);
```

### rte_trace_point_disable

```c
/**
 * Disable recording events of the given tracepoint in the trace buffer.
 * 
 * 禁用在跟踪缓冲区中记录给定跟踪点的事件。
 *
 * @param tp
 *   The tracepoint object to disable.
 * @return
 *   - 0: Success.
 *   - (-ERANGE): Trace object is not registered.
 *   
 * @参数 tp
 *   要禁用的跟踪点对象。
 * 
 * @返回值
 *   - 0 成功
 *   - (-ERANGE)： 跟踪对象未注册。
 *   
 */
__rte_experimental
int rte_trace_point_disable(rte_trace_point_t *tp);
```

### rte_trace_point_is_enabled

```c
/**
 * Test if recording events from the given tracepoint is enabled.
 * 
 * 测试是否启用了给定跟踪点的记录事件。
 *
 * @param tp
 *    The tracepoint object.
 * @return
 *    true if tracepoint is enabled, false otherwise.
 *    
 * @参数 tp
 *   跟踪点对象。
 * 
 * @返回值
 *   true 表明跟踪点开启，false表明跟踪点被禁用
 */
__rte_experimental
bool rte_trace_point_is_enabled(rte_trace_point_t *tp);
```


### rte_trace_point_lookup

```c
/**
 * Lookup a tracepoint object from its name.
 * 
 * 通过名字查找一个跟踪点对象
 *
 * @param name
 *   The name of the tracepoint.
 * @return
 *   The tracepoint object or NULL if not found.
 *   
 * @参数 name
 *   跟踪点的名称
 * @返回值
 *   如果未找到，则为 NULL,找到则返回跟踪点对象
 */
__rte_experimental
rte_trace_point_t *rte_trace_point_lookup(const char *name);
```



