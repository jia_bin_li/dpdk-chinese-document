[TOC]

# kni

```c
#include<rte_cryptodev.h>
```

> 源码位置：src/lib/librte_cryptodev/rte_cryptodev.h


## 宏

### rte_crypto_op_ctod_offset

```c
/**
 * A macro that points to an offset from the start
 * of the crypto operation structure (rte_crypto_op)
 * 
 * 返回从加密操作结构 (rte_crypto_op) 开始的偏移量的指针
 *
 * The returned pointer is cast to type t.
 * 
 * 返回的指针被强制转换为 t 类型
 *
 * @param c
 *   The crypto operation.
 *   加密操作。
 * @param o
 *   The offset from the start of the crypto operation.
 *   从加密操作对象开始的偏移量
 * @param t
 *   The type to cast the result into.
 *   将结果转换为t的类型。
 */
#define rte_crypto_op_ctod_offset(c, t, o)	\
	((t)((char *)(c) + (o)))
```


### rte_crypto_op_ctophys_offset


```c
/**
 * A macro that returns the physical address that points
 * to an offset from the start of the crypto operation
 * (rte_crypto_op)
 * 
 * 返回物理地址的宏，该地址指向从加密操作开始的偏移量 (rte_crypto_op)
 *
 * @param c
 *   The crypto operation.
 *   加密操作
 * @param o
 *   The offset from the start of the crypto operation
 *   to calculate address from.
 *   从加密操作开始计算地址的偏移量。
 */
#define rte_crypto_op_ctophys_offset(c, o)	\
	(rte_iova_t)((c)->phys_addr + (o))
```

## 函数

### rte_crypto_param_range

```c
/**
 * Crypto parameters range description
 * 
 * 加密参数范围描述
 */
struct rte_crypto_param_range {
    // 最小值
	uint16_t min;	/**< minimum size */
    // 最大值
	uint16_t max;	/**< maximum size */
    // 如果支持大小范围，则此参数用于指示在最小和最大之间支持的字节大小增量
	uint16_t increment;
	/**< if a range of sizes are supported,
	 * this parameter is used to indicate
	 * increments in byte size that are supported
	 * between the minimum and maximum
	 */
};
```

### rte_cryptodev_symmetric_capability

```c
/**
 * Symmetric Crypto Capability
 * 
 * 对称加密能力
 * 
 */
struct rte_cryptodev_symmetric_capability {
    // 加密转换类型 （未指定加密转换类型、验证转换类型、密码转换类型、AEAD转换类型）
	enum rte_crypto_sym_xform_type xform_type;
	/**< Transform type : Authentication / Cipher / AEAD */
	RTE_STD_C11
	union {
		struct {
            // 加密认证算法
			enum rte_crypto_auth_algorithm algo;
			/**< authentication algorithm */
            // 算法块大小
			uint16_t block_size;
			/**< algorithm block size */
            // key大小范围
			struct rte_crypto_param_range key_size;
			/**< auth key size range */
            // 摘要大小范围
			struct rte_crypto_param_range digest_size;
			/**< digest size range */
            // 附加认证数据大小范围
			struct rte_crypto_param_range aad_size;
			/**< Additional authentication data size range */
            // 初始化向量数据大小范围
			struct rte_crypto_param_range iv_size;
			/**< Initialisation vector data size range */
		} auth;
		/**< Symmetric Authentication transform capabilities */
        
        
		struct 
            //加密算法
			enum rte_crypto_cipher_algorithm algo;
			/**< cipher algorithm */
            // 块大小
			uint16_t block_size;
			/**< algorithm block size */
            //key的大小范围
			struct rte_crypto_param_range key_size;
			/**< cipher key size range */
            //初始化向量大小范围
			struct rte_crypto_param_range iv_size;
			/**< Initialisation vector data size range */
		} cipher;
		/**< Symmetric Cipher transform capabilities */
        
        
		struct {
            //aead加密算法
			enum rte_crypto_aead_algorithm algo;
			/**< AEAD algorithm */
            //块大小
			uint16_t block_size;
			/**< algorithm block size */
            //key的大小范围
			struct rte_crypto_param_range key_size;
			/**< AEAD key size range */
            //摘要的大小范围
			struct rte_crypto_param_range digest_size;
			/**< digest size range */
            //附加认证数据大小范围
			struct rte_crypto_param_range aad_size;
			/**< Additional authentication data size range */
            //初始化向量数据大小范围
			struct rte_crypto_param_range iv_size;
			/**< Initialisation vector data size range */
		} aead;
	};
};
```

### rte_cryptodev_asymmetric_xform_capability

```c
/**
 * Asymmetric Xform Crypto Capability
 * 
 * 非对称加密能力
 *
 */
struct rte_cryptodev_asymmetric_xform_capability {
    
    //加密转换类型  RSA/MODEXP/DH/DSA/MODINV
	enum rte_crypto_asym_xform_type xform_type;
	/**< Transform type: RSA/MODEXP/DH/DSA/MODINV */

    // 支持的操作位掩码
	uint32_t op_types;
	/**< bitmask for supported rte_crypto_asym_op_type */

	__extension__
	union {
        // 支持的模数长度范围。值 0 表示实现默认值
		struct rte_crypto_param_range modlen;
		/**< Range of modulus length supported by modulus based xform.
		 * Value 0 mean implementation default
		 */
	};
};
```

### rte_cryptodev_asymmetric_capability

```c
/**
 * Asymmetric Crypto Capability
 * 
 * 非对称加密能力
 *
 */
struct rte_cryptodev_asymmetric_capability {
	struct rte_cryptodev_asymmetric_xform_capability xform_capa;
};
```


### rte_cryptodev_capabilities


```c
/** Structure used to capture a capability of a crypto device */
// 用于存储加密设备功能的结构体
struct rte_cryptodev_capabilities {
    //操作类型（无操作、对称加密、非对称加密）
	enum rte_crypto_op_type op;
	/**< Operation type */

	RTE_STD_C11
	union {
        // 对称加密能力参数
		struct rte_cryptodev_symmetric_capability sym;
		/**< Symmetric operation capability parameters */
        // 非对称加密能力参数
		struct rte_cryptodev_asymmetric_capability asym;
		/**< Asymmetric operation capability parameters */
	};
};
```

### rte_cryptodev_sym_capability_idx

```c
/** Structure used to describe crypto algorithms */
// 用于描述对称加密算法的结构
struct rte_cryptodev_sym_capability_idx {
    //加密转换类型
	enum rte_crypto_sym_xform_type type;
	union {
		enum rte_crypto_cipher_algorithm cipher;
		enum rte_crypto_auth_algorithm auth;
		enum rte_crypto_aead_algorithm aead;
	} algo;
};
```

### rte_cryptodev_asym_capability_idx

```c
/**
 * Structure used to describe asymmetric crypto xforms
 * Each xform maps to one asym algorithm.
 * 
 * 用于描述非对称加密转换的结构 每个转换映射到一个非对称算法。
 *
 */
struct rte_cryptodev_asym_capability_idx {
    //非对称加密转换类型
	enum rte_crypto_asym_xform_type type;
	/**< Asymmetric xform (algo) type */
};
```

## 函数

### rte_cryptodev_sym_capability_get

```c
/**
 * Provide capabilities available for defined device and algorithm
 * 
 * 验证给定设备和算法的功能是否可用
 *
 * @param	dev_id		The identifier of the device.  设备ID
 * @param	idx		Description of crypto algorithms.  对称加密算法描述
 *
 * @return
 *   - Return description of the symmetric crypto capability if exist. 
 *   如果存在，则返回对称加密功能的描述。
 *   - Return NULL if the capability not exist.
 *   如果能力不存在，则返回 NULL。
 */
const struct rte_cryptodev_symmetric_capability *
rte_cryptodev_sym_capability_get(uint8_t dev_id,
		const struct rte_cryptodev_sym_capability_idx *idx);
```



### rte_cryptodev_asym_capability_get

```c
/**
 *  Provide capabilities available for defined device and xform
 *  
 *  验证给定设备和算法的功能是否可用
 *
 * @param	dev_id		The identifier of the device. 设备ID
 * @param	idx		Description of asym crypto xform. 非对称加密算法描述
 *
 * @return
 *   - Return description of the asymmetric crypto capability if exist.
 *   如果存在，则返回非对称加密功能的描述。
 *   - Return NULL if the capability not exist.
 *   如果能力不存在，则返回 NULL。
 */
__rte_experimental
const struct rte_cryptodev_asymmetric_xform_capability *
rte_cryptodev_asym_capability_get(uint8_t dev_id,
		const struct rte_cryptodev_asym_capability_idx *idx);
```

### rte_cryptodev_sym_capability_check_cipher

```c
/**
 * Check if key size and initial vector are supported
 * in crypto cipher capability
 * 
 * 检查对称加密密码功能是否支持指定的密钥大小和初始向量大小
 *
 * @param	capability	Description of the symmetric crypto capability. 对称加密功能的描述对象
 * @param	key_size	Cipher key size. 要验证的key的大小
 * @param	iv_size		Cipher initial vector size. 要验证的初始化向量的大小
 *
 * @return
 *   - Return 0 if the parameters are in range of the capability.
 *   如果参数在能力范围内，则返回 0
 *   - Return -1 if the parameters are out of range of the capability.
 *   如果参数不在能力范围内，则返回 -1
 */
int
rte_cryptodev_sym_capability_check_cipher(
		const struct rte_cryptodev_symmetric_capability *capability,
		uint16_t key_size, uint16_t iv_size);
```

### rte_cryptodev_sym_capability_check_auth

```c
/**
 * Check if key size and initial vector are supported
 * in crypto auth capability
 * 
 * 检查对称加密身份认证功能是否支持指定的密钥大小和初始向量大小
 *
 * @param	capability	Description of the symmetric crypto capability. 对称加密功能的描述对象
 * @param	key_size	Auth key size. 要验证的key的大小
 * @param	digest_size	Auth digest size. 要验证的摘要的大小
 * @param	iv_size		Auth initial vector size. 要验证的初始向量大小
 *
 * @return
 *   - Return 0 if the parameters are in range of the capability.
 *   如果参数在能力范围内返回0
 *   - Return -1 if the parameters are out of range of the capability.
 *   如果参数不在能力范围内返回-1
 */
int
rte_cryptodev_sym_capability_check_auth(
		const struct rte_cryptodev_symmetric_capability *capability,
		uint16_t key_size, uint16_t digest_size, uint16_t iv_size);
```

### rte_cryptodev_sym_capability_check_aead

```c
/**
 * Check if key, digest, AAD and initial vector sizes are supported
 * in crypto AEAD capability
 * 
 * 检查加密 AEAD 功能是否支持密钥、摘要、AAD 和初始向量大小
 *
 * @param	capability	Description of the symmetric crypto capability. 对称加密功能的描述对象
 * @param	key_size	AEAD key size. AEAD key大小
 * @param	digest_size	AEAD digest size. AEAD 摘要大小
 * @param	aad_size	AEAD AAD size. AEAD AAD大小
 * @param	iv_size		AEAD IV size. AEAD 向量大小
 *
 * @return
 *   - Return 0 if the parameters are in range of the capability.
 *   如果参数在能力范围内返回0
 *   - Return -1 if the parameters are out of range of the capability.
 *   如果参数不在能力范围内返回-1
 */
int
rte_cryptodev_sym_capability_check_aead(
		const struct rte_cryptodev_symmetric_capability *capability,
		uint16_t key_size, uint16_t digest_size, uint16_t aad_size,
		uint16_t iv_size);
```

### rte_cryptodev_asym_xform_capability_check_optype

```c
/**
 * Check if op type is supported
 * 
 * 检查是否支持 op 类型
 *
 * @param	capability	Description of the asymmetric crypto capability. 非对称加密功能的描述对象
 * @param	op_type		op type 操作类型
 *
 * @return
 *   - Return 1 if the op type is supported
 *   支持的操作类型返回1
 *   - Return 0 if unsupported
 *   不支持的操作类型返回0
 */
__rte_experimental
int
rte_cryptodev_asym_xform_capability_check_optype(
	const struct rte_cryptodev_asymmetric_xform_capability *capability,
		enum rte_crypto_asym_op_type op_type);
```


### rte_cryptodev_asym_xform_capability_check_modlen


```c
/**
 * Check if modulus length is in supported range
 * 
 * 检查模数长度是否在支持范围内
 *
 * @param	capability	Description of the asymmetric crypto capability. 非对称加密功能的描述对象
 * @param	modlen		modulus length. 模数长度
 *
 * @return
 *   - Return 0 if the parameters are in range of the capability.
 *   在能力范围内返回0
 *   - Return -1 if the parameters are out of range of the capability.
 *   不在能力范围内返回-1
 */
__rte_experimental
int
rte_cryptodev_asym_xform_capability_check_modlen(
	const struct rte_cryptodev_asymmetric_xform_capability *capability,
		uint16_t modlen);
```

### rte_cryptodev_get_cipher_algo_enum

```c
/**
 * Provide the cipher algorithm enum, given an algorithm string
 * 
 * 通过算法名称获取算法枚举对象
 *
 * @param	algo_enum	A pointer to the cipher algorithm
 *				enum to be filled 指向要填充的密码算法枚举的指针
 * @param	algo_string	Authentication algo string 认证算法字符串
 *
 * @return
 * - Return -1 if string is not valid
 * 字符串无效则返回-1
 * - Return 0 is the string is valid
 * 成功返回0
 */
int
rte_cryptodev_get_cipher_algo_enum(enum rte_crypto_cipher_algorithm *algo_enum,
		const char *algo_string);
```


### rte_cryptodev_get_auth_algo_enum


```c
/**
 * Provide the authentication algorithm enum, given an algorithm string
 * 
 * 通过认证算法名称获取算法枚举对象
 *
 * @param	algo_enum	A pointer to the authentication algorithm
 *				enum to be filled  指向要填充的身份验证算法枚举的指针
 * @param	algo_string	Authentication algo string 认证算法字符串
 *
 * @return
 * - Return -1 if string is not valid
 * 字符串无效则返回-1
 * - Return 0 is the string is valid
 * 成功返回0
 */
int
rte_cryptodev_get_auth_algo_enum(enum rte_crypto_auth_algorithm *algo_enum,
		const char *algo_string);
```

### rte_cryptodev_get_aead_algo_enum

```c
/**
 * Provide the AEAD algorithm enum, given an algorithm string
 * 
 * 通过AEAD算法名称获取算法枚举对象
 *
 * @param	algo_enum	A pointer to the AEAD algorithm
 *				enum to be filled 指向要填充的 AEAD 算法枚举的指针
 * @param	algo_string	AEAD algorithm string AEAD算法字符串
 *
 * @return
 * - Return -1 if string is not valid
 * 字符串无效则返回-1
 * - Return 0 is the string is valid
 * 成功返回0
 */
int
rte_cryptodev_get_aead_algo_enum(enum rte_crypto_aead_algorithm *algo_enum,
		const char *algo_string);
```

### rte_cryptodev_asym_get_xform_enum


```c
/**
 * Provide the Asymmetric xform enum, given an xform string
 * 
 * 给定一个 xform 字符串，返回非对称 xform 枚举
 *
 * @param	xform_enum	A pointer to the xform type
 *				enum to be filled 指向要填充的 xform 类型枚举的指针
 * @param	xform_string	xform string xform字符串
 *
 * @return
 * - Return -1 if string is not valid
 * 字符串无效则返回-1
 * - Return 0 if the string is valid
 * 成功返回0
 */
__rte_experimental
int
rte_cryptodev_asym_get_xform_enum(enum rte_crypto_asym_xform_type *xform_enum,
		const char *xform_string);
```

### rte_cryptodev_get_feature_name

```c
/**
 * Get the name of a crypto device feature flag
 * 
 * 获取加密设备功能标志的名称
 *
 * @param	flag	The mask describing the flag. 描述标志的掩码
 *
 * @return
 *   The name of this flag, or NULL if it's not a valid feature flag.
 *   
 *   此标志的名称，如果不是有效的功能标志，则为 NULL。
 */
extern const char *
rte_cryptodev_get_feature_name(uint64_t flag);
```

