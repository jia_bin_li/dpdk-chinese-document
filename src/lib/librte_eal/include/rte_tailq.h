/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright(c) 2010-2014 Intel Corporation
 */

#ifndef _RTE_TAILQ_H_
#define _RTE_TAILQ_H_

/**
 * @file
 *  Here defines rte_tailq APIs for only internal use
 *  这里定义了为内部使用的尾队列操作API
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/queue.h>
#include <stdio.h>
#include <rte_debug.h>

/** dummy structure type used by the rte_tailq APIs
 *
 * #define	_TAILQ_ENTRY(type, qual)
 * struct {
 *	qual type *tqe_next;
 * 	qual type *qual *tqe_prev;
 * }
 * #define TAILQ_ENTRY(type)	_TAILQ_ENTRY(struct type,)
 *
 * */
// 每个尾队列的节点结构体
struct rte_tailq_entry {
    // sys/queue.h定义的尾队列节点
    TAILQ_ENTRY(rte_tailq_entry)
    next; /**< Pointer entries for a tailq list */
    //该节点的数据指针
    void *data; /**< Pointer to the data referenced by this tailq entry */
};
/** dummy
 *
#define	_TAILQ_HEAD(name, type, qual)
struct name {
	qual type *tqh_first;
	qual type *qual *tqh_last;
}
#define TAILQ_HEAD(name, type)	_TAILQ_HEAD(name, struct type,)
 *
 *
 * */
//定义尾队列头结构体
TAILQ_HEAD(rte_tailq_entry_head, rte_tailq_entry);

//队列名字长度
#define RTE_TAILQ_NAMESIZE 32

/**
 * The structure defining a tailq header entry for storing
 * in the rte_config structure in shared memory. Each tailq
 * is identified by name.
 * Any library storing a set of objects e.g. rings, mempools, hash-tables,
 * is recommended to use an entry here, so as to make it easy for
 * a multi-process app to find already-created elements in shared memory.
 *
 * 定义了一个尾队列头结构体，为了能够存储在共享内存中的rte_config结构中。每个队列通过名字
 * 进行标识。
 * 建议任何存储一组对象（如环、内存池、哈希表）的库在此处使用条目，以便多进程应用程序在共享
 * 内存中查找已创建的元素。
 *
 */
struct rte_tailq_head {
    struct rte_tailq_entry_head tailq_head; /**< NOTE: must be first element */
    char name[RTE_TAILQ_NAMESIZE];
};

struct rte_tailq_elem {
    /**
     * Reference to head in shared mem, updated at init time by
     * rte_eal_tailqs_init()
     */
    struct rte_tailq_head *head;
    TAILQ_ENTRY(rte_tailq_elem)
    next;
    const char name[RTE_TAILQ_NAMESIZE];
};

/**
 * Return the first tailq entry cast to the right struct.
 * 将第一个tailq entry类型转换为struct_name结构。
 */
#define RTE_TAILQ_CAST(tailq_entry, struct_name) \
    (struct struct_name *)&(tailq_entry)->tailq_head

/**
 * Utility macro to make looking up a tailqueue for a particular struct easier.
 *
 * 查找指定名称的tailq
 *
 * @param name
 *   The name of tailq
 *   tailq名称
 *
 * @param struct_name
 *   The name of the list type we are using. (Generally this is the same as the
 *   first parameter passed to TAILQ_HEAD macro)
 *
 *   使用的队列的类型，将进行强转
 *
 * @return
 *   The return value from rte_eal_tailq_lookup, typecast to the appropriate
 *   structure pointer type.
 *   NULL on error, since the tailq_head is the first
 *   element in the rte_tailq_head structure.
 *
 *   返回找到的tailq并强转成指定的类型
 *
 */
#define RTE_TAILQ_LOOKUP(name, struct_name) \
    RTE_TAILQ_CAST(rte_eal_tailq_lookup(name), struct_name)

/**
 * Dump tail queues to a file.
 *
 * 将尾部队列转储到文件中。
 *
 * @param f
 *   A pointer to a file for output
 *   要转储的输出文件
 */
void rte_dump_tailq(FILE *f);

/**
 * Lookup for a tail queue.
 *
 * 查找一个尾队列
 *
 * Get a pointer to a tail queue header of a tail
 * queue identified by the name given as an argument.
 * Note: this function is not multi-thread safe, and should only be called from
 * a single thread at a time
 *
 * 获取指定名称的尾队列指针
 * 注意：这个方法不是线程安全的，只能在同一时间一个线程调用
 *
 * @param name
 *   The name of the queue.
 *   队列名称
 * @return
 *   A pointer to the tail queue head structure.
 *   找到的队列头指针
 */
struct rte_tailq_head *rte_eal_tailq_lookup(const char *name);

/**
 * Register a tail queue.
 *
 * 注册一个尾队列
 *
 * Register a tail queue from shared memory.
 * This function is mainly used by EAL_REGISTER_TAILQ macro which is used to
 * register tailq from the different dpdk libraries. Since this macro is a
 * constructor, the function has no access to dpdk shared memory, so the
 * registered tailq can not be used before call to rte_eal_init() which calls
 * rte_eal_tailqs_init().
 *
 * 从共享内存中注册一个尾队列，此函数主要由EAL_REGISTER_TAILQ宏使用，该宏用于从不同
 * 的dpdk库中注册TAILQ。由于该宏是一个构造函数，该函数无法访问dpdk共享内存，因此在调
 * 用rte_eal_init()之前，不能使用已注册的tailq，rte_eal_init()方法内会调用
 * rte_eal_tailqs_init()
 *
 * @param t
 *   The tailq element which contains the name of the tailq you want to
 *   create (/retrieve when in secondary process).
 *
 *   要注册的队列元素结构体，里面包含了队列名称
 *
 * @return
 *   0 on success or -1 in case of an error.
 */
int rte_eal_tailq_register(struct rte_tailq_elem *t);

#define EAL_REGISTER_TAILQ(t) \
RTE_INIT(tailqinitfn_ ##t) \
{ \
    if (rte_eal_tailq_register(&t) < 0) \
        rte_panic("Cannot initialize tailq: %s\n", t.name); \
}

/* This macro permits both remove and free var within the loop safely.*/
// 该宏允许在循环中安全地删除和释放队列节点
#ifndef TAILQ_FOREACH_SAFE
#define TAILQ_FOREACH_SAFE(var, head, field, tvar)        \
    for ((var) = TAILQ_FIRST((head));            \
        (var) && ((tvar) = TAILQ_NEXT((var), field), 1);    \
        (var) = (tvar))
#endif

#ifdef __cplusplus
}
#endif

#endif /* _RTE_TAILQ_H_ */
